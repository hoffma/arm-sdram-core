# ARMv4 Microprocessor

Source to recreate the project from my thesis "Entwurf und Implementierung
eines Cache- und SDRAM-Controllers für eine ARMv4 Mikroarchitektur auf Xilinx
Kintex FPGAs"

Thesis can be found [here](masterarbeit_hoffmann_555625.pdf)

## Project structure

This project was written for an Arty A7 though. Project structure is the
following:

- srcs/ (All the VHDL sources)
    - ArmCore/ (Sources of the original ARMv4 Core from Carsten Böhme)
    - Sdram/ (Additional Sources created for this thesis)
        - ArmSdramInterface.vhd (Abstraction layer to connect sdram and ARM core)
        - Sdram.vhd (Abstraction layer to reduce sdram output to 32 bit and toggle Cache controller)
        - CacheController.vhd (Actual Cache controller source)
        - SdramDriver.vhd (Simple abstraction layer for the SDRAM controller state machine)
    - sim/ (Simulation sources for this thesis)
        - MyArmTop_tb.vhd
        - CacheController_tb.vhd
        - Sdram_fileio_tb.vhd
        - SdramDriver_fileio_tb.vhd
    - DummyRegister.vhd (A simple register that was needed for simulation)
    - MyArmTop.vhd (A simple top entity with All needed hardware signals)
- ip_scripts/ (tcl scripts to recreate all IP instances that were needed)
    - blk_mem_gen_0.tcl (Main flash memory of the ARM core)
    - clk_wiz_0.tcl (newly created clock gen, creating core (20 MHz) and sdram (166 MHz, 200 MHz) clocks)
    - cache_mem.tcl (Cache data memory)
    - cache_ctrl_mem.tcl (Cache control memory for tags and valid bits)
    - mig_7series_0.tcl (SDRAM Controller instance)

## Recreate the project in Vivado

_Disclaimer:_ This was tested on Vivado 2021.2

There are two possibilites to recreate this project. With the CLI or GUI.

- CLI
    To recreate this project from the CLI just change into this directory and
    then run Vivado from the command line with the following 
    argument: `vivado -source create_project.tcl`
- GUI
    Start Vivado and on the startup window go on the top left under _Tools_ ->
    _Run Tcl Script_ -> Select the create_project.tcl script

This should create the project, synthesize everything and generate the
bitstream. After uploading this to the fpga the program can be uploaded

### Compile software

Sample programs can be found in the folder `sample_programs`

The folders marked with 'no delay' were mainly used to create simulation data

### Upload program to FPGA

To upload software to the FPGA you can use termio. Copy the termio folder to
~/termio/ and compile it. 

- Make sure the LDP button is set. Reset your FPGA a few times (or upload the .bit file with the LDP button set)
- Run `make flash` inside the prefered program folder.
- You can cancel the output and watch it with `minicom -D /dev/ttyUSB0 -b 115200`

On the Arty A7 the UART Pins are (Arduino-like Pinout):
- IO0 -> RX
- IO1 -> TX

### Generate .coe file

The coe files, needed for simulation can be created like this:

```
python3 makehex.py 01_hello_world_stdlib/FLASH.bin 4096 > ramdata.coe
```

### Used IP

The following describes all the used IP and its parameters


#### clk_wiz_0

Clock generation IP, generating 4 clocks:

- clk_in1, 100 MHz, Arty A7 base clock
- clk_out20, 20 MHz - General CPU Clock
- clk_out_inv20, 20 MHz, 180° shifted - BRAM Clock
- clk_out200, 200 MHz - SDRAM reference clock
- clk_out166, 166.6667 MHz - SDRAM system clock


#### blk_mem_gen_0

Main memory for the CPU. 

- Native True Dual Port RAM
- Byte Write Enable: True
- Byte Size: 8 bits
- Algorithm: Minimum Area
- Width: 32 Bit
- Depth: 4096
- Primitives Output Register: False
- Core output Register: False
- RSTA Pin: True


#### cache_ctrl_mem

Cache control memory to save tags and valid bits

- Native Single Port RAM
- Byte Write Enable: True
- Byte Size: 8 bits
- Algorithm: Minimum Area
- Width: 32 Bit
- Depth: 64
- Primitives Output Register: False
- Core output Register: False
- RSTA Pin: True


#### cache_mem

- Native Single Port RAM
- Byte Write Enable: True
- Byte Size: 8 bits
- Algorithm: Minimum Area
- Width: 128 Bit
- Depth: 64
- Primitives Output Register: False
- Core output Register: False
- RSTA Pin: True


#### mig_7series_0

- DDR3 SDRAM
- Clock period: 3000 ps (333.33 MHz)
- PHY to Controller Clock Ratio: 4:1
- Memory Part: MT41K128M16XX-15E
- Memory Voltage: 1.35V
- Data Width: 16
- Data Mask: enabled
- Ordering: strict
- Input clock period: 6000 ps (166.667 MHz)
- Read Burst Type and Length: Sequential
- Output Driver Impedance Control: RZQ/6
- Controller Chip Select Pin: Enable
- Memory Address Mapping: Bank | Row | Column
- System Clock: no buffer
- Reference clock: no buffer
- System Reset Polarity: Active high (same as CPU)
- Debug Signals: Off
- Internal Vref: enable
- IO Power reduction: enabled
- Internal Termination Impedance: 50 Ohms
- Fixed pinout: As described in Arty A7 Schematic

https://digilent.com/reference/programmable-logic/arty-a7/reference-manual#memory

## Helpful tcl commands

To recreate tcl scripts from the IP inside vivado, just run:

```tcl
write_ip_tcl -force -multiple_files [get_ips] /home/user/ip_out/
```

This will create a tcl-script for each IP with the same name as the instance.

Xilinx tcl reference: https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug835-vivado-tcl-commands.pdf
