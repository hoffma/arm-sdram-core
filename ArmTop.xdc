# RST ist BTN3, LDP ist SW0
set_property PACKAGE_PIN B8 [get_ports EXT_RST]
set_property PACKAGE_PIN A8 [get_ports EXT_LDP]
# auf JB1
# set_property PACKAGE_PIN AA9 [get_ports EXT_RST]
# set_property PACKAGE_PIN AB11 [get_ports EXT_LDP]

set_property PACKAGE_PIN E3 [get_ports EXT_CLK]
# TXD ist Pin JA2, RXD ist Pin JA3 Bei PMOD-Connector JA1
set_property PACKAGE_PIN V15 [get_ports EXT_RXD]
set_property PACKAGE_PIN U16 [get_ports EXT_TXD]

set_property PACKAGE_PIN H5 [get_ports {EXT_LED[0]}]
set_property PACKAGE_PIN J5 [get_ports {EXT_LED[1]}]
set_property PACKAGE_PIN T9 [get_ports {EXT_LED[2]}]
set_property PACKAGE_PIN T10 [get_ports {EXT_LED[3]}]
set_property PACKAGE_PIN F6 [get_ports {EXT_LED[4]}]
set_property PACKAGE_PIN J4 [get_ports {EXT_LED[5]}]
set_property PACKAGE_PIN J2 [get_ports {EXT_LED[6]}]
set_property PACKAGE_PIN H6 [get_ports {EXT_LED[7]}]

set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_RST]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_LDP]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_RXD]
set_property IOSTANDARD LVCMOS33 [get_ports EXT_TXD]

# create_clock -period 10.000 -name EXT_CLK -waveform {0.000 5.000} [get_ports -filter { NAME =~  "*EXT_CLK*" && DIRECTION == "IN" }]


########### OLD ZEDBOARD XDC
## RST ist BTNR, LDP ist SW0
# set_property PACKAGE_PIN R18 [get_ports EXT_RST]
#set_property PACKAGE_PIN G22 [get_ports EXT_RST]
#set_property PACKAGE_PIN F22 [get_ports EXT_LDP]
## auf JB1
## set_property PACKAGE_PIN AA9 [get_ports EXT_RST]
## set_property PACKAGE_PIN AB11 [get_ports EXT_LDP]

#set_property PACKAGE_PIN Y9 [get_ports EXT_CLK]
## TXD ist Pin JA2, RXD ist Pin JA3 Bei PMOD-Connector JA1
#set_property PACKAGE_PIN Y10 [get_ports EXT_RXD]
#set_property PACKAGE_PIN AA11 [get_ports EXT_TXD]

#set_property PACKAGE_PIN T22 [get_ports {EXT_LED[0]}]
#set_property PACKAGE_PIN T21 [get_ports {EXT_LED[1]}]
#set_property PACKAGE_PIN U22 [get_ports {EXT_LED[2]}]
#set_property PACKAGE_PIN U21 [get_ports {EXT_LED[3]}]
#set_property PACKAGE_PIN V22 [get_ports {EXT_LED[4]}]
#set_property PACKAGE_PIN W22 [get_ports {EXT_LED[5]}]
#set_property PACKAGE_PIN U19 [get_ports {EXT_LED[6]}]
#set_property PACKAGE_PIN U14 [get_ports {EXT_LED[7]}]

#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[7]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[6]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[5]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {EXT_LED[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports EXT_RST]
#set_property IOSTANDARD LVCMOS33 [get_ports EXT_LDP]
#set_property IOSTANDARD LVCMOS33 [get_ports EXT_CLK]
#set_property IOSTANDARD LVCMOS33 [get_ports EXT_RXD]
#set_property IOSTANDARD LVCMOS33 [get_ports EXT_TXD]







