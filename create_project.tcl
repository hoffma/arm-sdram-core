# original source:
# https://github.com/stnolting/neorv32-setups/blob/main/vivado/arty-a7-test-setup/create_project.tcl

# get path of this script; https://stackoverflow.com/a/23285361
set script_path [ file dirname [ file normalize [ info script ] ] ]
set origin_dir $script_path
puts $origin_dir

set board "arty-a7-35"

# Create and clear output directory
set outputdir "[file normalize "$origin_dir/work"]"
file mkdir $outputdir

set files [glob -nocomplain "$outputdir/*"]
if {[llength $files] != 0} {
    puts "deleting contents of $outputdir"
    file delete -force {*}[glob -directory $outputdir *]; # clear folder contents
} else {
    puts "$outputdir is empty"
}

switch $board {
  "arty-a7-35" {
    set a7part "xc7a35ticsg324-1L"
    set a7prj ARM-SDRAM-${board}
  }
}

# Create project
create_project -part $a7part $a7prj $outputdir

set_property board_part digilentinc.com:${board}:part0:1.0 [current_project]
set_property target_language VHDL [current_project]

# Set project properties
set obj [current_project]
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "enable_vhdl_2008" -value "1" -objects $obj
set_property -name "part" -value "xc7a35ticsg324-1L" -objects $obj
set_property -name "revised_directory_structure" -value "1" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj

# waveform configuration
import_files [glob $script_path/*.wcfg]

# Core Sources
import_files [glob $script_path/srcs/ArmCore/*.vhd]
import_files [glob $script_path/srcs/Sdram/*.vhd]
import_files [glob $script_path/srcs/*.vhd]

set fileset_constraints [glob $script_path/*.xdc]

set fileset_sim [glob $script_path/srcs/sim/*.vhd]
#
## Constraints
import_files -fileset constrs_1 $fileset_constraints

## Simulation-only
import_files -fileset sim_1 $fileset_sim
# Add DDR3 definition
import_files -fileset sim_1 $script_path/srcs/sim/DDR3_SDRAM_Verilog_Model/ddr3.v
import_files -fileset sim_1 $script_path/srcs/sim/DDR3_SDRAM_Verilog_Model/1024Mb_ddr3_parameters.vh
import_files -fileset sim_1 $script_path/srcs/sim/DDR3_SDRAM_Verilog_Model/2048Mb_ddr3_parameters.vh
import_files -fileset sim_1 $script_path/srcs/sim/DDR3_SDRAM_Verilog_Model/4096Mb_ddr3_parameters.vh
import_files -fileset sim_1 $script_path/srcs/sim/DDR3_SDRAM_Verilog_Model/8192Mb_ddr3_parameters.vh

# generate all needed IP's
source $script_path/ip_scripts/blk_mem_gen_0.tcl
source $script_path/ip_scripts/clk_wiz_0.tcl
source $script_path/ip_scripts/cache_mem.tcl
source $script_path/ip_scripts/cache_ctrl_mem.tcl
source $script_path/ip_scripts/mig_7series_0.tcl

upgrade_ip [get_ips -all]

synth_design -top MyArmTop -name synth_1
# Run synthesis, implementation and bitstream generation
launch_runs impl_1 -to_step write_bitstream -jobs 4
wait_on_run impl_1
