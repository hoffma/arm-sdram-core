#include <stdint.h>

void write_to_uart(char *string, int len);
void print_uint32(uint32_t number);
void println_uint32(uint32_t number);
void println(char *string, int len);

char buf[255];

void delay() {
    for (int i=0; i<1000000; i++)
        asm ("");
}
void minidelay() {
    for (int i=0; i<3; i++)
        asm ("");
}

int main () {	
    volatile uint32_t *dummy_reg0 = (uint32_t *) 0x80005004;
    volatile uint32_t *dummy_reg1 = (uint32_t *) 0x80005008;

    volatile uint32_t *sdram_addr = (uint32_t *) 0x10002008;

    for(uint32_t i = 0; i < 10; i++)
        minidelay();

    while(1) {
        /* store some data in sdram */
        for(uint32_t i = 0; i < 10; i += 4) {
            *(sdram_addr + i + 0x1000) = 16 + i + 1;
            *(sdram_addr + i + 0x200) = 4096+ i + 1;
        }
        /* read data back into the processor */
        for(uint32_t i = 0; i < 10; i += 4) {
            uint32_t tmp = *(sdram_addr + i + 0x200);
            uint32_t tmp2 = *(sdram_addr + i + 0x1000);

            *dummy_reg0 = tmp;
            *dummy_reg1 = tmp2;
        }
        minidelay();
        minidelay();
        minidelay();
    }
    return 0;
}

/* Gibt n/10 zurück */
uint32_t divu10(uint32_t n) {
    uint32_t q, r;
    q = (n >> 1) + (n >> 2);
    q = q + (q >> 4);
    q = q + (q >> 8);
    q = q + (q >> 16);
    q = q >> 3;
    r = n - (((q << 2) + q) << 1); return q + (r > 9);
}

void println_uint32(uint32_t number) {
    print_uint32(number);
    write_to_uart("\r\n", 2);
}

/* Gibt die gegebene Zahl, ohne Linebreak, über uart aus */
void print_uint32(uint32_t number) {
    int len = 0;
    char tmpbuf[255];
    while(number > 0) {
        /*int rem = number % 10;*/
        uint32_t tmp = divu10(number);
        uint32_t rem = number - (tmp*10);
        tmpbuf[len++] = rem + '0';
        number = divu10(number);
    }


    if(len == 0) {
        write_to_uart("0", 1);
    }
    else {
        for(int i = (len-1); i >= 0; i--) {
            buf[(len-1)-i] = tmpbuf[i];
        }
        write_to_uart(buf, len);
    }
}

void println(char *string, int len) {
    write_to_uart(string, len);
    write_to_uart("\r\n", 2);
}

void write_to_uart(char *string, int len)
{
    //Speicheradresse des RS232-Senderegisters
    char *RS232_TRM = (char *) 0x80000004;

    //Speicheradresse des RS232-Statusregisters
    volatile int *RS232_STATUS = (int *) 0x80000008;

    //Maske zum Ausfiltern des Busy-Bits des RS232-Senders
    const int RS232_BUSY_MASK = 0x00000010;

    for(int i=0; i<len; i++){
        //warten, bis das Busy-Bit nicht mehr gesetzt ist
        while(*RS232_STATUS & RS232_BUSY_MASK){			
            ; //warten
        }
        //Character in Senderegister schreiben
        *RS232_TRM = string[i];			
    }

}
