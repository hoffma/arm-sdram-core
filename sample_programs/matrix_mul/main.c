#include <stdint.h>

void write_to_uart(char *string, int len);
void print_uint32(uint32_t number);
void println(char *string, int len);
void mmm2(uint32_t n);

char buf[255];

void delay() {
    for (int i=0; i<1000000; i++)
        asm ("");
}

int main () {	

    for(int i = 0; i < 20; i++)
        delay();

    mmm2(10);
    
    while(1){
        delay();
        break;
    }
    return 0;
}

void mmm2(uint32_t n)
{
    /*uint32_t A[n*n], B[n*n], C[n*n];*/
    volatile uint32_t *A = (uint32_t *) 0x10100000;
    volatile uint32_t *B = (uint32_t *) 0x10200000;
    volatile uint32_t *C = (uint32_t *) 0x10300000;

    for(uint32_t i = 0; i < (n*n); i++) {
        A[i] = 2;
        B[i] = 3;
        C[i] = 0;
    }

    write_to_uart("S\r\n", 3);
    for(uint32_t i = 0; i < n; ++i) {
        for (uint32_t j = 0; j < n; ++j) {
            for(uint32_t k = 0; k < n; ++k) {
                C[n*i + j] += A[n*i + k]*B[n*k + j];
            }
        }
    }
    write_to_uart("E\r\n", 3);

    for(uint32_t i = 0; i < n; ++i) {
        for (uint32_t j = 0; j < n; ++j) {
            print_uint32(C[n*i + j]);
            write_to_uart(" ", 1);
        }
        write_to_uart("\r\n", 2);
    }
}

/* Gibt n/10 zurück */
uint32_t divu10(uint32_t n) {
    uint32_t q, r;
    q = (n >> 1) + (n >> 2);
    q = q + (q >> 4);
    q = q + (q >> 8);
    q = q + (q >> 16);
    q = q >> 3;
    r = n - (((q << 2) + q) << 1); return q + (r > 9);
}

/* Gibt die gegebene Zahl, ohne Linebreak, über uart aus */
void print_uint32(uint32_t number) {
    int len = 0;
    char tmpbuf[255];
    while(number > 0) {
        /*int rem = number % 10;*/
        uint32_t tmp = divu10(number);
        uint32_t rem = number - (tmp*10);
        tmpbuf[len++] = rem + '0';
        number = divu10(number);
    }


    if(len == 0) {
        write_to_uart("0", 1);
    }
    else {
        for(int i = (len-1); i >= 0; i--) {
            buf[(len-1)-i] = tmpbuf[i];
        }
        write_to_uart(buf, len);
    }
}

void println(char *string, int len) {
    write_to_uart(string, len);
    write_to_uart("\r\n", 2);
}

void write_to_uart(char *string, int len)
{
    //Speicheradresse des RS232-Senderegisters
    char *RS232_TRM = (char *) 0x80000004;

    //Speicheradresse des RS232-Statusregisters
    volatile int *RS232_STATUS = (int *) 0x80000008;

    //Maske zum Ausfiltern des Busy-Bits des RS232-Senders
    const int RS232_BUSY_MASK = 0x00000010;

    for(int i=0; i<len; i++){
        //warten, bis das Busy-Bit nicht mehr gesetzt ist
        while(*RS232_STATUS & RS232_BUSY_MASK){			
            ; //warten
        }
        //Character in Senderegister schreiben
        *RS232_TRM = string[i];			
    }

}
