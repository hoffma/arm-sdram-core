#include <stdint.h>

void mmm2(uint32_t n);

void delay() {
    for (int i=0; i<1000000; i++)
        asm ("");
}

int main () {	
    volatile uint32_t *sdram_start = (uint32_t *) 0x10000000;
    volatile uint32_t *dummy_reg = (uint32_t *) 0x80005004;

    *dummy_reg = 0x55;
    mmm2(3);
    *dummy_reg = 0xaa;
    
    while(1){
        delay();
    }
    return 0;
}

void mmm2(uint32_t n)
{
    /*uint32_t A[n*n], B[n*n], C[n*n];*/
    volatile uint32_t *A = (uint32_t *) 0x10100000;
    volatile uint32_t *B = (uint32_t *) 0x10200000;
    volatile uint32_t *C = (uint32_t *) 0x10300000;

    volatile uint32_t *dummy_reg = (uint32_t *) 0x80005004;

    for(uint32_t i = 0; i < (n*n); i++) {
        A[i] = 3;
        B[i] = 2;
        C[i] = 0;
    }

    for(uint32_t i = 0; i < n; ++i) {
        for (uint32_t j = 0; j < n; ++j) {
            for(uint32_t k = 0; k < n; ++k) {
                C[n*i + j] += A[n*i + k]*B[n*k + j];
            }
        }
    }

    /* "print" to a dummy register */
    for(uint32_t i = 0; i < n; ++i) {
        for(uint32_t j = 0; j < n; ++j) {
            *dummy_reg = C[n*i + j];
        }
    }
}

