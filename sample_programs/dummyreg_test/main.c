#include <stdint.h>

#define OFFSET 0x1000

void short_delay() {
    for (int i=0; i<10; i++)
        asm ("");
}

int main () {	
    volatile uint32_t *sdram_start = (uint32_t *) 0x10000000;
    volatile uint32_t *dummy_reg_base = (uint32_t *) 0x80005000;

    for(uint32_t j = 0; j < 4; j++) {
        *(dummy_reg_base+j) = j+11;
    }

    short_delay();

    for(uint32_t j = 0; j < 4; j++) {
        *(sdram_start+j) = *(dummy_reg_base+j);
    }


    while(1){
        asm("nop");
    }
    return 0;
}
