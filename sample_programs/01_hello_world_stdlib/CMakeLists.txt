cmake_minimum_required(VERSION 3.10.0)

project(hello_world)
enable_language(C ASM CXX)

set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS OFF)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_C_COMPILER "arm-none-eabi-gcc")
set(CMAKE_ASM_COMPILER "arm-none-eabi-as")
set(CMAKE_OBJCOPY "arm-none-eabi-objcopy")

# startup script
set(STARTUP_SCRIPT "src/arm_startup.s")
# linker script
set(LINKER_SCRIPT "../sections.lds")

file(GLOB_RECURSE SOURCES src/*.c src/*.cpp)

set(EXECUTABLE ${PROJECT_NAME}.out)

add_executable(${EXECUTABLE}
                ${SOURCES}
                ${STARTUP_SCRIPT})

target_compile_options(${EXECUTABLE} PRIVATE
    -mlittle-endian
    -mcpu=arm8
    -mfloat-abi=soft
    -Wall
    )

target_link_options(${EXECUTABLE} PRIVATE
    -T${LINKER_SCRIPT}
    -mno-thumb-interwork
    -marm
    -ffreestanding
    -nostdlib
    -lgcc
    )
