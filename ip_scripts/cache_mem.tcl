##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2021.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source cache_mem.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./ArmCacheProcessor/ArmCacheProcessor.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project ArmCacheProcessor ArmCacheProcessor -part xc7a35ticsg324-1L
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:blk_mem_gen:8.4 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP cache_mem
##################################################################

set cache_mem [create_ip -name blk_mem_gen -vendor xilinx.com -library ip -version 8.4 -module_name cache_mem]

set_property -dict { 
  CONFIG.Enable_32bit_Address {false}
  CONFIG.Use_Byte_Write_Enable {true}
  CONFIG.Byte_Size {8}
  CONFIG.Write_Width_A {128}
  CONFIG.Write_Depth_A {64}
  CONFIG.Read_Width_A {128}
  CONFIG.Write_Width_B {128}
  CONFIG.Read_Width_B {128}
  CONFIG.Register_PortA_Output_of_Memory_Primitives {false}
  CONFIG.Use_RSTA_Pin {false}
  CONFIG.EN_SAFETY_CKT {false}
} [get_ips cache_mem]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $cache_mem

##################################################################

