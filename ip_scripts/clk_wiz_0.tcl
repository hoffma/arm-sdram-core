##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2021.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source clk_wiz_0.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./ArmCacheProcessor/ArmCacheProcessor.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project ArmCacheProcessor ArmCacheProcessor -part xc7a35ticsg324-1L
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:clk_wiz:6.0 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# CREATE IP clk_wiz_0
##################################################################

set clk_wiz_0 [create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_wiz_0]

set_property -dict { 
  CONFIG.CLKOUT2_USED {true}
  CONFIG.CLKOUT3_USED {true}
  CONFIG.CLKOUT4_USED {true}
  CONFIG.NUM_OUT_CLKS {4}
  CONFIG.CLK_OUT1_PORT {clk_out20}
  CONFIG.CLK_OUT2_PORT {clk_out_inv20}
  CONFIG.CLK_OUT3_PORT {clk_out200}
  CONFIG.CLK_OUT4_PORT {clk_out166}
  CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {20.000}
  CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {20.000}
  CONFIG.CLKOUT2_REQUESTED_PHASE {180.000}
  CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {200.000}
  CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {166.66667}
  CONFIG.MMCM_DIVCLK_DIVIDE {1}
  CONFIG.MMCM_CLKFBOUT_MULT_F {10.000}
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {50.000}
  CONFIG.MMCM_CLKOUT1_DIVIDE {50}
  CONFIG.MMCM_CLKOUT1_PHASE {180.000}
  CONFIG.MMCM_CLKOUT2_DIVIDE {5}
  CONFIG.MMCM_CLKOUT3_DIVIDE {6}
  CONFIG.CLKOUT1_JITTER {183.243}
  CONFIG.CLKOUT1_PHASE_ERROR {98.575}
  CONFIG.CLKOUT2_JITTER {183.243}
  CONFIG.CLKOUT2_PHASE_ERROR {98.575}
  CONFIG.CLKOUT3_JITTER {114.829}
  CONFIG.CLKOUT3_PHASE_ERROR {98.575}
  CONFIG.CLKOUT4_JITTER {118.758}
  CONFIG.CLKOUT4_PHASE_ERROR {98.575}
} [get_ips clk_wiz_0]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $clk_wiz_0

##################################################################

