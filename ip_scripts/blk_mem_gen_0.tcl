##################################################################
# CHECK VIVADO VERSION
##################################################################

set scripts_vivado_version 2021.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
  catch {common::send_msg_id "IPS_TCL-100" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_ip_tcl to create an updated script."}
  return 1
}

##################################################################
# START
##################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source blk_mem_gen_0.tcl
# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./ArmCacheProcessor/ArmCacheProcessor.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
  create_project ArmCacheProcessor ArmCacheProcessor -part xc7a35ticsg324-1L
  set_property target_language VHDL [current_project]
  set_property simulator_language Mixed [current_project]
}

##################################################################
# CHECK IPs
##################################################################

set bCheckIPs 1
set bCheckIPsPassed 1
if { $bCheckIPs == 1 } {
  set list_check_ips { xilinx.com:ip:blk_mem_gen:8.4 }
  set list_ips_missing ""
  common::send_msg_id "IPS_TCL-1001" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

  foreach ip_vlnv $list_check_ips {
  set ip_obj [get_ipdefs -all $ip_vlnv]
  if { $ip_obj eq "" } {
    lappend list_ips_missing $ip_vlnv
    }
  }

  if { $list_ips_missing ne "" } {
    catch {common::send_msg_id "IPS_TCL-105" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
    set bCheckIPsPassed 0
  }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "IPS_TCL-102" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 1
}

##################################################################
# blk_mem_gen_0 FILES
##################################################################

proc write_blk_mem_gen_ramdata { blk_mem_gen_ramdata_filepath } {
  set blk_mem_gen_ramdata [open $blk_mem_gen_ramdata_filepath  w+]

  puts $blk_mem_gen_ramdata {memory_initialization_radix = 16;}
  puts $blk_mem_gen_ramdata {memory_initialization_vector = }
  puts $blk_mem_gen_ramdata {ea000006}
  puts $blk_mem_gen_ramdata {e1b0f00e}
  puts $blk_mem_gen_ramdata {e1b0f00e}
  puts $blk_mem_gen_ramdata {e25ef004}
  puts $blk_mem_gen_ramdata {e25ef008}
  puts $blk_mem_gen_ramdata {e1a00000}
  puts $blk_mem_gen_ramdata {e25ef004}
  puts $blk_mem_gen_ramdata {e25ef004}
  puts $blk_mem_gen_ramdata {e3a0da02}
  puts $blk_mem_gen_ramdata {e92d4070}
  puts $blk_mem_gen_ramdata {eb000010}
  puts $blk_mem_gen_ramdata {e3a04001}
  puts $blk_mem_gen_ramdata {e3a05000}
  puts $blk_mem_gen_ramdata {e59f6030}
  puts $blk_mem_gen_ramdata {e1a00004}
  puts $blk_mem_gen_ramdata {eb000027}
  puts $blk_mem_gen_ramdata {e0944004}
  puts $blk_mem_gen_ramdata {e0a55005}
  puts $blk_mem_gen_ramdata {e3a01008}
  puts $blk_mem_gen_ramdata {e1a00006}
  puts $blk_mem_gen_ramdata {eb000017}
  puts $blk_mem_gen_ramdata {eb000005}
  puts $blk_mem_gen_ramdata {e3550000}
  puts $blk_mem_gen_ramdata {03540a01}
  puts $blk_mem_gen_ramdata {83a04001}
  puts $blk_mem_gen_ramdata {83a05000}
  puts $blk_mem_gen_ramdata {eafffff2}
  puts $blk_mem_gen_ramdata {0000039e}
  puts $blk_mem_gen_ramdata {e59f3008}
  puts $blk_mem_gen_ramdata {e2533001}
  puts $blk_mem_gen_ramdata {1afffffd}
  puts $blk_mem_gen_ramdata {e1a0f00e}
  puts $blk_mem_gen_ramdata {000f4240}
  puts $blk_mem_gen_ramdata {e1a03120}
  puts $blk_mem_gen_ramdata {e08330a0}
  puts $blk_mem_gen_ramdata {e0833223}
  puts $blk_mem_gen_ramdata {e0833423}
  puts $blk_mem_gen_ramdata {e0833823}
  puts $blk_mem_gen_ramdata {e1a031a3}
  puts $blk_mem_gen_ramdata {e0832103}
  puts $blk_mem_gen_ramdata {e0400082}
  puts $blk_mem_gen_ramdata {e3500009}
  puts $blk_mem_gen_ramdata {91a00003}
  puts $blk_mem_gen_ramdata {82830001}
  puts $blk_mem_gen_ramdata {e1a0f00e}
  puts $blk_mem_gen_ramdata {e3a03000}
  puts $blk_mem_gen_ramdata {e3a02102}
  puts $blk_mem_gen_ramdata {e1530001}
  puts $blk_mem_gen_ramdata {a1a0f00e}
  puts $blk_mem_gen_ramdata {e592c008}
  puts $blk_mem_gen_ramdata {e31c0010}
  puts $blk_mem_gen_ramdata {1afffffc}
  puts $blk_mem_gen_ramdata {e7d0c003}
  puts $blk_mem_gen_ramdata {e2833001}
  puts $blk_mem_gen_ramdata {e5c2c004}
  puts $blk_mem_gen_ramdata {eafffff6}
  puts $blk_mem_gen_ramdata {e92d40f0}
  puts $blk_mem_gen_ramdata {e24ddf41}
  puts $blk_mem_gen_ramdata {e1a0500d}
  puts $blk_mem_gen_ramdata {e1a04000}
  puts $blk_mem_gen_ramdata {e3a01000}
  puts $blk_mem_gen_ramdata {e1a06005}
  puts $blk_mem_gen_ramdata {e3a0700a}
  puts $blk_mem_gen_ramdata {e3540000}
  puts $blk_mem_gen_ramdata {1a00000d}
  puts $blk_mem_gen_ramdata {e3510000}
  puts $blk_mem_gen_ramdata {03a01001}
  puts $blk_mem_gen_ramdata {059f004c}
  puts $blk_mem_gen_ramdata {0a000006}
  puts $blk_mem_gen_ramdata {e59f2048}
  puts $blk_mem_gen_ramdata {e2413001}
  puts $blk_mem_gen_ramdata {e7d60003}
  puts $blk_mem_gen_ramdata {e2533001}
  puts $blk_mem_gen_ramdata {e4c20001}
  puts $blk_mem_gen_ramdata {2afffffb}
  puts $blk_mem_gen_ramdata {e59f0030}
  puts $blk_mem_gen_ramdata {e28ddf41}
  puts $blk_mem_gen_ramdata {e8bd40f0}
  puts $blk_mem_gen_ramdata {eaffffdd}
  puts $blk_mem_gen_ramdata {e1a00004}
  puts $blk_mem_gen_ramdata {ebffffcf}
  puts $blk_mem_gen_ramdata {e0030097}
  puts $blk_mem_gen_ramdata {e0444003}
  puts $blk_mem_gen_ramdata {e2844030}
  puts $blk_mem_gen_ramdata {e4c54001}
  puts $blk_mem_gen_ramdata {e2811001}
  puts $blk_mem_gen_ramdata {e1a04000}
  puts $blk_mem_gen_ramdata {eaffffe6}
  puts $blk_mem_gen_ramdata {0000039c}
  puts $blk_mem_gen_ramdata {000003b0}
  puts $blk_mem_gen_ramdata {e92d41f0}
  puts $blk_mem_gen_ramdata {e24ddc01}
  puts $blk_mem_gen_ramdata {e1a0700d}
  puts $blk_mem_gen_ramdata {e1a04000}
  puts $blk_mem_gen_ramdata {e1a05001}
  puts $blk_mem_gen_ramdata {e3a06000}
  puts $blk_mem_gen_ramdata {e1a08007}
  puts $blk_mem_gen_ramdata {e1943005}
  puts $blk_mem_gen_ramdata {1a00000e}
  puts $blk_mem_gen_ramdata {e3560000}
  puts $blk_mem_gen_ramdata {03a01001}
  puts $blk_mem_gen_ramdata {059f006c}
  puts $blk_mem_gen_ramdata {0a000007}
  puts $blk_mem_gen_ramdata {e59f2068}
  puts $blk_mem_gen_ramdata {e2463001}
  puts $blk_mem_gen_ramdata {e7d81003}
  puts $blk_mem_gen_ramdata {e2533001}
  puts $blk_mem_gen_ramdata {e4c21001}
  puts $blk_mem_gen_ramdata {2afffffb}
  puts $blk_mem_gen_ramdata {e1a01006}
  puts $blk_mem_gen_ramdata {e59f004c}
  puts $blk_mem_gen_ramdata {e28ddc01}
  puts $blk_mem_gen_ramdata {e8bd41f0}
  puts $blk_mem_gen_ramdata {eaffffba}
  puts $blk_mem_gen_ramdata {e1a00004}
  puts $blk_mem_gen_ramdata {e1a01005}
  puts $blk_mem_gen_ramdata {e3a0200a}
  puts $blk_mem_gen_ramdata {e3a03000}
  puts $blk_mem_gen_ramdata {eb000013}
  puts $blk_mem_gen_ramdata {e2822030}
  puts $blk_mem_gen_ramdata {e1a00004}
  puts $blk_mem_gen_ramdata {e1a01005}
  puts $blk_mem_gen_ramdata {e4c72001}
  puts $blk_mem_gen_ramdata {e3a03000}
  puts $blk_mem_gen_ramdata {e3a0200a}
  puts $blk_mem_gen_ramdata {eb00000c}
  puts $blk_mem_gen_ramdata {e2866001}
  puts $blk_mem_gen_ramdata {e1a04000}
  puts $blk_mem_gen_ramdata {e1a05001}
  puts $blk_mem_gen_ramdata {eaffffde}
  puts $blk_mem_gen_ramdata {0000039c}
  puts $blk_mem_gen_ramdata {000003b0}
  puts $blk_mem_gen_ramdata {e92d4010}
  puts $blk_mem_gen_ramdata {ebffffa6}
  puts $blk_mem_gen_ramdata {e8bd4010}
  puts $blk_mem_gen_ramdata {e3a01002}
  puts $blk_mem_gen_ramdata {e59f0000}
  puts $blk_mem_gen_ramdata {eaffffa2}
  puts $blk_mem_gen_ramdata {000003a4}
  puts $blk_mem_gen_ramdata {e3530000}
  puts $blk_mem_gen_ramdata {03520000}
  puts $blk_mem_gen_ramdata {1a000004}
  puts $blk_mem_gen_ramdata {e3510000}
  puts $blk_mem_gen_ramdata {03500000}
  puts $blk_mem_gen_ramdata {13e01000}
  puts $blk_mem_gen_ramdata {13e00000}
  puts $blk_mem_gen_ramdata {ea000052}
  puts $blk_mem_gen_ramdata {e24dd008}
  puts $blk_mem_gen_ramdata {e92d6000}
  puts $blk_mem_gen_ramdata {eb000003}
  puts $blk_mem_gen_ramdata {e59de004}
  puts $blk_mem_gen_ramdata {e28dd008}
  puts $blk_mem_gen_ramdata {e8bd000c}
  puts $blk_mem_gen_ramdata {e12fff1e}
  puts $blk_mem_gen_ramdata {e1510003}
  puts $blk_mem_gen_ramdata {01500002}
  puts $blk_mem_gen_ramdata {e92d47f0}
  puts $blk_mem_gen_ramdata {e1a04000}
  puts $blk_mem_gen_ramdata {33a00000}
  puts $blk_mem_gen_ramdata {e1a05001}
  puts $blk_mem_gen_ramdata {e59dc020}
  puts $blk_mem_gen_ramdata {31a01000}
  puts $blk_mem_gen_ramdata {3a00003f}
  puts $blk_mem_gen_ramdata {e1a06002}
  puts $blk_mem_gen_ramdata {e3530000}
  puts $blk_mem_gen_ramdata {116f2f13}
  puts $blk_mem_gen_ramdata {016f2f16}
  puts $blk_mem_gen_ramdata {02822020}
  puts $blk_mem_gen_ramdata {e3550000}
  puts $blk_mem_gen_ramdata {e1a07003}
  puts $blk_mem_gen_ramdata {016f3f14}
  puts $blk_mem_gen_ramdata {02833020}
  puts $blk_mem_gen_ramdata {116f3f15}
  puts $blk_mem_gen_ramdata {e0422003}
  puts $blk_mem_gen_ramdata {e1a09217}
  puts $blk_mem_gen_ramdata {e242a020}
  puts $blk_mem_gen_ramdata {e1899a16}
  puts $blk_mem_gen_ramdata {e262e020}
  puts $blk_mem_gen_ramdata {e1899e36}
  puts $blk_mem_gen_ramdata {e1a08216}
  puts $blk_mem_gen_ramdata {e1550009}
  puts $blk_mem_gen_ramdata {01540008}
  puts $blk_mem_gen_ramdata {33a00000}
  puts $blk_mem_gen_ramdata {31a01000}
  puts $blk_mem_gen_ramdata {3a000005}
  puts $blk_mem_gen_ramdata {e3a00001}
  puts $blk_mem_gen_ramdata {e1a01a10}
  puts $blk_mem_gen_ramdata {e0544008}
  puts $blk_mem_gen_ramdata {e1811e30}
  puts $blk_mem_gen_ramdata {e1a00210}
  puts $blk_mem_gen_ramdata {e0c55009}
  puts $blk_mem_gen_ramdata {e3520000}
  puts $blk_mem_gen_ramdata {0a000021}
  puts $blk_mem_gen_ramdata {e1a060a8}
  puts $blk_mem_gen_ramdata {e1866f89}
  puts $blk_mem_gen_ramdata {e1a070a9}
  puts $blk_mem_gen_ramdata {e1a08002}
  puts $blk_mem_gen_ramdata {ea000007}
  puts $blk_mem_gen_ramdata {e0543006}
  puts $blk_mem_gen_ramdata {e0c59007}
  puts $blk_mem_gen_ramdata {e0933003}
  puts $blk_mem_gen_ramdata {e0a99009}
  puts $blk_mem_gen_ramdata {e2934001}
  puts $blk_mem_gen_ramdata {e2a95000}
  puts $blk_mem_gen_ramdata {e2588001}
  puts $blk_mem_gen_ramdata {0a000006}
  puts $blk_mem_gen_ramdata {e1550007}
  puts $blk_mem_gen_ramdata {01540006}
  puts $blk_mem_gen_ramdata {2afffff4}
  puts $blk_mem_gen_ramdata {e0944004}
  puts $blk_mem_gen_ramdata {e0a55005}
  puts $blk_mem_gen_ramdata {e2588001}
  puts $blk_mem_gen_ramdata {1afffff8}
  puts $blk_mem_gen_ramdata {e1a03234}
  puts $blk_mem_gen_ramdata {e1833e15}
  puts $blk_mem_gen_ramdata {e1a06235}
  puts $blk_mem_gen_ramdata {e1833a35}
  puts $blk_mem_gen_ramdata {e0900004}
  puts $blk_mem_gen_ramdata {e1a04003}
  puts $blk_mem_gen_ramdata {e1a03216}
  puts $blk_mem_gen_ramdata {e1833a14}
  puts $blk_mem_gen_ramdata {e1a02214}
  puts $blk_mem_gen_ramdata {e1833e34}
  puts $blk_mem_gen_ramdata {e0a11005}
  puts $blk_mem_gen_ramdata {e0500002}
  puts $blk_mem_gen_ramdata {e1a05006}
  puts $blk_mem_gen_ramdata {e0c11003}
  puts $blk_mem_gen_ramdata {e35c0000}
  puts $blk_mem_gen_ramdata {11cc40f0}
  puts $blk_mem_gen_ramdata {e8bd87f0}
  puts $blk_mem_gen_ramdata {e12fff1e}
  puts $blk_mem_gen_ramdata {48200030}
  puts $blk_mem_gen_ramdata {4f4c4c45}
  puts $blk_mem_gen_ramdata {00000a0d}
  puts $blk_mem_gen_ramdata {7ffffec0}
  puts $blk_mem_gen_ramdata {00000001}

  flush $blk_mem_gen_ramdata
  close $blk_mem_gen_ramdata
}

##################################################################
# CREATE IP blk_mem_gen_0
##################################################################

set blk_mem_gen_0 [create_ip -name blk_mem_gen -vendor xilinx.com -library ip -version 8.4 -module_name blk_mem_gen_0]

write_blk_mem_gen_ramdata  [file join [get_property IP_DIR [get_ips blk_mem_gen_0]] ramdata.coe]
set_property -dict { 
  CONFIG.Memory_Type {True_Dual_Port_RAM}
  CONFIG.Enable_32bit_Address {false}
  CONFIG.Use_Byte_Write_Enable {true}
  CONFIG.Byte_Size {8}
  CONFIG.Algorithm {Minimum_Area}
  CONFIG.Primitive {8kx2}
  CONFIG.Assume_Synchronous_Clk {false}
  CONFIG.Write_Width_A {32}
  CONFIG.Write_Depth_A {4096}
  CONFIG.Read_Width_A {32}
  CONFIG.Operating_Mode_A {WRITE_FIRST}
  CONFIG.Write_Width_B {32}
  CONFIG.Read_Width_B {32}
  CONFIG.Enable_B {Use_ENB_Pin}
  CONFIG.Register_PortA_Output_of_Memory_Primitives {false}
  CONFIG.Register_PortB_Output_of_Memory_Primitives {false}
  CONFIG.Register_PortB_Output_of_Memory_Core {false}
  CONFIG.Use_REGCEB_Pin {false}
  CONFIG.Load_Init_File {true}
  CONFIG.Coe_File {ramdata.coe}
  CONFIG.Fill_Remaining_Memory_Locations {false}
  CONFIG.Use_RSTA_Pin {true}
  CONFIG.Use_RSTB_Pin {true}
  CONFIG.Port_B_Clock {100}
  CONFIG.Port_B_Write_Rate {50}
  CONFIG.Port_B_Enable_Rate {100}
  CONFIG.EN_SAFETY_CKT {true}
} [get_ips blk_mem_gen_0]

set_property -dict { 
  GENERATE_SYNTH_CHECKPOINT {1}
} $blk_mem_gen_0

##################################################################

