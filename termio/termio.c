#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/termios.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>

#define DTDEVROOT  "DTDEVROOT"
#define TTY        "/unit/dev/term/a"

void read_uart(int fdtty);

int main (int ac, char **av)
{
  int i;
  int cnt;
  int fd;
  int fdtty;
  char devstr[1024];
  struct termios term_attr;
  speed_t speed;
  tcflag_t old_flags;
  FILE *tty;
  unsigned char outbuf[16384] = {0};
  unsigned char inbuf[16384];
  size_t file_size;
  struct stat finfo;
  size_t write_size = atoi(av[3]);
  size_t bytes_written = 0;
  int readback_count = 0;
  int verify_success = 1;
  
  printf("av[1]: %s, av[2]: %s\n", av[1], av[2]);

#ifdef UT  
  fprintf(stderr, "utdevroot = %s\n", getenv(DTDEVROOT));

  strcpy(devstr, getenv(DTDEVROOT));
  strcat(devstr, TTY);
#endif

  /*strcpy(devstr,"/dev/ttyUSB0");*/
  strcpy(devstr, av[2]);

  fprintf (stderr, "opening tty device %s\n", devstr);

  if ((fdtty = open(devstr, O_RDWR)) < 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  tty = fdopen(fdtty, "r+");

  if(tcgetattr(fdtty, &term_attr) < 0) {
    perror("tcgetattr");
    return EXIT_FAILURE;
  }

  old_flags = term_attr.c_lflag;
  /*cfsetispeed(&term_attr, B57600);
  cfsetospeed(&term_attr, B57600);*/
  cfsetispeed(&term_attr, B115200);
  cfsetospeed(&term_attr, B115200);
  cfmakeraw(&term_attr); 

  if(tcsetattr(fdtty, TCSANOW, &term_attr) < 0) {
    perror("tcgetattr");
    return EXIT_FAILURE;
  }


  fd = open(av[1], O_RDONLY);
  fstat(fd, &finfo);
  file_size = finfo.st_size;
  
  tcflush(fdtty, TCIOFLUSH);
  printf("File size: %d write size: %d\n", file_size, write_size);  
  printf("\n###\nflash new program\n###\n");

  while ((cnt = read(fd, outbuf, file_size)) > 0) {
    bytes_written = write(fdtty, outbuf, write_size);
    printf(".");
    fflush(stdout);
    tcdrain(fdtty);
  }
  
  printf("Wrote %d bytes to tty\n", bytes_written);
  printf("verify...\n");
  while (readback_count < bytes_written)
  {  
    cnt = read(fdtty, &(inbuf[readback_count]), bytes_written);
    //write(2, inbuf, cnt);
    for (int i = 0;i<cnt;i++)
    {
	    printf(" %02X", (unsigned)inbuf[readback_count+i]);
    }
    printf(" readback %d bytes\n", cnt);
    readback_count += cnt;
  }
  for (int i = 0; i < bytes_written; i++)
  {
    if (inbuf[i] != outbuf[i])
    {
      verify_success = 0;
      printf("verify failed at %d because %02X != %02X\n", i, (unsigned)inbuf[i], (unsigned)outbuf[i]);
      break;
    }
  }
  if (verify_success)
  {
    printf("verify success!\n###UART output:\n");
  } else {
    printf("press enter to continue read from UART, else press ctrl+c");
    getchar();
    tcflush(fdtty, TCIOFLUSH);
  }
  
  read_uart(fdtty);
}

void read_uart(int fdtty)
{
  unsigned char inbuf[16384];
  int read_cnt;
  int cnt = 0;
  int write_hex;
  while ((read_cnt = read(fdtty, &(inbuf[cnt]), 1)) > 0) {
    if (read_cnt > 0)
    {
      write_hex = 0;
      // write hexadecimal representation after a newline or 32 bytes
//      if ((inbuf[cnt]) == '\n')
//      {
//        inbuf[cnt] = ' ';
//	write_hex = 1;
//      }
//      else 
//      {
        if (cnt > 31)
          write_hex = 1;
//      }
      
      // write the actual read byte
//      write(1, &(inbuf[cnt]), read_cnt);

      // write hexadecimal representation
      if (write_hex > 0)
      {
	// create column with 32 width
	for (int i = 0;i<32-cnt;i++)
	  printf(" ");

	// column seperator
        printf("|");

        for (int i = 0;i<cnt;i++)
        {
	    printf("%02X ", (unsigned)inbuf[i]);
        }
        printf("got %d bytes\n", cnt);
	cnt = 0;
      } else { 
        cnt++;
      }
    }
  } 
}
