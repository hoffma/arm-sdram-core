----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 01/06/2022 06:57:57 AM
-- Design Name: 
-- Module Name: DummyRegister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.ArmConfiguration.all;
use work.ArmTypes.all;

entity DummyRegister is
    Port ( SYS_CLK : in STD_LOGIC;
           SYS_RST : in STD_LOGIC;
           CS : in STD_LOGIC;
           DnRW : in STD_LOGIC;
           DMAS : in STD_LOGIC_VECTOR (1 downto 0);
           DA : in STD_LOGIC_VECTOR (31 downto 0);
           DDIN : in STD_LOGIC_VECTOR (31 downto 0);
           DDOUT : out STD_LOGIC_VECTOR (31 downto 0);
           DABORT : out STD_LOGIC;
           IRQ : out STD_LOGIC);
end DummyRegister;

architecture Behavioral of DummyRegister is
--------------------------------------------------------------------------------
    --	Lokale Schnittstellensignale zum Datenbus zur Realisierung der
    --	Tristate-Ausgaenge.
--------------------------------------------------------------------------------
    signal DABORT_INTERNAL	: std_logic;	
    signal DDOUT_INTERNAL	: std_logic_vector(31 downto 0);
    
    type reg_file_t is array (natural range<>) of std_logic_vector(31 downto 0);
    
    signal register_data : reg_file_t(0 to 3);
begin
--------------------------------------------------------------------------------
    --	Data Abort, wenn eine Wortzugriff mit nicht ausgerichteter Adresse
    --	erfolgt oder ein Bytezugriff auf die nicht verwendeten hochwertigen
    --	24 Registerbits erfolgt.
--------------------------------------------------------------------------------
    DABORT_INTERNAL <= '1' when ((DMAS /= DMAS_BYTE) and (DMAS /= DMAS_WORD)) or DA(1 downto 0) /= "00" else '0';
    --	Daten- und Abortsignale werden nur getrieben, wenn das Modul tatsaechlich aktiv sein soll
    DABORT	<= DABORT_INTERNAL when CS = '1' else 'Z';
    DDOUT	<= DDOUT_INTERNAL when CS = '1' and DnRW = '0' else (others => 'Z');

--------------------------------------------------------------------------------
    --	Interrupt ist bis auf weiteres undefiniert
--------------------------------------------------------------------------------
    IRQ <= 'Z';

    process(SYS_CLK, SYS_RST)
    begin
        if rising_edge(SYS_CLK) then
            -- Register lesen
            if (DnRW = '0') and (CS = '1') then
                case DA is
                    when DUMMYREG_REG0_ADDR =>
                        DDOUT_INTERNAL <= register_data(0);
                    when DUMMYREG_REG1_ADDR =>
                        DDOUT_INTERNAL <= register_data(1);
                    when DUMMYREG_REG2_ADDR =>
                        DDOUT_INTERNAL <= register_data(2);
                    when DUMMYREG_REG3_ADDR =>
                        DDOUT_INTERNAL <= register_data(3);
                    when others =>
                        null;
                end case;
            elsif (DnRW = '1') and (CS = '1') then
                case DA is
                    when DUMMYREG_REG0_ADDR =>
                        register_data(0) <= DDIN;
                    when DUMMYREG_REG1_ADDR =>
                        register_data(1) <= DDIN;
                    when DUMMYREG_REG2_ADDR =>
                        register_data(2) <= DDIN;
                    when DUMMYREG_REG3_ADDR =>
                        register_data(3) <= DDIN;
                    when others =>
                        null;
                end case;
            end if;
            
            if SYS_RST = '1' then
                for I in 0 to 3 loop
                    register_data(I) <= (others => '0');
                end loop;
            end if;
        end if;
    end process;

end Behavioral;
