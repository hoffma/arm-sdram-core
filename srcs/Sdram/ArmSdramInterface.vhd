----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 08/10/2021 01:55:35 PM
-- Design Name: 
-- Module Name: ArmSdramInterface - Behavioral
-- Target Devices: ARMv4 Prozessor
-- Description: Diese Entität bildet die Schnittstelle zwischen SDRAM und ARMv4
-- Prozessor ab. Hier wird mit einem Generate-Statement überprüft, ob der
-- CacheController instanziiert werden soll oder nicht. Entsprechend wird eine
-- Nachricht in das log geschrieben.
-- 
-- Dependencies: CacheController.vhd, Sdram.vhd und SdramDriver.vhd
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library work;
use work.ArmConfiguration.all;
use work.ArmTypes.all;

entity ArmSdramInterface is
    Generic (
        CACHE_ENABLE : boolean := FALSE
    );
    Port ( 
        SYS_CLK : in STD_LOGIC;
        SYS_RST : in STD_LOGIC;
        CS : in STD_LOGIC;
        nRW : in STD_LOGIC;
        DMAS : in STD_LOGIC_VECTOR (1 downto 0);
        ADDR : in STD_LOGIC_VECTOR (31 downto 0);
        DIN : in STD_LOGIC_VECTOR (31 downto 0);
        DOUT : out STD_LOGIC_VECTOR (31 downto 0);
        DABORT : out STD_LOGIC;
        IRQ : out STD_LOGIC;
        BUSY : out STD_LOGIC;
        -- init_complete : out STD_LOGIC;
        -- System clock; Reference clock is SDRAM_CLK (200 MHz)
        sys_clk166 : in STD_LOGIC;
        ref_clk200 : in STD_LOGIC;
        -- hardware interface
        -- Inouts
        ddr3_dq                        : inout std_logic_vector(15 downto 0);
        ddr3_dqs_p                     : inout std_logic_vector(1 downto 0);
        ddr3_dqs_n                     : inout std_logic_vector(1 downto 0);
        
        -- Outputs
        ddr3_addr                      : out   std_logic_vector(13 downto 0);
        ddr3_ba                        : out   std_logic_vector(2 downto 0);
        ddr3_ras_n                     : out   std_logic;
        ddr3_cas_n                     : out   std_logic;
        ddr3_we_n                      : out   std_logic;
        ddr3_reset_n                   : out   std_logic;
        ddr3_ck_p                      : out   std_logic_vector(0 downto 0);
        ddr3_ck_n                      : out   std_logic_vector(0 downto 0);
        ddr3_cke                       : out   std_logic_vector(0 downto 0);
        ddr3_cs_n                      : out   std_logic_vector(0 downto 0);
        ddr3_dm                        : out   std_logic_vector(1 downto 0);
        ddr3_odt                       : out   std_logic_vector(0 downto 0)
    );
end ArmSdramInterface;

architecture Behavioral of ArmSdramInterface is

    --------------------------------------------------------------------------------
    --	Lokale Schnittstellensignale zum Datenbus zur Realisierung der
    --	Tristate-Ausgaenge.
    --------------------------------------------------------------------------------
    signal DABORT_INTERNAL	: std_logic;	
    signal DDOUT_INTERNAL	: std_logic_vector(31 downto 0);
    signal ADDR_INTERNAL : std_logic_vector(27 downto 0);
    
    signal BUSY_INTERNAL : std_logic;
    
    signal prev_CS : std_logic := '0';
    signal readBusy, writeBusy, ack_sdram : std_logic;
    signal ram_writeEn, ram_readEn : std_logic;
    signal ram_init_complete : std_logic;

begin

    --------------------------------------------------------------------------------
    --	Data Abort, wenn eine Wortzugriff mit nicht ausgerichteter Adresse
    --	erfolgt oder ein Bytezugriff auf die nicht verwendeten hochwertigen
    --	24 Registerbits erfolgt.
    --------------------------------------------------------------------------------
    DABORT_INTERNAL <= '1' when ((DMAS /= DMAS_BYTE) and (DMAS /= DMAS_WORD)) or ADDR(1 downto 0) /= "00" else '0';
    
    --	Daten- und Abortsignale werden nur getrieben, wenn das Modul tatsaechlich aktiv sein soll
    DABORT	<= DABORT_INTERNAL when CS = '1' else 'Z';
    DOUT	<= DDOUT_INTERNAL when CS = '1' and nRW = '0' else (others => 'Z');
    
    --------------------------------------------------------------------------------
    --	Interrupt ist bis auf weiteres undefiniert
    --------------------------------------------------------------------------------
    IRQ <= 'Z';
    
    ADDR_INTERNAL <= ADDR(27 downto 0);
    
    BUSY    <= BUSY_INTERNAL or (not ram_init_complete);
    BUSY_INTERNAL <= (readBusy or writeBusy) when CS = '1' else '0';
    
    readBusy <= '1' when (ram_readEn = '1' and ack_sdram = '0') else '0';
    writeBusy <= '1' when (ram_writeEn = '1' and ack_sdram = '0') else '0';
    
    ram_readEn <= (not nRW) and CS;
    ram_writeEn <= nRW and CS;
    
    -- TODO: Check if this works. Adjust severity level.
    assert (CACHE_ENABLE = FALSE) report "ArmSdramInterface CONFIG NOTE: Implementing SDRAM WITHOUT cache" severity ERROR;
    assert (CACHE_ENABLE = TRUE) report "ArmSdramInterface CONFIG NOTE: Implementing SDRAM WITH cache" severity ERROR;

    
    -- instanziieren der Sdram-Entität, entsprechend des gegebenen CACHE-Flag
    cache_inst : if(CACHE_ENABLE = TRUE) generate
        inst_Sdram : entity work.Sdram(DirectMapped) port map
        (
            sys_clk => sys_clk,
            sys_rst => sys_rst,
            sys_clk166 => sys_clk166,
            ref_clk200 => ref_clk200,
            writeEn => ram_writeEn,
            readEn => ram_readEn,
            ack => ack_sdram,
            
            addr => ADDR_INTERNAL,
            din => DIN,
            dout => DDOUT_INTERNAL,
            -- hardware interface
            ddr3_addr                      => ddr3_addr,
            ddr3_ba                        => ddr3_ba,
            ddr3_cas_n                     => ddr3_cas_n,
            ddr3_ck_n                      => ddr3_ck_n,
            ddr3_ck_p                      => ddr3_ck_p,
            ddr3_cke                       => ddr3_cke,
            ddr3_ras_n                     => ddr3_ras_n,
            ddr3_reset_n                   => ddr3_reset_n,
            ddr3_we_n                      => ddr3_we_n,
            ddr3_dq                        => ddr3_dq,
            ddr3_dqs_n                     => ddr3_dqs_n,
            ddr3_dqs_p                     => ddr3_dqs_p,
            ddr3_cs_n                      => ddr3_cs_n,
            ddr3_dm                        => ddr3_dm,
            ddr3_odt                       => ddr3_odt,
            init_complete                  => ram_init_complete
        );
    end generate;
    
    no_cache_inst : if (CACHE_ENABLE = FALSE) generate
        inst_Sdram : entity work.Sdram(SimpleBuffer) port map
        (
            sys_clk => sys_clk,
            sys_rst => sys_rst,
            sys_clk166 => sys_clk166,
            ref_clk200 => ref_clk200,
            writeEn => ram_writeEn,
            readEn => ram_readEn,
            ack => ack_sdram,
            
            addr => ADDR_INTERNAL,
            din => DIN,
            dout => DDOUT_INTERNAL,
            -- hardware interface
            ddr3_addr                      => ddr3_addr,
            ddr3_ba                        => ddr3_ba,
            ddr3_cas_n                     => ddr3_cas_n,
            ddr3_ck_n                      => ddr3_ck_n,
            ddr3_ck_p                      => ddr3_ck_p,
            ddr3_cke                       => ddr3_cke,
            ddr3_ras_n                     => ddr3_ras_n,
            ddr3_reset_n                   => ddr3_reset_n,
            ddr3_we_n                      => ddr3_we_n,
            ddr3_dq                        => ddr3_dq,
            ddr3_dqs_n                     => ddr3_dqs_n,
            ddr3_dqs_p                     => ddr3_dqs_p,
            ddr3_cs_n                      => ddr3_cs_n,
            ddr3_dm                        => ddr3_dm,
            ddr3_odt                       => ddr3_odt,
            init_complete                  => ram_init_complete
        );
    end generate;


end Behavioral;
