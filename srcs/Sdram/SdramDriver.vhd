----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 09/06/2021 10:46:24 AM
-- Module Name: SdramDriver - Behavioral
-- Target Devices: ARMv4 Prozessor
-- Description: 
-- Der SDRAM-Treiber stellt die Verbidung zum MIG-generierten Controller her.
-- Der Zugriff ist auf die 32 Bit Wortbreite des ARM-Prozessor angepasst
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- inspired by https://numato.com/kb/learning-fpga-verilog-beginners-guide-part-6-ddr-sdram-a7/
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SdramDriver is
    Generic (
        DATA_BURST_WIDTH : integer := 128;
        ADDR_WIDTH : integer := 28
    );
    Port (
        sys_clk : in STD_LOGIC;
        sys_rst : in STD_LOGIC;
        -- Reference clock is always 200 MHz
        -- System clock is 166 MHz (see Arty A7 reference docs)
        sys_clk166 : in STD_LOGIC;
        ref_clk200 : in STD_LOGIC;
        
        writeEn : in STD_LOGIC;
        readEn : in STD_LOGIC;
        ack : out STD_LOGIC; -- current operation done. Is asserted for one clock cycle
        
        addr : in STD_LOGIC_VECTOR(ADDR_WIDTH-1 downto 0);
        din : in STD_LOGIC_VECTOR(DATA_BURST_WIDTH-1 downto 0);
        dout : out STD_LOGIC_VECTOR(DATA_BURST_WIDTH-1 downto 0);
        mask : in STD_LOGIC_VECTOR(15 downto 0);
        -- hardware interface
        -- Inouts
        ddr3_dq                        : inout std_logic_vector(15 downto 0);
        ddr3_dqs_p                     : inout std_logic_vector(1 downto 0);
        ddr3_dqs_n                     : inout std_logic_vector(1 downto 0);
        
        -- Outputs
        ddr3_addr                      : out   std_logic_vector(13 downto 0);
        ddr3_ba                        : out   std_logic_vector(2 downto 0);
        ddr3_ras_n                     : out   std_logic;
        ddr3_cas_n                     : out   std_logic;
        ddr3_we_n                      : out   std_logic;
        ddr3_reset_n                   : out   std_logic;
        ddr3_ck_p                      : out   std_logic_vector(0 downto 0);
        ddr3_ck_n                      : out   std_logic_vector(0 downto 0);
        ddr3_cke                       : out   std_logic_vector(0 downto 0);
        ddr3_cs_n                      : out   std_logic_vector(0 downto 0);
        ddr3_dm                        : out   std_logic_vector(1 downto 0);
        ddr3_odt                       : out   std_logic_vector(0 downto 0);
        init_complete                  : out std_Logic
    );
end SdramDriver;

architecture Behavioral of SdramDriver is
    component mig_7series_0
      port (
          ddr3_dq       : inout std_logic_vector(15 downto 0);
          ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
          ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
    
          ddr3_addr     : out   std_logic_vector(13 downto 0);
          ddr3_ba       : out   std_logic_vector(2 downto 0);
          ddr3_ras_n    : out   std_logic;
          ddr3_cas_n    : out   std_logic;
          ddr3_we_n     : out   std_logic;
          ddr3_reset_n  : out   std_logic;
          ddr3_ck_p     : out   std_logic_vector(0 downto 0);
          ddr3_ck_n     : out   std_logic_vector(0 downto 0);
          ddr3_cke      : out   std_logic_vector(0 downto 0);
          ddr3_cs_n     : out   std_logic_vector(0 downto 0);
          ddr3_dm       : out   std_logic_vector(1 downto 0);
          ddr3_odt      : out   std_logic_vector(0 downto 0);
          app_addr                  : in    std_logic_vector(27 downto 0);
          app_cmd                   : in    std_logic_vector(2 downto 0);
          app_en                    : in    std_logic;
          app_wdf_data              : in    std_logic_vector(127 downto 0);
          app_wdf_end               : in    std_logic;
          app_wdf_mask         : in    std_logic_vector(15 downto 0);
          app_wdf_wren              : in    std_logic;
          app_rd_data               : out   std_logic_vector(127 downto 0);
          app_rd_data_end           : out   std_logic;
          app_rd_data_valid         : out   std_logic;
          app_rdy                   : out   std_logic;
          app_wdf_rdy               : out   std_logic;
          app_sr_req                : in    std_logic;
          app_ref_req               : in    std_logic;
          app_zq_req                : in    std_logic;
          app_sr_active             : out   std_logic;
          app_ref_ack               : out   std_logic;
          app_zq_ack                : out   std_logic;
          ui_clk                    : out   std_logic;
          ui_clk_sync_rst           : out   std_logic;
          init_calib_complete       : out   std_logic;
          -- System Clock Ports
          sys_clk_i                      : in    std_logic;
          -- Reference Clock Ports
          clk_ref_i                                : in    std_logic;
        sys_rst                     : in    std_logic
      );
    end component mig_7series_0;
    
    constant nCK_PER_CLK           : integer := 4;
    -- constant ADDR_WIDTH : integer := 28;
    constant DATA_WIDTH            : integer := 16;
    constant PAYLOAD_WIDTH         : integer := DATA_WIDTH;
    constant BURST_LENGTH          : integer := 8;
    constant APP_DATA_WIDTH        : integer := 2 * nCK_PER_CLK * PAYLOAD_WIDTH;
    constant APP_MASK_WIDTH        : integer := APP_DATA_WIDTH / 8;
    
    signal init_calib_complete_o : std_logic;
    signal app_addr                    : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal app_cmd                     : std_logic_vector(2 downto 0);
    signal app_en                      : std_logic;
    signal app_rdy                     : std_logic;
    signal app_rd_data                 : std_logic_vector(APP_DATA_WIDTH-1 downto 0);
    signal app_rd_data_end             : std_logic;
    signal app_rd_data_valid           : std_logic;
    signal app_wdf_data                : std_logic_vector(APP_DATA_WIDTH-1 downto 0);
    signal app_wdf_end                 : std_logic;
    signal app_wdf_mask                : std_logic_vector(APP_MASK_WIDTH-1 downto 0);
    signal app_wdf_rdy                 : std_logic;
    signal app_sr_active               : std_logic;
    signal app_ref_ack                 : std_logic;
    signal app_zq_ack                  : std_logic;
    signal app_wdf_wren                : std_logic;
    
    signal ui_clk, ui_rst : std_logic;
    
    -- fsm states
    signal sdram_state : std_logic_vector(2 downto 0) := "000";
    constant IDLE : std_logic_vector(2 downto 0) := "000";
    constant WRITE : std_logic_vector(2 downto 0) := "001";
    constant READ : std_logic_vector(2 downto 0) := "010";
    constant WRITE_DONE : std_logic_vector(2 downto 0) := "011";
    constant READ_DONE : std_logic_vector(2 downto 0) := "100";
    constant WAIT_ACK : std_logic_vector(2 downto 0) := "101";
    
    signal dout_internal : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    
    -- ui commands
    constant WRITE_CMD : std_Logic_vector(2 downto 0) := "000";
    constant READ_CMD : std_logic_vector(2 downto 0) := "001";
    
    signal doRead, doWrite, cmdReq, ackInternal, validInternal : std_logic := '0';
    signal tmpData : std_logic_vector(63 downto 0);
    
    
begin

-- Start of User Design top instance
--***************************************************************************
-- The User design is instantiated below. The memory interface ports are
-- connected to the top-level and the application interface ports are
-- connected to the traffic generator module. This provides a reference
-- for connecting the memory controller to system.
--***************************************************************************
  u_mig_7series_0 : mig_7series_0
      port map (
       -- Memory interface ports
       ddr3_addr                      => ddr3_addr,
       ddr3_ba                        => ddr3_ba,
       ddr3_cas_n                     => ddr3_cas_n,
       ddr3_ck_n                      => ddr3_ck_n,
       ddr3_ck_p                      => ddr3_ck_p,
       ddr3_cke                       => ddr3_cke,
       ddr3_ras_n                     => ddr3_ras_n,
       ddr3_reset_n                   => ddr3_reset_n,
       ddr3_we_n                      => ddr3_we_n,
       ddr3_dq                        => ddr3_dq,
       ddr3_dqs_n                     => ddr3_dqs_n,
       ddr3_dqs_p                     => ddr3_dqs_p,
       init_calib_complete  => init_calib_complete_o,
       ddr3_cs_n                      => ddr3_cs_n,
       ddr3_dm                        => ddr3_dm,
       ddr3_odt                       => ddr3_odt,
-- Application interface ports
       app_addr                       => app_addr,
       app_cmd                        => app_cmd,
       app_en                         => app_en,
       app_wdf_data                   => app_wdf_data,
       app_wdf_end                    => app_wdf_end,
       app_wdf_wren                   => app_wdf_wren,
       app_rd_data                    => app_rd_data,
       app_rd_data_end                => app_rd_data_end,
       app_rd_data_valid              => app_rd_data_valid,
       app_rdy                        => app_rdy,
       app_wdf_rdy                    => app_wdf_rdy,
       app_sr_req                     => '0',
       app_ref_req                    => '0',
       app_zq_req                     => '0',
       app_sr_active                  => app_sr_active,
       app_ref_ack                    => app_ref_ack,
       app_zq_ack                     => app_zq_ack,
       ui_clk                         => ui_clk,
       ui_clk_sync_rst                => ui_rst,
       app_wdf_mask                   => app_wdf_mask,
-- System Clock Ports
       sys_clk_i                       => sys_clk166,
        sys_rst                        => SYS_RST,
        clk_ref_i                      => ref_clk200
        );
-- End of User Design top instance

    -- Es werden niemals mehrere Burstdaten nacheinander geschrieben.
    app_wdf_end <= '1'; 
    ack <= ackInternal;
    init_complete <= init_calib_complete_o;
    
    -- ###### Clock Domain Crossing Prozesse #################################
    -- Sys_clk ist die Proezssorclock (momentan 20 MHz)
    process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            ackInternal <= '0'; -- ack ist für maximal einen Zyklus aktiv
            
            -- Eine neue Operation wird angefordert, wenn die letzte fertig ist
            -- und read oder write Signal gesetzt ist.
            if (readEn = '1' or writeEn = '1') and (ackInternal = '0') and (cmdReq = '0') then
                cmdReq <= '1';
            end if;
            
            -- Ist die aktuelle Operation fertig, dann setze das Ack und nehme
            -- den cmdReq zurück
            if (validInternal = '1') then
                ackInternal <= '1';
                dout <= dout_internal;
                cmdReq <= '0';
            end if;
        end if;
    end process;
    
    -- ui_clk ist die vom MIG-PLL generierte Interface clock.
    process(ui_clk)
    begin
        if rising_edge(ui_clk) then
            -- Hier werden die readEn bzw. writeEn Signale in die neue
            -- Taktdomäne umgesetzt. Bzw. zurückgesetzt, wenn die aktuelle
            -- Operation fertig ist.
            if sdram_state = WAIT_ACK then
                doRead <= '0';
            elsif (readEn = '1') and (cmdReq = '1') then
                doRead <= '1';
            end if;
            
            if sdram_state = WAIT_ACK then
                doWrite <= '0';
            elsif (writeEn = '1') and (cmdReq = '1') then
                doWrite <= '1';
            end if;
        end if;
    end process;
    -- #######################################################################
    

    process(ui_clk, ui_rst)
    begin
        if rising_edge(ui_clk) then
            case sdram_state is
                when IDLE =>
                    validInternal <= '0';
                    -- wait for calibration complete before advancing
                    if (init_calib_complete_o = '1') then
                        if doRead = '1' then
                            sdram_state <= READ;
                        elsif doWrite = '1' then
                            sdram_state <= WRITE;
                        end if;
                    end if;
                -- IDLE END
                
                when WRITE =>
                    -- Setzen von enable, Adresse, Kommando, Daten und Maske,
                    -- sobald die UI bereit zum schreiben ist.
                    if (app_rdy = '1') and (app_wdf_rdy = '1') then
                        sdram_state <= WRITE_DONE;
                        app_en <= '1';
                        app_wdf_wren <= '1';
                        app_addr <= ADDR;
                        app_cmd <= WRITE_CMD;
                        app_wdf_data <= din;
                        app_wdf_mask <= mask;
                    end if;
                -- WRITE END
                
                when WRITE_DONE =>
                    -- hold app_en HIGH til app_rdy is available
                    if (app_rdy = '1') and (app_en = '1') then
                        app_en <= '0';
                    end if;
                    -- hold wdf_wren HIGH until wdf_rdy is available
                    if (app_wdf_rdy = '1') and (app_wdf_wren = '1') then
                        app_wdf_wren <= '0';
                    end if;
                    
                    -- wait til both signals are zero
                    if (app_en = '0') and (app_wdf_wren = '0') then
                        validInternal <= '1';
                        sdram_state <= WAIT_ACK;
                    end if;
                -- WRITE_DONE END
                
                when READ =>
                    -- setzen von enable, Adresse und Kommano, sobald das UI
                    --  bereit ist.
                    if app_rdy = '1' then
                        app_en <= '1';
                        app_addr <= ADDR;
                        app_cmd <= READ_CMD;
                        sdram_state <= READ_DONE;
                    end if;
                -- READ END
                
                when READ_DONE =>
                    if (app_rdy = '1') and (app_en = '1') then
                        app_en <= '0';
                    end if;
                    
                    if app_rd_data_valid = '1' then
                        dout_internal <= app_rd_data;
                        validInternal <= '1';
                        sdram_state <= WAIT_ACK;
                    end if;
                -- READ_DONE END
                
                when WAIT_ACK =>
                    -- Warten bis die Anforderung auf der sys_clk Seite zurück
                    -- genommen wurde, sonst würde eine Schleife entstehen.
                    if cmdReq = '0' then
                        validInternal <= '0';
                        sdram_state <= IDLE;
                    end if;
                -- WAIT_ACK END
                
                when others =>
                    sdram_state <= IDLE;
                    validInternal <= '0';
            end case;
            
            if ui_rst = '1' then
                sdram_state <= IDLE;
                validInternal <= '0';
                app_wdf_wren <= '0';
                app_en <= '0';
            end if;
        end if;
    end process;

end Behavioral;
