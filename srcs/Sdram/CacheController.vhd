----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 11/02/2021 08:59:02 PM
-- Design Name: 
-- Module Name: CacheController - Behavioral
-- Target Devices: ARMv4 Prozessor
-- Description: 
-- 
-- Dependencies: 
-- Einfacher Cache Controller mit Verbindung zum Cache Speicher. Die aktuelle
-- Architektur DirectMapped stellt einen direkt abgebildeten Cache dar mit einer
-- breite von DATA_BURST_WIDTH und einer Tiefe von CACHE_DEPTH.
-- WICHTIG: Der verwendete BRAM muss entsprechend angepasst werden.
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity CacheController is
    Generic (
        CACHE_DEPTH : natural := 64;
        DATA_BURST_WIDTH : natural := 128;
        ADDR_WIDTH : natural := 28
    );
    Port ( 
        SYS_CLK : in STD_LOGIC;
        SYS_RST : in STD_LOGIC;
        -- Verbindungen zwischen Prozessor und CacheController
        ADDR : in STD_LOGIC_VECTOR (27 downto 0);
        DIN : in STD_LOGIC_VECTOR (31 downto 0);
        DOUT : out STD_LOGIC_VECTOR (31 downto 0);
        -- Kontrollsignale
        writeEn : in STD_LOGIC;
        readEn : in STD_LOGIC;
        ack : out STD_LOGIC;
        -- Verbindungen zwischen Cache und RAM.
        MEM_READY : in STD_LOGIC;
        -- Datenverbindungen zwischen Cache und RAM
        MEM_IN : out STD_LOGIC_VECTOR (127 downto 0); -- Mem in, Cache Out
        MEM_OUT : in STD_LOGIC_VECTOR (127 downto 0); -- Mem out, Cache in
        -- Anfragesignale von Cache zum Speicher
        MEM_READ_EN : out STD_LOGIC;
        MEM_WRITE_EN : out STD_LOGIC 
    );
end CacheController;

architecture DirectMapped of CacheController is
    COMPONENT cache_mem
      PORT (
        clka : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(127 DOWNTO 0)
      );
    END COMPONENT;
    
    COMPONENT cache_ctrl_mem
      PORT (
        clka : IN STD_LOGIC;
        rsta : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        rsta_busy : OUT STD_LOGIC
      );
    END COMPONENT;

    -- simple function to get the needed bits for the length of the memory
    function log2_f(input : natural) return natural is
    begin
        for i in 0 to natural'high loop
            if (2**i >= input) then
                return i;
            end if;
        end loop;
        return 0;
    end function log2_f;
    
    constant INDEX_BITS : integer := log2_f(CACHE_DEPTH);
    -- burst length 8 x 16 bit == 8 x 2 bytes = 16 bytes => log2(16) 
    constant BURST_ALIGN : integer := log2_f(DATA_BURST_WIDTH/16); 
    -- all the remaining bits are used for the tag
    constant TAG_BITS :  integer := (ADDR_WIDTH - INDEX_BITS - BURST_ALIGN);

--    type t_CACHE_CONTROL_LINE is record
--        valid : std_logic;
--        tag : std_logic_vector(TAG_BITS-1 downto 0);
--    end record t_CACHE_CONTROL_LINE;
   
--     type cacheType is array(0 to CACHE_DEPTH-1) of t_CACHE_CONTROL_LINE;
--     signal cacheControlMem : cacheType;
    
    signal cache_ctrl_out, cache_ctrl_in : std_logic_vector(31 downto 0);
    signal cache_ctrl_we : std_logic_vector(3 downto 0);
    signal cache_ctrl_tag : std_logic_vector(TAG_BITS-1 downto 0);
    signal cache_ctrl_valid : std_logic_vector(3 downto 0);
    
    signal tag_in : std_logic_vector(TAG_BITS-1 downto 0) := (others => '0');
    signal index_in : unsigned(INDEX_BITS-1 downto 0) := (others => '0');
    signal word_select : std_logic_vector(1 downto 0);
    signal hit, is_equal : std_logic := '0';
    
    signal mem_ack, cache_ack_o : std_logic;
    
    signal cache_mem_we : std_logic_vector(15 downto 0);
    signal cache_mem_in, cache_mem_out : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal cache_mask : std_logic_Vector(15 downto 0);
    signal cache_valid : std_Logic;
    signal cache_tag : std_logic_vector(TAG_BITS-1 downto 0);
    signal cache_data : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal cache_idx : std_logic_vector(INDEX_BITS-1 downto 0);
    
    signal curr_state : std_logic_vector(1 downto 0) := "00";
    constant ST_IDLE : std_Logic_vector(1 downto 0) := "00";
    constant ST_EVAL : std_Logic_vector(1 downto 0) := "01";
    
    -- constant ST_READ : std_Logic_vector(1 downto 0) := "01";
    -- constant ST_WRITE : std_Logic_vector(1 downto 0) := "10";
    constant ST_DONE : std_Logic_vector(1 downto 0) := "11";
    
    constant ZERO_WORD : std_logic_vector(31 downto 0) := (others => '0');
begin

    isnt_cache_bram : cache_mem port map
    (
        clka => sys_clk,
        ena => '1',
        wea => cache_mem_we,
        addra => std_logic_vector(index_in),
        dina => cache_mem_in,
        douta => cache_mem_out
    );
    
    inst_cache_ctrl : cache_ctrl_mem port map
    (
        clka => sys_clk,
        rsta => SYS_RST,
        ena => '1',
        wea => cache_ctrl_we,
        addra => std_logic_vector(index_in),
        dina => cache_ctrl_in,
        douta => cache_ctrl_out
    );

    mem_ack <= MEM_READY;
    ack <= cache_ack_o;
    tag_in <= addr(ADDR_WIDTH-1 downto (ADDR_WIDTH-TAG_BITS));
    index_in <= unsigned(addr((ADDR_WIDTH-TAG_BITS-1) downto BURST_ALIGN));
    word_select <= addr(3 downto 2);

    cache_ctrl_tag <= cache_ctrl_out(TAG_BITS-1 downto 0);
    cache_ctrl_valid <= cache_ctrl_out(TAG_BITS+3 downto TAG_BITS);
    
    with word_select select cache_valid <= cache_ctrl_valid(0) when "00",
        cache_ctrl_valid(1) when "01",
        cache_ctrl_valid(2) when "10",
        cache_ctrl_valid(3) when "11",
        '0' when others;
        
    hit <= is_equal and cache_valid; -- cacheMem(to_integer(index_in)).valid;
    is_equal <= '1' when tag_in = cache_ctrl_tag else '0';
    
    process(SYS_CLK)
    begin
        if rising_edge(SYS_CLK) then
            cache_ack_o <= '0';
            cache_ctrl_we <= (others => '0');
            cache_mem_we <= (others => '0');
            cache_ctrl_in <= (others => '0');
            
            case curr_state is
                when ST_IDLE =>
                    if readEn = '1' or writeEn = '1' then
                        curr_state <= ST_EVAL;
                    end if;
                when ST_EVAL =>
                    if readEn = '1' then
                        if hit = '1' then
                            case word_select is
                                when "00" =>
                                    dout <= cache_mem_out(31 downto 0);
                                when "01" =>
                                    dout <= cache_mem_out(63 downto 32);
                                when "10" =>
                                    dout <= cache_mem_out(95 downto 64);
                                when "11" =>
                                    dout <= cache_mem_out(127 downto 96);
                                when others =>
                            end case;
                            
                            cache_ack_o <= '1';
                            MEM_READ_EN <= '0';
                            curr_state <= ST_DONE;
                        else
                            MEM_READ_EN <= '1';
                            if mem_ack = '1' then
                                cache_ctrl_in(TAG_BITS+3 downto TAG_BITS) <= (others => '1');
                                cache_ctrl_in(TAG_BITS-1 downto 0) <= tag_in;
                                cache_ctrl_we <= (others => '1');
                                
                                cache_mem_we <= (others => '1');
                                cache_mem_in <= mem_out;
                                
                                case word_select is
                                    when "00" =>
                                        dout <= mem_out(31 downto 0);
                                    when "01" =>
                                        dout <= mem_out(63 downto 32);
                                    when "10" =>
                                        dout <= mem_out(95 downto 64);
                                    when "11" =>
                                        dout <= mem_out(127 downto 96);
                                    when others =>
                                end case;
                                cache_ack_o <= '1';
                                MEM_READ_EN <= '0';
                                curr_state <= ST_DONE;
                            end if;
                        end if;
                     elsif writeEn = '1' then -- WRITE TO CACHE
                        MEM_WRITE_EN <= '1';
                        cache_ctrl_in(TAG_BITS+to_integer(unsigned(word_select))) <= '1'; -- set valid flag
                        cache_ctrl_in(TAG_BITS-1 downto 0) <= tag_in;
                        cache_ctrl_we <= (others => '1');
                        -- put data into place and write to RAM
                        case word_select is
                            when "00" =>
                                mem_in <=       ZERO_WORD & ZERO_WORD & ZERO_WORD & din;
                                cache_mem_in <= ZERO_WORD & ZERO_WORD & ZERO_WORD & din;
                                cache_mem_we <= x"000f";
                            when "01" =>
                                mem_in <=       ZERO_WORD & ZERO_WORD & din & ZERO_WORD;
                                cache_mem_in <= ZERO_WORD & ZERO_WORD & din & ZERO_WORD;
                                cache_mem_we <= x"00f0";
                            when "10" =>
                                mem_in <=       ZERO_WORD & din & ZERO_WORD & ZERO_WORD;
                                cache_mem_in <= ZERO_WORD & din & ZERO_WORD & ZERO_WORD;
                                cache_mem_we <= x"0f00";
                            when "11" =>
                                mem_in <=       din & ZERO_WORD & ZERO_WORD & ZERO_WORD;
                                cache_mem_in <= din & ZERO_WORD & ZERO_WORD & ZERO_WORD;
                                cache_mem_we <= x"f000";
                            when others =>
                        end case;
                        
                        if mem_ack = '1' then
                            cache_ack_o <= '1';
                            MEM_WRITE_EN <= '0';
                            curr_state <= ST_DONE;
                        end if;
                    end if;
                when ST_DONE =>
                    curr_state <= ST_IDLE;
                when others =>
                    MEM_WRITE_EN <= '0';
                    MEM_READ_EN <= '0';
                    curr_state <= ST_IDLE;
                    cache_mem_we <= (others => '0');
            end case;
            
            if sys_rst = '1' then
                MEM_WRITE_EN <= '0';
                MEM_READ_EN <= '0';
                curr_state <= ST_IDLE;
                cache_mem_we <= (others => '0');
            end if;
        end if;
    end process;
end DirectMapped;

