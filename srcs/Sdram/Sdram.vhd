----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 09/23/2021 09:06:47 PM
-- Design Name: 
-- Module Name: SdramBuffer - {DirectMapped, SimpleBuffer}
-- Target Devices: ARMv4 Prozessor 
-- Description: 
-- Diese Entität instanziiert SdramDriver und (wenn benötigt) CacheController.
-- Anschließend werden die Daten auf 32 Bit breite gekürzt, damit diese im 
-- weiteren vom ARM Prozessor verwendet werden können.
-- 
-- Dependencies: SdramDriver.vhd, CacheController.vhd 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity Sdram is
    Generic (
        CACHE_DEPTH : integer := 512; -- 16#20#; -- 0x20 == 32
        DATA_BURST_WIDTH : integer := 128;
        ADDR_WIDTH : integer := 28
    );
    Port (
        sys_clk : in STD_LOGIC;
        sys_rst : in STD_LOGIC;
        -- reference clock is always 200 MHz
        -- system clock is 166 MHz (see Arty A7 reference docs)
        -- https://digilent.com/reference/programmable-logic/arty-a7/reference-manual#memory
        sys_clk166 : in STD_LOGIC;
        ref_clk200 : in STD_LOGIC;
        
        writeEn : in STD_LOGIC;
        readEn : in STD_LOGIC;
        ack : out STD_LOGIC;
        
        addr : in STD_LOGIC_VECTOR(ADDR_WIDTH-1 downto 0);
        din : in STD_LOGIC_VECTOR(31 downto 0);
        dout : out STD_LOGIC_VECTOR(31 downto 0);
        -- hardware interface
        -- Inouts
        ddr3_dq                        : inout std_logic_vector(15 downto 0);
        ddr3_dqs_p                     : inout std_logic_vector(1 downto 0);
        ddr3_dqs_n                     : inout std_logic_vector(1 downto 0);
        
        -- Outputs
        ddr3_addr                      : out   std_logic_vector(13 downto 0);
        ddr3_ba                        : out   std_logic_vector(2 downto 0);
        ddr3_ras_n                     : out   std_logic;
        ddr3_cas_n                     : out   std_logic;
        ddr3_we_n                      : out   std_logic;
        ddr3_reset_n                   : out   std_logic;
        ddr3_ck_p                      : out   std_logic_vector(0 downto 0);
        ddr3_ck_n                      : out   std_logic_vector(0 downto 0);
        ddr3_cke                       : out   std_logic_vector(0 downto 0);
        ddr3_cs_n                      : out   std_logic_vector(0 downto 0);
        ddr3_dm                        : out   std_logic_vector(1 downto 0);
        ddr3_odt                       : out   std_logic_vector(0 downto 0);
        init_complete                  : out std_Logic
    );
end Sdram;

architecture DirectMapped of Sdram is

    signal ram_addr : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal tmpData : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal ram_din, ram_dout : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal dout_internal : std_logic_vector(31 downto 0);
    signal ram_writeEn, ram_readEn, ack_sdram, ram_init_complete : std_logic;
    signal data_mask : std_logic_vector(15 downto 0) := (others => '0');
    
    signal cache_din, cache_dout : std_logic_vector(31 downto 0);
    signal cache_ack_o : std_logic;
    
    signal curr_state : std_logic_vector(1 downto 0) := "00";
    constant ST_IDLE : std_logic_vector(1 downto 0) := "00";
    constant ST_READ : std_logic_vector(1 downto 0) := "01";
    constant ST_WRITE : std_logic_vector(1 downto 0) := "10";
    constant ST_DONE : std_logic_vector(1 downto 0) := "11";
    
    -- Null-Wort, um Schreibzugriffe auf den SDRAM richtig auszurichten
    constant ZERO_WORD : std_logic_vector(31 downto 0) := (others => '0');
begin
    
    -- Erstellen der auf den Burst ausgerichteten Basis-Adresse. Pro Burst
    -- werden 128 Bit, also 4 Wörter gelesen
    ram_addr <= addr(ADDR_WIDTH-1 downto 4)  & "0000"; 
    ack <= cache_ack_o;
    
    -- Generieren der benötigten Maske für den aktuellen Zugriff
    with addr(3 downto 2) select data_mask <=
        x"fff0" when "00",
        x"ff0f" when "01",
        x"f0ff" when "10",
        x"0fff" when "11",
        x"0000" when others;
    
    
    inst_Cache : entity work.CacheController(DirectMapped) port map
    (
        sys_clk => sys_clk,
        sys_rst => sys_rst,
        
        writeEn => writeEn,
        readEn => readEn,
        ack => cache_ack_o,
        
        addr => addr,
        din => din,
        dout => dout,
        
        mem_ready => ack_sdram,
        mem_in => ram_din,
        mem_out => ram_dout,
        mem_read_en => ram_readEn,
        mem_write_en => ram_writeEn
        
    );
        
    inst_SdramDriver : entity work.SdramDriver port map
    (
        sys_clk => sys_clk,
        sys_rst => sys_rst,
        
        sys_clk166 => sys_clk166,
        ref_clk200 => ref_clk200,
        
        writeEn => ram_writeEn,
        readEn => ram_readEn,
        ack => ack_sdram,
        
        addr => ram_addr,
        din => ram_din,
        dout => ram_dout,
        mask => data_mask,
        -- hardware interface
        ddr3_addr                      => ddr3_addr,
        ddr3_ba                        => ddr3_ba,
        ddr3_cas_n                     => ddr3_cas_n,
        ddr3_ck_n                      => ddr3_ck_n,
        ddr3_ck_p                      => ddr3_ck_p,
        ddr3_cke                       => ddr3_cke,
        ddr3_ras_n                     => ddr3_ras_n,
        ddr3_reset_n                   => ddr3_reset_n,
        ddr3_we_n                      => ddr3_we_n,
        ddr3_dq                        => ddr3_dq,
        ddr3_dqs_n                     => ddr3_dqs_n,
        ddr3_dqs_p                     => ddr3_dqs_p,
        ddr3_cs_n                      => ddr3_cs_n,
        ddr3_dm                        => ddr3_dm,
        ddr3_odt                       => ddr3_odt,
        init_complete                  => ram_init_complete
    );
    
    init_complete <= ram_init_complete;

end DirectMapped;

architecture SimpleBuffer of Sdram is

    signal ram_addr : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal tmpData : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal ram_din, ram_dout : std_logic_vector(DATA_BURST_WIDTH-1 downto 0);
    signal dout_internal : std_logic_vector(31 downto 0);
    signal ram_writeEn, ram_readEn, ack_sdram, ram_init_complete : std_logic;
    signal data_mask : std_logic_vector(15 downto 0) := (others => '0');
    
    signal curr_state : std_logic_vector(1 downto 0) := "00";
    constant ST_IDLE : std_logic_vector(1 downto 0) := "00";
    constant ST_READ : std_logic_vector(1 downto 0) := "01";
    constant ST_WRITE : std_logic_vector(1 downto 0) := "10";
    constant ST_DONE : std_logic_vector(1 downto 0) := "11";
    
    constant ZERO_WORD : std_logic_vector(31 downto 0) := (others => '0');
begin
    
    -- Erstellen der auf den Burst ausgerichteten Basis-Adresse. Pro Burst
    -- werden 128 Bit, also 4 Wörter gelesen
    ram_addr <= addr(ADDR_WIDTH-1 downto 4)  & "0000"; 
    dout <= dout_internal;
    
    process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            ack <= '0'; -- Ack ist für maximal einen sys_clk Zyklus aktiv
            
            case curr_state is
                when ST_IDLE =>
                    -- Warte auf neuen Zugriff und springe in den
                    -- entsprechenden Zustand
                    if (writeEn = '1') then
                        curr_state <= ST_WRITE;
                    elsif (readEn = '1') then
                        curr_state <= ST_READ;
                        ram_readEn <= '1';
                    end if;
                when ST_READ =>
                    if ack_sdram = '1' then
                        if readEn = '1' then
                            case addr(3 downto 2) is
                                when "00" =>
                                    dout_internal <= ram_dout(31 downto 0);
                                when "01" =>
                                    dout_internal <= ram_dout(63 downto 32);
                                when "10" =>
                                    dout_internal <= ram_dout(95 downto 64);
                                when "11" =>
                                    dout_internal <= ram_dout(127 downto 96);
                                when others =>
                            end case;
                            ack <= '1';
                            curr_state <= ST_DONE;
                            ram_readEn <= '0';
                        end if;
                    end if;
                when ST_WRITE =>
                    ram_writeEn <= '1';
                    -- Das zu schreibende Wort wird in den SDRAM geschrieben
                    -- mit Hilfe einer Maske. Die restlichen Inputs werden auf 0
                    -- gesetzt. 
                    case addr(3 downto 2) is
                        when "00" =>
                            ram_din <= ZERO_WORD & ZERO_WORD & ZERO_WORD & din;
                            data_mask <= x"fff0";
                        when "01" =>
                            ram_din <= ZERO_WORD & ZERO_WORD & din & ZERO_WORD;
                            data_mask <= x"ff0f";
                        when "10" =>
                            ram_din <= ZERO_WORD & din & ZERO_WORD & ZERO_WORD;
                            data_mask <= x"f0ff";
                        when "11" =>
                            ram_din <= din & ZERO_WORD & ZERO_WORD & ZERO_WORD;
                            data_mask <= x"0fff";
                        when others =>
                    end case;
                    
                    if ack_sdram = '1' then
                        ram_writeEn <= '0';
                        ack <= '1';
                        curr_state <= ST_DONE;
                    end if;
                when ST_DONE => 
                    -- Wartezyklus nach jeder Operation, sonst nimmt der
                    -- Prozessor nicht schnell genug sein CS zurück und der Loop
                    -- wird nicht durchbrochen 
                    curr_state <= ST_IDLE;
                when others =>
                    curr_state <= ST_IDLE;
            end case;
        
            if sys_rst = '1' then
                curr_state <= ST_IDLE;
                ram_readEn <= '0';
                ram_writeEn <= '0';
                ack <= '0';
            end if;
        end if;
    end process;
    
    -- Init Complete Signal der SDRAM-Hardware. Wird benötigt, um ein
    -- Wartesignal im Prozessor zu generieren, so lang der SDRAM noch nicht
    -- initialisiert ist.
    init_complete <= ram_init_complete;
        
    inst_SdramDriver : entity work.SdramDriver port map
    (
        sys_clk => sys_clk,
        sys_rst => sys_rst,
        
        sys_clk166 => sys_clk166,
        ref_clk200 => ref_clk200,
        
        writeEn => ram_writeEn,
        readEn => ram_readEn,
        ack => ack_sdram,
        
        addr => ram_addr,
        din => ram_din,
        dout => ram_dout,
        mask => data_mask,
        -- hardware interface
        ddr3_addr                      => ddr3_addr,
        ddr3_ba                        => ddr3_ba,
        ddr3_cas_n                     => ddr3_cas_n,
        ddr3_ck_n                      => ddr3_ck_n,
        ddr3_ck_p                      => ddr3_ck_p,
        ddr3_cke                       => ddr3_cke,
        ddr3_ras_n                     => ddr3_ras_n,
        ddr3_reset_n                   => ddr3_reset_n,
        ddr3_we_n                      => ddr3_we_n,
        ddr3_dq                        => ddr3_dq,
        ddr3_dqs_n                     => ddr3_dqs_n,
        ddr3_dqs_p                     => ddr3_dqs_p,
        ddr3_cs_n                      => ddr3_cs_n,
        ddr3_dm                        => ddr3_dm,
        ddr3_odt                       => ddr3_odt,
        init_complete                  => ram_init_complete
    );

end SimpleBuffer;

