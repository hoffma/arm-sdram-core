library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.ArmTypes.all;
--------------------------------------------------------------------------------
--	Konfigurationsoption die festlegt, ob 
--	das Z-Bit bereits in der Komponente erzeugt
--	wird
--------------------------------------------------------------------------------
use work.ArmConfiguration.USE_DEDICATED_ARITH_ZERO_ALU;

entity ArmALULogicPart is
	Port (
		OP1	: in  std_logic_vector (31 downto 0);
		OP2	: in  std_logic_vector (31 downto 0);
		ALU_CTRL: in  std_logic_vector (3 downto 0);
		RES	: out std_logic_vector (31 downto 0);
		Z_OUT	: out std_logic
	);
end ArmALULogicPart;

architecture Behavioral of ArmALULogicPart is
	signal RES_INTERNAL : std_logic_vector(31 downto 0);	
begin

	SELECT_LOGIC_RESULT :process(OP1,OP2,ALU_CTRL) is
		variable INTERN_OP1 : std_logic_vector(31 downto 0);
		variable INTERN_OP2 : std_logic_vector(31 downto 0);		
	begin
--	Einige logische Verknuepfungen verbinden
--	den bitweise invertierten Operand 2. Im
--	Rahmen der logischen Verknuepfungen ist dies
--	unmittelbar von ALU_CTRL(1) abhaengig
		if ALU_CTRL(1) = '1' then
			INTERN_OP2 := not OP2;
		else
			INTERN_OP2 := OP2;
		end if;

		case (ALU_CTRL) is
			when OP_AND|OP_BIC|OP_TST =>
				RES_INTERNAL <= OP1 and INTERN_OP2;
			when OP_ORR =>
				RES_INTERNAL <= OP1 or INTERN_OP2;
			when OP_EOR|OP_TEQ =>
				RES_INTERNAL<= OP1 xor INTERN_OP2;
			when others =>	-- OP_MOV|OP_MVN
				RES_INTERNAL <= INTERN_OP2;
		end case;
	end process SELECT_LOGIC_RESULT;

	RES <= RES_INTERNAL;

--	Entweder Z-Bit aus dem Ergebnis erzeugen oder dauerhaft
--	0 setzen wenn es ohnehin in der umgebenden
--	ALU-Struktur erzeugt wird.
	Z_OUT <= '0' when not USE_DEDICATED_ARITH_ZERO_ALU else
	  		'1' when RES_INTERNAL = X"00000000" else '0';

end Behavioral;


