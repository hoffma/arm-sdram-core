--------------------------------------------------------------------------------
--	16-Bit-Register zur Steuerung der Auswahl des naechsten Registers
--	bei der Ausfuehrung von STM/LDM-Instruktionen. Das Register wird
--	mit der Bitmaske der Instruktion geladen. Ein Prioritaetsencoder
--	(Modul ArmPriorityVectorFilter) bestimmt das Bit mit der hochsten 
--	Prioritaet. Zu diesem Bit wird eine 4-Bit-Registeradresse erzeugt und
--	das Bit im Register geloescht. Bis zum Laden eines neuen Datums wird
--	mit jedem Takt ein Bit geloescht bis das Register leer ist.	
--------------------------------------------------------------------------------
--	Datum:		13.06.2010
--	Version:	1.01
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ArmLdmStmNextAddress is
	Port(
		SYS_RST			: in std_logic;
		SYS_CLK			: in std_logic;	
		LNA_LOAD_REGLIST 	: in std_logic;
		LNA_HOLD_VALUE 		: in std_logic;
		LNA_REGLIST 		: in std_logic_vector(15 downto 0);
		LNA_ADDRESS 		: out std_logic_vector(3 downto 0);
		LNA_CURRENT_REGLIST_REG : out std_logic_vector(15 downto 0)
	    );
end ArmLdmStmNextAddress;

architecture BEHAVE of ArmLdmStmNextAddress is
	constant NR_OF_REGS	: natural := 16;
	signal NEW_REGLIST	: std_logic_vector(NR_OF_REGS - 1 downto 0);
	signal CURRENT_REGLIST	: std_logic_vector(NR_OF_REGS - 1 downto 0) := (others => '1');
	signal FILTERED_REGLIST : std_logic_vector(NR_OF_REGS - 1 downto 0) := (others => '1');

	component ArmPriorityVectorFilter
		port(
			PVF_VECTOR_UNFILTERED	: in std_logic_vector(15 downto 0);
			PVF_VECTOR_FILTERED	: out std_logic_vector(15 downto 0)
		);
	end component;

begin
--------------------------------------------------------------------------------
--	Priorisierungsschaltung fuer die Bits der Registerliste,
--	am Ausgang ist nur das Bit mit der hochsten Prioritaet (niedrigster
--	Index) gesetzt.	Dieses Datum ist die "gefilterte Registerliste".
--------------------------------------------------------------------------------
	CURRENT_REGLIST_FILTER : ArmPriorityVectorFilter
		port map(
			PVF_VECTOR_UNFILTERED	=> CURRENT_REGLIST,
			PVF_VECTOR_FILTERED	=> FILTERED_REGLIST
		);

	NEW_REGLIST <= LNA_REGLIST;

--------------------------------------------------------------------------------
--	Verhalten der Speicherflipflops mit Beschaltung,
--	das Filtern der Bits ist separat beschrieben.
--------------------------------------------------------------------------------
--	LNA_LOAD_REGLIST ist priorisiert und ueberschreibt das gesamte 16-Bit-
--	Register mit dem Wert von LNA_REGLIST.
--	LNA_HOLD_VALUE stoppt das Loeschen des derzeit priorisierten Bits, was
--	zum Beispiel notwendig ist, wenn sich ein Speicherzugriff verzoegert.
--------------------------------------------------------------------------------
	SET_REGLIST_VALUE : process(SYS_CLK)is
	begin
		if SYS_CLK'event and SYS_CLK = '1' then
			if SYS_RST = '1' then
				CURRENT_REGLIST <= (others => '0');
			else
				for i in 0 to NR_OF_REGS - 1 loop
					if LNA_LOAD_REGLIST = '1' then
--------------------------------------------------------------------------------
--	Neue Registerliste laden, geschieht immer dann, wenn eine neue 
--	Instruktion gefetcht wird waehrend die aktuell decodierte Instruktion
--	weder LDM noch STM ist. Ausserdem wird die Registerliste im letzten
--	Takt einer LDM/STM-Instruktion aus dem Instruktionsregister
--	aktualisiert. Auf diese Weise werden zwei aufeinander folgende Befehle
--	dieses Typs korrekt verarbeitet.	
--------------------------------------------------------------------------------
						CURRENT_REGLIST(i) <= NEW_REGLIST(i);
					else
						if LNA_HOLD_VALUE = '1' then
--	Liste halten, z.B. weil sich ein Speicherzugriff verzoegert.							
							CURRENT_REGLIST(i) <= CURRENT_REGLIST(i);
						else
--	Loeschen des am hoechsten priorisierten Bits.
							CURRENT_REGLIST(i) <= CURRENT_REGLIST(i) and (not FILTERED_REGLIST(i));
						end if;
					end if;

				end loop;
				
			end if;
		end if;		
	end process SET_REGLIST_VALUE;

	LNA_CURRENT_REGLIST_REG <= CURRENT_REGLIST;

--------------------------------------------------------------------------------
--	Codierung der Registeradresse mithilfe der gefilterten Registerliste
--	FILTERED_REGLIST enthaelt genau null oder eine 1. Die einzelne 1 zeigt 
--	das Register mit der hoechsten Prioritaet an. Eine 1 an niedrigster
--	Position steht dabei fuer Register 0 und muss entsprechend mit Adresse
--	"0000" codiert werden.
--	Ein Register voller Nullen wird ebenfalls mit Adresse "0000" dekodiert.
--------------------------------------------------------------------------------
	process(FILTERED_REGLIST, CURRENT_REGLIST) is
	begin
--------------------------------------------------------------------------------
--	Das case-Statement ist hier geeignet, um die one-hot-Codierung
--	von FILTERED_REGLIST sinnvoll auswerten zu koennen.
-- 	Die obere Variante fuehrt ggf. zu einem etwas schlechteren Synthese-
--	ergebnis, ohne dabei aber in der Verhaltenssimulation
-- 	das Problem mit don't care-Signalen zu verursachen (Vergleich mit "-"
--	ist in VHDL nur fuer ein Signal mit dem Wert "-" wahr).
--	Fuer das HWPR ist Variante 1 deshalb geeigneter.
--------------------------------------------------------------------------------
--	Weiteres Verbesserungspotential: der Prioritaetsfilter und die 
--	Addresscodierung sind z.T. redundant, weil in beiden Faellen auf die
--	1 mit der hoechsten Prioritaet getestet wird. Beide Schaltungen
--	sollte zukuenftig evtl. vereinigt werden.
--------------------------------------------------------------------------------
 		case FILTERED_REGLIST is
 			when X"0001" => LNA_ADDRESS <= X"0";
 			when X"0002" => LNA_ADDRESS <= X"1";
 			when X"0004" => LNA_ADDRESS <= X"2";
 			when X"0008" => LNA_ADDRESS <= X"3";
 			when X"0010" => LNA_ADDRESS <= X"4";
 			when X"0020" => LNA_ADDRESS <= X"5";
 			when X"0040" => LNA_ADDRESS <= X"6";
 			when X"0080" => LNA_ADDRESS <= X"7";
 			when X"0100" => LNA_ADDRESS <= X"8";
 			when X"0200" => LNA_ADDRESS <= X"9";
 			when X"0400" => LNA_ADDRESS <= X"A";
 			when X"0800" => LNA_ADDRESS <= X"B";
 			when X"1000" => LNA_ADDRESS <= X"C";
 			when X"2000" => LNA_ADDRESS <= X"D";
 			when X"4000" => LNA_ADDRESS <= X"E";
 			when X"8000" => LNA_ADDRESS <= X"F";
 			when others  => LNA_ADDRESS <= X"0";	
		
--			when "---------------1" => LNA_ADDRESS <= X"0";
--			when "--------------10" => LNA_ADDRESS <= X"1";
--			when "-------------100" => LNA_ADDRESS <= X"2";
--			when "------------1000" => LNA_ADDRESS <= X"3";
--			when "-----------10000" => LNA_ADDRESS <= X"4";
--			when "----------100000" => LNA_ADDRESS <= X"5";
--			when "---------1000000" => LNA_ADDRESS <= X"6";
--			when "--------10000000" => LNA_ADDRESS <= X"7";
--			when "-------100000000" => LNA_ADDRESS <= X"8";
--			when "------1000000000" => LNA_ADDRESS <= X"9";
--			when "-----10000000000" => LNA_ADDRESS <= X"A";
--			when "----100000000000" => LNA_ADDRESS <= X"B";
--			when "---1000000000000" => LNA_ADDRESS <= X"C";
--			when "--10000000000000" => LNA_ADDRESS <= X"D";
--			when "-100000000000000" => LNA_ADDRESS <= X"E";
--			when "1000000000000000" => LNA_ADDRESS <= X"F";
-- 			when others		=> LNA_ADDRESS <= X"0";	
		end case;
	end process;
end BEHAVE;
