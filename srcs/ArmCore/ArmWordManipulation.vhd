--------------------------------------------------------------------------------
--	Einheit fuer Manipulationen gelesender Worte aus dem Speicher des
-- 	ARM-SoC.
--------------------------------------------------------------------------------
--	Worte werden so rotiert, dass das durch die beiden niederwertigen
--	Adressbits adressierte Byte rechts im Ausgangswort erscheint.
--	Dies ist immer dann notwendig, wenn der Zugriff auf ein Wort nicht
--	ausgerichtet erfolgt. Fuer Byte- und Halbworttransfers wird
--	anhand der Zugriffsadresse bestimmt, auf welchen Teilvektor
--	des Eingangswortes sich der Transfer bezog. Er wird
--	ganz rechts im Ausgangswort ausgerichtet und dann Null- oder 
--	Vorzeichenerweitert.
--------------------------------------------------------------------------------
--	Datum:		01.06.2010
--	Version:	1.0
--------------------------------------------------------------------------------
-- 	Aenderungen:
-- 	Zusaetzliche Stufe hinzugefuegt um bei LDR/LDRT-Transaktionen
-- 	unausgerichteter Worte das tatsaechliche adressierte Byte
-- 	an die rechteste Byteposition zu rotieren.
-- 	Der Versuch, die Rotation per ror-Verknruepfung zu realisieren
-- 	hat aus unerfindlichen Gruenden zu einer sehr langsamen Schaltung
-- 	gefuehrt. Daher wird die Rotationsstufe explizit durch 4:1-Multiplexer 
-- 	realisiert (case-Statement im Prozess ROTATE).
-- 	Aenderung der Schnittstelle
-- 	WMP_BYTE und WMP_HW wurden durch WMP_DMAS als gemeinsames
-- 	Signalbuendel ersetzt. Dadurch ist zwar etwas zusaetzliche
-- 	Decodierarbeit notwendig, aber da das Modul erst spaet in
-- 	der MEM-Stufe des Prozessors wirkt, spielt dies keine Rolle.
-- 	Auf der anderen Seite werden nun DataReplication,
-- 	ArmWordManipulation und der Speicherbus selbst durch identische 
-- 	Signale gesteuert, die im Kontrollpfad alle dieselben Pipeline-
-- 	register verwenden.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ArmTypes.all;

--------------------------------------------------------------------------------
--	WMP_WORD_IN/OUT:	Ein- und Ausgangsdaten in Wortbreite
--	WMP_DMAS :		Art des Speicherzugriffs, kennzeichnet die
--				Interpretation des Datums an WMP_WORD_IN
--				(Wort, Halbwort, Byte).
--	WMP_ADDRESS:		Die beiden niederwertigen Bits der Zugriffs-
--				adresse	eines Lesezugriffs.
--	WMP_SIGNED:		Die Erweiterung von Halbw. und Bytes erfolgt
--				mit Vorzeichen (SIGNED = 1).	
--------------------------------------------------------------------------------
entity ArmWordManipulation is
    Port(  WMP_WORD_IN 		: in std_logic_vector (31 downto 0);
    	   WMP_ADDRESS 		: in std_logic_vector(1 downto 0);
           WMP_DMAS 		: in std_logic_vector(1 downto 0);
           WMP_SIGNED 		: in std_logic;
           WMP_WORD_OUT 	: out std_logic_vector (31 downto 0)
   	);
end ArmWordManipulation;

architecture BEHAVE of ArmWordManipulation is
	signal SIGN		: std_logic;
	signal RELEVANT_BYTE	: std_logic_vector(7 downto 0);
	signal RELEVANT_HW	: std_logic_vector(15 downto 0);
	signal ROT_WIDTH	: std_logic_vector(1 downto 0);
	signal ROTATED_WORD	: std_logic_vector(31 downto 0);
	signal H 		: std_logic;
	signal B 		: std_logic;
	alias  W		: std_logic_vector(31 downto 0) is WMP_WORD_IN;
	alias  ADDR		: std_logic_vector(1 downto 0) is WMP_ADDRESS;
	alias  S		: std_logic is WMP_SIGNED;
--------------------------------------------------------------------------------
--	Hilfssignal, um einen nicht erklaerbaren Fehler zu kompensieren
-- 	bei der Zuweisung an WMP_WORD_OUT(15 downto 8).
--------------------------------------------------------------------------------
	signal SIGN_BYTE	: std_logic_vector(7 downto 0);
begin
	B <= '1' when WMP_DMAS = DMAS_BYTE else '0';
	H <= '1' when WMP_DMAS = DMAS_HWORD else '0';
--------------------------------------------------------------------------------
-- 	Bei Bytezugriffen ist nichts zu rotieren und bei Halbwortzugriffen
-- 	schreibt die ISA dies zumindest nicht vor. Bei Wortzugriffen haengt
-- 	die Rotationsweite von den beiden niederwertigen Adressbits ab.
--------------------------------------------------------------------------------
	ROT_WIDTH <= "00" when (H or B) = '1' else ADDR;

 	ROTATE : process(ROT_WIDTH,WMP_WORD_IN)is
 	begin
		case ROT_WIDTH is
			when "00" =>
				ROTATED_WORD(7 downto 0)	<= WMP_WORD_IN(7 downto 0);
				ROTATED_WORD(15 downto 8)	<= WMP_WORD_IN(15 downto 8);
				ROTATED_WORD(23 downto 16)	<= WMP_WORD_IN(23 downto 16);
				ROTATED_WORD(31 downto 24)	<= WMP_WORD_IN(31 downto 24);
			when "01" =>
				ROTATED_WORD(7 downto 0)	<= WMP_WORD_IN(15 downto 8);
				ROTATED_WORD(15 downto 8)	<= WMP_WORD_IN(23 downto 16);
				ROTATED_WORD(23 downto 16)	<= WMP_WORD_IN(31 downto 24);
				ROTATED_WORD(31 downto 24)	<= WMP_WORD_IN(7 downto 0);
			when "10" =>
				ROTATED_WORD(7 downto 0)	<= WMP_WORD_IN(23 downto 16);
				ROTATED_WORD(15 downto 8)	<= WMP_WORD_IN(31 downto 24);
				ROTATED_WORD(23 downto 16)	<= WMP_WORD_IN(7 downto 0);
				ROTATED_WORD(31 downto 24)	<= WMP_WORD_IN(15 downto 8);
			when others =>	
				ROTATED_WORD(7 downto 0)	<= WMP_WORD_IN(31 downto 24);
				ROTATED_WORD(15 downto 8)	<= WMP_WORD_IN(7 downto 0);
				ROTATED_WORD(23 downto 16)	<= WMP_WORD_IN(15 downto 8);
				ROTATED_WORD(31 downto 24)	<= WMP_WORD_IN(23 downto 16);
		end case;
 	end process ROTATE;	

--------------------------------------------------------------------------------
--	Fuer Halbworte und Bytes ist von den beiden nieder-
--	wertigen Adressbits abhaengig, welcher Teilvektor innerhalb
--	eines Wortes rechts ins Ausgangswort geschrieben und erweitert wird.
--------------------------------------------------------------------------------
	RELEVANT_BYTE <= W(7 downto 0)		when ADDR = "00" else 
			 W(15 downto 8)		when ADDR = "01" else
			 W(23 downto 16)	when ADDR = "10" else
			 W(31 downto 24);

	RELEVANT_HW <= W(15 downto 0) when ADDR(1) = '0' else W(31 downto 16);
--------------------------------------------------------------------------------
--	Das Vorzeichenbit ist von der Transferinstruktion und der
--	gewuenschten Vorzeichenerweiterung abhaengig.
--------------------------------------------------------------------------------
	SIGN <= RELEVANT_HW(15) and S when H='1' else RELEVANT_BYTE(7) and S;
--------------------------------------------------------------------------------
--	Workaround weil WMP_WORD_OUT(15 downto 8) <= (others => sign) in der
-- 	Simulation nicht funktioniert.
--------------------------------------------------------------------------------
	with SIGN select
		SIGN_BYTE <=	X"FF" when '1',
				X"00" when others;
--------------------------------------------------------------------------------
--	Aufbau des resultierenden Wortes aus 2 Vorzeichenbytes und dem Halbwort
-- 	oder 3 Vorzeichenbytes und dem Nutzdatenbyte.
--	Wird ein Wort gelesen oder die ungueltige Kombination H=B=1 angelegt,
-- 	wird das ganze (ggf. rotierte) Wort unveraendert durchgeleitet 
--------------------------------------------------------------------------------
	WMP_WORD_OUT <= ROTATED_WORD when H = B else
			(SIGN_BYTE & SIGN_BYTE & RELEVANT_HW) when H = '1' and B = '0' else
			(SIGN_BYTE & SIGN_BYTE & SIGN_BYTE & RELEVANT_BYTE);

end BEHAVE;

