--------------------------------------------------------------------------------
--	Statusregister des ARM-Datenpfades
--------------------------------------------------------------------------------
--	Datum:		06.01.10
--	Version:	1.0
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ArmTypes.all;
use work.ArmConfiguration.all;

entity ArmProgramStatusRegister is
	port(
		PSR_CLK 		: in std_logic;
		PSR_RST			: in std_logic;
		PSR_ENABLE 		: in std_logic;		
		PSR_EXCEPTION_ENTRY 	: in std_logic;
		PSR_EXCEPTION_RETURN 	: in std_logic;
		PSR_SET_CC 		: in std_logic;
		PSR_WRITE_SPSR 		: in std_logic;
		PSR_XPSR_IN 		: in std_logic_vector(31 downto 0);
		PSR_CC_IN 		: in std_logic_vector(3 downto 0);
		PSR_MODE_IN 		: in std_logic_vector(4 downto 0);
		PSR_MASK 		: in std_logic_vector(3 downto 0);
		PSR_CPSR_OUT 		: out std_logic_vector(31 downto 0);
		PSR_SPSR_OUT 		: out std_logic_vector(31 downto 0)
		
	    );
end ArmProgramStatusRegister;

architecture BEHAVE of ArmProgramStatusRegister is

-------------------------------------------------------------------------------
-- 	Benoetigte Flipflops:
--	4 + 2 + 5 = 11 Sinnvolle FFs pro Statusregister x 6 = 66 FFs insgesamt
-------------------------------------------------------------------------------

-- 	Deklaration eines CPSR und eines Arrays mit 5 SPSR
	signal CPSR 		: PSR_TYPE := (PSR_CC => "0000", PSR_IF => "11", PSR_MODE => SUPERVISOR);
	signal SPSR 		: ARR_OF_PSR_TYPE := (others => ("0000","00","00000"));

	signal CURRENT_MODE 	: MODE;
--	Hilfssignal um illegale Zugriffe auf SPSR in den Modi USER/SYSTEM zu verhindern
	signal IN_EXCEPTION_MODE: BOOLEAN;

-------------------------------------------------------------------------------
-- 	Zuordung von Modus zu Index des SPSR-Arrays
-------------------------------------------------------------------------------
	function GET_SPSR_INDEX(THIS_MODE : MODE)return integer is
		variable TEMP : integer range 1 to 5 := 1;
	begin
		case THIS_MODE is
			when FIQ 		=>TEMP := 1;
			when IRQ 		=>TEMP := 2;
			when SUPERVISOR 	=>TEMP := 3;
			when ABORT 		=>TEMP := 4;
			when UNDEFINED	 	=>TEMP := 5;
			when others 		=>TEMP := 5;
		end case;
		return TEMP;
	end function GET_SPSR_INDEX;

-- 	Signal zur Indexierung des SPRS-Arrays
	signal SPSR_INDEX : integer range 1 to 5 := 5;

begin
	CURRENT_MODE		<= CPSR.PSR_MODE;
	IN_EXCEPTION_MODE	<= true when CURRENT_MODE /= USER AND CURRENT_MODE /= SYSTEM else false;	
	SPSR_INDEX		<= GET_SPSR_INDEX(CURRENT_MODE);
--------------------------------------------------------------------------------
--	Generieren von  CPSR- und SPSR-Ausgangs
-- 	Alle reservierten Bits	sind immer 0.
-- 	Fuer USER/SYSTEM liefert die Indexfunktion gegenwaertig den Index
-- 	von UNDEFINED, so dass in diesen Modi das UNDEFINED-SPSR bei
-- 	einem versuchten SPSR-Zugriff angezeigt wird.
--------------------------------------------------------------------------------
	PSR_CPSR_OUT	<= CPSR.PSR_CC & "0000" & X"0000" & CPSR.PSR_IF & '0' & CPSR.PSR_MODE;
	PSR_SPSR_OUT	<= SPSR(SPSR_INDEX).PSR_CC & "0000" & X"0000" & SPSR(SPSR_INDEX).PSR_IF & '0' & SPSR(SPSR_INDEX).PSR_MODE;

--------------------------------------------------------------------------------	
-- 	Schreibzugriff auf das CPSR oder das SPSR des aktuellen Modus
-- 	in Abhaengigkeit von den diversen Steuereingaengen
--------------------------------------------------------------------------------	
	SET_PSR : process(PSR_CLK) 
	begin
		if(PSR_CLK'event and PSR_CLK = '1')then
			if(PSR_RST = '1')then
				CPSR.PSR_CC <= (others => '0'); CPSR.PSR_IF <= "11"; CPSR.PSR_MODE <= SUPERVISOR;
				SPSR <= (others =>("0000","00","00000"));
			else
				CPSR <= CPSR;
				SPSR <= SPSR;
				if(PSR_ENABLE = '1') then
					if(PSR_EXCEPTION_ENTRY = '1')then
--------------------------------------------------------------------------------	
--	Wechsel in einen Ausnahmemodus als Teil der Ausnahmebehandlung
--	Der Eintritt in eine Exception kann kann nicht in den USER- oder
-- 	Systemmodus fuehren. Der aktuelle Modus ist irrelevant, Unterbrechungen
-- 	sind ggf. unterbrechbar.						
--------------------------------------------------------------------------------	
						case PSR_MODE_IN is
							when USER | SYSTEM => null;
							when UNDEFINED =>
								SPSR(GET_SPSR_INDEX(UNDEFINED)) <= CPSR;
--								IRQs werden maskiert, FIQs nicht
							      	CPSR.PSR_MODE <= UNDEFINED; CPSR.PSR_IF(1) <= '1'; 	
							when FIQ =>
								SPSR(GET_SPSR_INDEX(FIQ)) <= CPSR;
--								IRQs werden maskiert, FIQs ebenfalls
							       	CPSR.PSR_MODE <= FIQ; CPSR.PSR_IF <= "11"; 	
							when IRQ =>
								SPSR(GET_SPSR_INDEX(IRQ)) <= CPSR;
--								IRQs werden maskiert, FIQs nicht
							       	CPSR.PSR_MODE <= IRQ; CPSR.PSR_IF(1) <= '1'; 	
							when SUPERVISOR =>
								SPSR(GET_SPSR_INDEX(SUPERVISOR)) <= CPSR;
--								IRQs werden maskiert, FIQs nicht
							       	CPSR.PSR_MODE <= SUPERVISOR; CPSR.PSR_IF(1) <= '1'; 	
							when ABORT =>	
								SPSR(GET_SPSR_INDEX(ABORT)) <= CPSR;
--								IRQs werden maskiert, FIQs nicht
							       	CPSR.PSR_MODE <= ABORT; CPSR.PSR_IF(1) <= '1'; 	
							when others => null;	
						end case;					
					
					else
						if(PSR_EXCEPTION_RETURN = '1')then
--------------------------------------------------------------------------------	
--	Rueckkehr aus einer Ausnahmebehandlung
--	Eine Rueckkehr aus USER/SYSTEM ist auf diesem Weg nicht moeglich
--------------------------------------------------------------------------------	
							if(IN_EXCEPTION_MODE)then
								CPSR <= SPSR(SPSR_INDEX);
							end if;	
						else						
--------------------------------------------------------------------------------	
--	CC-Aktualisierung oder anderweitiger normaler Schreibzugriff
--------------------------------------------------------------------------------	
							if(PSR_SET_CC = '1')then
--------------------------------------------------------------------------------	
--	Gewoehnlicher Schreibzugriff auf den Conditioncode
--------------------------------------------------------------------------------	
								CPSR.PSR_CC <= PSR_CC_IN;	
							else
--------------------------------------------------------------------------------	
--	Keine Explizite Operation zum Moduswechsel und keine normale
-- 	CC-Aktivierung sondern eine MSR-Instruktion. Dabei muessen
-- 	Schreibzugriffe auf das CPSR und das SPSR des aktuellen Modus
-- 	unterschieden werden. In den Modi USER/SYSTEM wird der Versuch, 
-- 	auf das SPSR zu schreiben, ignoriert.
--------------------------------------------------------------------------------	
								if(PSR_WRITE_SPSR = '1' AND NOT IN_EXCEPTION_MODE)then
									null;
								elsif(PSR_WRITE_SPSR = '1' AND IN_EXCEPTION_MODE)then
--------------------------------------------------------------------------------	
--	Zugriff auf SPSR des aktuellen Modus	
--------------------------------------------------------------------------------	
									if(PSR_MASK(0) = '1' AND CURRENT_MODE /= USER)then
										SPSR(SPSR_INDEX).PSR_MODE <= PSR_XPSR_IN(4 downto 0);
										SPSR(SPSR_INDEX).PSR_IF <= PSR_XPSR_IN(7 downto 6);
									end if;	
									if(PSR_MASK(3) = '1')then
										SPSR(SPSR_INDEX).PSR_CC <= PSR_XPSR_IN(31 downto 28);
									end if;

								else
--------------------------------------------------------------------------------	
--	Zugriff auf CPSR des aktuellen Modus, im User-Modus koennen
-- 	nur die CC-Bits geaendert werden.	
--------------------------------------------------------------------------------	
									if(PSR_MASK(0) = '1' AND CURRENT_MODE /= USER) then
										CPSR.PSR_MODE <= PSR_XPSR_IN(4 downto 0);
										CPSR.PSR_IF <= PSR_XPSR_IN(7 downto 6);	
									end if;
--------------------------------------------------------------------------------	
--	Im USER-Mode sind nur Schreibzugriffe auf das höchstwertige Byte
-- 	(und dort den Condition Code) möglich
--------------------------------------------------------------------------------	
									if(PSR_MASK(3) = '1')then
										CPSR.PSR_CC <= PSR_XPSR_IN(31 downto 28);
									end if;
								end if;	
							end if;
						end if;	
					end if;	
				end if;

			end if;
		end if;
	end process SET_PSR;

end BEHAVE;
