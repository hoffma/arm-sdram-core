LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
--------------------------------------------------------------------------------

--Waitstategenerator mit nicht registriertem Ausgang. Das Wartesignal wird
--aktiviert, sobals das Startsignal gesetzt ist und gleichzeitung 
--der Initialwert gr��er null ist. Es wird davon ausgegangen, dass der Ausgang
--des �bergeordneten Moduls registriert ist. In diesem Fall liegt das Startsignal
--im gleichen Takt an, im dem das �bergeordnete Modul das Wartesignal potentiell 
--erstmals �berpr�ft.

entity ArmWaitStateGenAsync is
	GENERIC(
		COUNT_VALUE_WIDTH : integer := 32
	       );
	PORT(
		SYS_CLK : IN STD_LOGIC;
		SYS_RST : IN STD_LOGIC;
		WSG_COUNT_INIT : IN STD_LOGIC_VECTOR(COUNT_VALUE_WIDTH -1 downto 0);
		WSG_START : IN STD_LOGIC;
		WSG_WAIT : OUT STD_LOGIC
	    );
end ArmWaitStateGenAsync;

architecture BEHAVE of ArmWaitStateGenAsync is

--	SIGNAL WAIT_REG : STD_LOGIC := '0';
	SIGNAL COUNT_REG : UNSIGNED(COUNT_VALUE_WIDTH -1 downto 0) := (others => '0');
	SIGNAL COUNT_INIT : UNSIGNED (COUNT_VALUE_WIDTH -1 downto 0);
	constant ZERO : UNSIGNED (COUNT_VALUE_WIDTH -1 downto 0) := (others => '0');
begin
	COUNT_INIT <= unsigned(WSG_COUNT_INIT);
	WSG_WAIT <= '1' when COUNT_REG > ZERO OR (WSG_START = '1' AND COUNT_INIT > ZERO) else '0';

--------------------------------------------------------------------------------

--	Unter der Annahme eines sonst auf steigende Flanken 
--	reagierenden Systems tested der Waitstategenerator
--	das Anliegend von Eingangssignalen bei der fallenden
--	Flanke, wodurch das �bergeordnete Modul das Wartesignal bereits im
--	Takt nach der Aktivierung auswerten kann.
--	WSG_SET_WAIT_SIGNAL : PROCESS(SYS_CLK) 
--	begin
--		if SYS_CLK'event and SYS_CLK = '0' then
--			if SYS_RST = '1' then
--				WAIT_REG <= '0';
--			else
--				WAIT_REG <= WAIT_REG;
--				if ( WSG_START = '1' ) then
--					WAIT_REG <= '1';
--				else
--					if ( COUNT_REG = 0 ) then
--						WAIT_REG <= '0';
--					else
--						WAIT_REG <= '1';
--					end if;
--				end if;
--			end if;
--		end if;
--	end PROCESS WSG_SET_WAIT_SIGNAL;
--------------------------------------------------------------------------------

--	Die �bernahme des Initialwertes erfolgt mit der steigenden Flanke, 
--	ebenso das Z�hlen. 
--	Das Wartesignal ist immer mind. einen Takt aktiv, auch wenn der 
--	Initialwert 0 ist. Bei Initialwerten > 0 wird der Wert bereits 
--	w�hrend der �bernahme dekrementiert, denn es handelt sich aus
--	Sicht des �bergeordneten Moduls bereits um den ersten Wartezyklus.

	WSG_INIT_AND_COUNT : PROCESS(SYS_CLK) 
	begin
		if SYS_CLK'event and SYS_CLK = '1' then
			if SYS_RST = '1' then
				COUNT_REG <= (others => '0');
			else
				if WSG_START = '1' then
					if(COUNT_INIT = ZERO) then
						COUNT_REG <= ZERO;
					else
						COUNT_REG <= COUNT_INIT -1 ;
					end if;	
				else
					if COUNT_REG > ZERO then
						COUNT_REG <= COUNT_REG -1;
					else
						COUNT_REG <= ZERO;
					end if;
				end if;
			end if;	
		end if;
	end PROCESS WSG_INIT_AND_COUNT;


end BEHAVE;


