--------------------------------------------------------------------------------
--	Decoder zur Ermittlung der Instruktionsgruppe der aktuellen
--	Instruktion im der ID-Stufe im Kontrollpfad des HWPR-Prozessors.
--------------------------------------------------------------------------------
--	Datum:		19.06.2010
--	Version:	1.1
--------------------------------------------------------------------------------
--	Aenderungen:
--	Kleine kosmetische Aenderungen, sonst identisch zum HWPR 2009.	
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library work;
use work.ArmTypes.all;

--------------------------------------------------------------------------------
--	17 Instruktionsgruppen:
--	CD_UNDEFINED
--	CD_SWI
--	CD_COPROCESSOR
--	CD_BRANCH
--	CD_LOAD_STORE_MULTIPLE
--	CD_LOAD_STORE_UNSIGNED_IMMEDIATE
--	CD_LOAD_STORE_UNSIGNED_REGISTER
--	CD_LOAD_STORE_SIGNED_IMMEDIATE
--	CD_LOAD_STORE_UNSIGNED_REGISTER
--	CD_ARITH_IMMEDIATE
--	CD_ARITH_REGISTER
--	CD_ARITH_REGISTER_REGISTER
--	CD_MSR_IMMEDIATE
--	CD_MSR_REGISTER
--	CD_MRS
--	CD_MULTIPLY
--	CD_SWAP

-- 	UNDEFINED wird durch den Nullvektor angezeigt, die anderen
--	Befehlsgruppen durch einen 1-aus-16-Code.
--------------------------------------------------------------------------------

entity ArmCoarseInstructionDecoder is
	port(
		CID_INSTRUCTION		: in std_logic_vector(31 downto 0);
		CID_DECODED_VECTOR	: out std_logic_vector(15 downto 0)
	    );
end ArmCoarseInstructionDecoder;

architecture BEHAVE of ArmCoarseInstructionDecoder is
	signal DECV	: COARSE_DECODE_TYPE;
--	Hilfssignale im Entscheidungsprozess
	signal INST_7_4 : std_logic_vector(1 downto 0);
	signal INST_20_6 : std_logic_vector(1 downto 0);
	
begin
	CID_DECODED_VECTOR	<= DECV;
	INST_7_4		<= CID_INSTRUCTION(7) & CID_INSTRUCTION(4);
	INST_20_6		<= CID_INSTRUCTION(20) & CID_INSTRUCTION(6);

	COARSE_DECODE : process(CID_INSTRUCTION, INST_7_4, INST_20_6) is
		alias INST : std_logic_vector(31 downto 0) is CID_INSTRUCTION;
	begin
		DECV <= CD_UNDEFINED;
		case INST(27 downto 25) IS
			when "111" =>
				if INST(24) = '1' then
					DECV <= CD_SWI;
				else
					DECV <= CD_COPROCESSOR;
				end if;
			when "110" =>
				DECV <= CD_COPROCESSOR;
			when "101" =>
				DECV <= CD_BRANCH;
			when "100" =>
				DECV <= CD_LOAD_STORE_MULTIPLE;
			when "011" =>
				if INST(4) = '0' then
					DECV <= CD_LOAD_STORE_UNSIGNED_REGISTER;
				else
					DECV <= CD_UNDEFINED;
				end if;
			when "010" =>
				DECV <= CD_LOAD_STORE_UNSIGNED_IMMEDIATE;
			when "001" =>
				if INST(24 downto 23) = "10" then
					CASE INST(21 downto 20) IS
						when "10" =>	DECV <= CD_MSR_IMMEDIATE;
						when "00" => 	DECV <= CD_UNDEFINED;
						when "01"|"11" =>	DECV <= CD_ARITH_IMMEDIATE;
						when others =>	DECV <= (others => 'X');
					END CASE;	
				else
					DECV <= CD_ARITH_IMMEDIATE;
				end if;
			when "000" =>
				CASE INST_7_4 is
					when "11" =>
						if INST(6 downto 5) = "00" then
							if INST(24) = '0' then
								DECV <= CD_MULTIPLY;
							else
								if INST(23) & INST(21 downto 20) = "000" then
									DECV <= CD_SWAP;
								else
									DECV <= CD_UNDEFINED;
								end if;
							end if;
						else
							if INST_20_6 = "01" then
								DECV <= CD_UNDEFINED;
							else
								if INST(22) = '1' then
									DECV <= CD_LOAD_STORE_SIGNED_IMMEDIATE;
								else
									DECV <= CD_LOAD_STORE_SIGNED_REGISTER;
								end if;
							end if;	
						end if;
					when "10" =>
						if INST(24 downto 23) = "10" then
							if INST(20) = '1' then
								DECV <= CD_ARITH_REGISTER;
							else
								DECV <= CD_UNDEFINED;

							end if;
						else
							DECV <= CD_ARITH_REGISTER;
						end if;
					when "01" =>
						if INST(24 downto 23) = "10" then
							if INST(20) = '1' then
								DECV <= CD_ARITH_REGISTER_REGISTER;
							else
								DECV <= CD_UNDEFINED;
							end if;

						else
							DECV <= CD_ARITH_REGISTER_REGISTER;
						end if;	
					when others => --"00" =>
						if INST(24 downto 23) = "10" then
							if INST(20) = '1' then
								DECV <= CD_ARITH_REGISTER;
							else
								if INST(21) = '1' then
									DECV <= CD_MSR_REGISTER;
								else
									DECV <= CD_MRS;
								end if;
							end if;
						else
							DECV <= CD_ARITH_REGISTER;
						end if;
				end case;
			when others =>	
--	Nur fuer die Simulation relevant
-- 				DECV <= (others => 'U');
				DECV <= CD_UNDEFINED;
		end case;	
	end process COARSE_DECODE;	

--------------------------------------------------------------------------------
--	Test fuer die Verhaltenssimulation der nur aktiviert werden sollte, wenn
-- 	die Befehlsgruppen durch einen 1-aus-n-Code dargestellt werden.
--	Getestet wird, ob in  DECV maximal ein Bit gesetzt ist.
--------------------------------------------------------------------------------
-- synthesis translate_off
 	CHECK_NR_OF_SIGNALS : process(CID_INSTRUCTION,DECV)IS
 		variable NR : integer range 0 to 16 := 0;
 	begin
 		NR := 0;
 		for i in DECV'range loop
 			if DECV(i) = '1' then
 				NR := NR + 1;
 			end if;
 		end loop;
 		if DECODE_TYPE_VARIANT = 0 then
  			assert NR <= 1 report "Fehler in ArmCoarseInstructionDecoder: Instruktion nicht eindeutig erkannt." severity error;
 		end if;	
 	end process CHECK_NR_OF_SIGNALS;
-- synthesis translate_on
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
end BEHAVE;
