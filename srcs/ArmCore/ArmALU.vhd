--------------------------------------------------------------------------------
--	ALU des ARM-Datenpfades
--------------------------------------------------------------------------------
--	Datum:		30.05.2010
--	Version:	1.2
--------------------------------------------------------------------------------
--	Die Ursprungsversion der ALU folgert nicht nur das V-Bit aus dem MSB
--	der Operanden (a,b) und des Ergebnisses (z) sondern auch das C-Bit.
--	Jenes kann alternativ auch durch eine kuenstliche Erweiterung der
--	Operanden um eine fuehrende 0 und die Zuweisung an 33-Bit breiten
--	Ergebnisvektor gewonnen werden.
--------------------------------------------------------------------------------
--	Neufassung (betrifft nicht das HWPR sondern ist fuer eine Steigerung
--	der Prozessorfrequenz im Praxiseinsatz gedacht):
--	Die ALU wurde gruendlich ueberarbeitet und fasst nun jeweils eine
--	optimierte Teilkomponente fuer logische und arithmetische Operationen
--	zusammen. Sie Multiplext deren Ausgaenge und erzeugt den Condition
--	Code. Spielraum fuer Verbesserungen bleibt ggf. noch bei der Erzeugung
--	des Z-Bts. Bisherige Versuche, das Z-Bit der arithmetischen Instruktionen
--	sehr schnell zu erzeugen (noch in der arithmetischen Komponente) haben
--	keine nennenswerten Verbesserungen gebracht	
--	Fuer das HWPR wurden die Konfigurationsoptionen aus ArmConfiguration
--	entfernt und lokal in ArmALU vorgesehen.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ArmTypes.all;
--------------------------------------------------------------------------------
--	Konfigurationseinstellung zur Verwendung der ueberarbeiteten ALU
--------------------------------------------------------------------------------
-- use work.ArmConfiguration.USE_ALU_V2;
--------------------------------------------------------------------------------
--	Konfigurationseinstellung zur Verwendung einer dedizierten Schaltung
--	fuer die Bestimmung des Z-Bits bereits in den Teilkomponenten der ALU
--------------------------------------------------------------------------------
-- use work.ArmConfiguration.USE_DEDICATED_ARITH_ZERO_ALU;

entity ArmALU is
    port ( ALU_OP1 	: in	STD_LOGIC_VECTOR(31 downto 0);
           ALU_OP2 	: in 	STD_LOGIC_VECTOR(31 downto 0);           
    	   ALU_CTRL 	: in	STD_LOGIC_VECTOR(3 downto 0);
    	   ALU_CC_IN 	: in	STD_LOGIC_VECTOR(1 downto 0);
	   ALU_RES 	: out	STD_LOGIC_VECTOR(31 downto 0);
	   ALU_CC_OUT	: out	STD_LOGIC_VECTOR(3 downto 0)
   	);
end ArmALU;

architecture BEHAVE of ArmALU is

--------------------------------------------------------------------------------	
--	Fuer das HWPR lokal eingefuehrte Konstanten, in der 
--	Produktivversion des ARM-Kerns sind diese in ArmConfiguration deklariert
--------------------------------------------------------------------------------	
	constant USE_ALU_V2 : boolean := false;
	constant USE_DEDICATED_ARITH_ZERO_ALU : boolean := false;
begin	

--------------------------------------------------------------------------------
--	Die urspruengliche und sehr primitive Fassung der ALU
--	Sie dient im Wesentlichen noch als Vergleichsgrundlage
--	fuer die Neufassung und um eine Loesung zu zeigen, die weder
--	auf vertieften VHDL- noch FPGA-Kenntnissen aufbaut (fuer das HWPR).
--------------------------------------------------------------------------------
	GEN_ALU_V1 : if not USE_ALU_V2	generate
--------------------------------------------------------------------------------
--	Alias fuer die Eingangsoperanden um die Aehnlichkeit zur ISA
--	herauszustellen.	
--------------------------------------------------------------------------------
		alias Rn	: std_logic_vector(31 downto 0) is ALU_OP1;
		alias Operand2	: std_logic_vector(31 downto 0) is ALU_OP2;

--------------------------------------------------------------------------------
--	Trennung von C und V aus dem CC-Eingangsvektor, N und Z werden immer neu
--	gesetzt und deshalb hier nicht beruecksichtigt.
--------------------------------------------------------------------------------
		alias ALU_C_IN	: std_logic is ALU_CC_IN(1);
		alias ALU_V_IN	: std_logic is ALU_CC_IN(0);
--	Einzelne Komponenten des CC-Ausgangsvektors	
		signal ALU_N_OUT, ALU_Z_OUT, ALU_C_OUT, ALU_V_OUT : std_logic := '0';

--------------------------------------------------------------------------------
--	Verknuepfungergebnis, wird zur Bestimmung des Condition Code einmal als
--	Modulinternes Signal benoetigt.
--------------------------------------------------------------------------------
		signal Rd : std_logic_vector(31 downto 0);
--	Vektor fuer 31 Nullen	
		signal JBZ : std_logic_vector(30 downto 0);
		signal ALU_C_IN_VECTOR: std_logic_vector(31 downto 0);
		signal ALU_C_IN_NOT_VECTOR: std_logic_vector(31 downto 0);

		begin

			ALU_CC_OUT(3) <= ALU_N_OUT;
			ALU_CC_OUT(2) <= ALU_Z_OUT;
			ALU_CC_OUT(1) <= ALU_C_OUT;
			ALU_CC_OUT(0) <= ALU_V_OUT;

			JBZ <= (others => '0');

--------------------------------------------------------------------------------
--	Erweiterung von des Carry-Eingangs auf einen Vektor um ihn
--	einfacher als 3. Operanden verwenden zu koennen.
--------------------------------------------------------------------------------
			ALU_C_IN_VECTOR <= (JBZ & ALU_C_IN);
			ALU_C_IN_NOT_VECTOR <= (JBZ & not ALU_C_IN);

--------------------------------------------------------------------------------
--	Berechnung des Ergebnisses durch VHDL-Arithmetikoperatoren
--	Die verwendeten Bezeichner sind fuer Werte von ALU_CTRL sind Konstanten
--	aus ArmTypes.vhd um Fluechtigkeitsfehler bei den Operationscodes zu 
--	vermeiden.
--------------------------------------------------------------------------------
			compute_res : process(ALU_CTRL,ALU_OP1,ALU_OP2,ALU_C_IN_VECTOR,ALU_C_IN_NOT_VECTOR)
			begin	
				Rd <= (others => '0');
				case ALU_CTRL is
					when OP_AND | OP_TST 	=>
						Rd <= Rn and Operand2;			
					when OP_EOR | OP_TEQ 	=>
						Rd <= Rn xor Operand2;			
					when OP_SUB | OP_CMP 	=>
						Rd <= std_logic_vector(signed(Rn) - signed(Operand2));							
					when OP_RSB 		=>
						Rd <= std_logic_vector(signed(Operand2) - signed(Rn));
					when OP_ADD | OP_CMN 	=>
						Rd <= std_logic_vector(signed(Rn) + signed(Operand2));
					when OP_ADC 		=>
						Rd <= std_logic_vector(signed(Rn) + signed(Operand2) + signed(ALU_C_IN_VECTOR));			
					when OP_SBC 		=>
						Rd <= std_logic_vector(signed(Rn) - signed(Operand2) - signed(ALU_C_IN_NOT_VECTOR));
					when OP_RSC 		=>
						Rd <= std_logic_vector(signed(Operand2) - signed(Rn) - signed(ALU_C_IN_NOT_VECTOR));
					when OP_ORR 		=>
						Rd <= Rn or Operand2;
					when OP_BIC 		=>
						Rd <= Rn and (not Operand2);
					when OP_MVN 		=>	
						Rd <= not Operand2;
					when others => --OP_MOV
						Rd <= Operand2;
				end case;
			end process compute_res;

--------------------------------------------------------------------------------
--	Z und N lassen sich Operationsunabhaengig am Ergebnis ablesen
--------------------------------------------------------------------------------
			ALU_Z_OUT	<= '1' when Rd = X"00000000" else '0';
			ALU_N_OUT	<= '1' when Rd(31)='1' else '0';
			ALU_RES		<= Rd;

--------------------------------------------------------------------------------
--	Setzen der Flags auf Basis der Operanden und des Ergebnisses.
--	Es gilt zu beachten, dass alle Subtraktionsbefehle (SUB,SBC,RSB,RSC,CMP)
--	das C-Flag gerade dann setzen, wenn kein Uebertrag/Borrow auftritt!
--------------------------------------------------------------------------------
	compute_flags : process(Rd,ALU_CTRL,ALU_OP1,ALU_OP2,ALU_C_IN,ALU_V_IN,ALU_CC_IN)
				alias a is ALU_OP1(31);
				alias b is ALU_OP2(31);
				alias z is Rd(31);
			begin	
				ALU_C_OUT <= '0';
				ALU_V_OUT <= '0';
				case ALU_CTRL is
					when OP_SUB | OP_CMP | OP_SBC   =>
--------------------------------------------------------------------------------
--	Carry der Subtraktion aus Operanden und Ergebnis abgeleitet:
--	c=!ab + Cin(!a xor b) = !ab + z(!(a xor b))
--	A ist kleiner als B -> Carry; 
--	A und B haben das gleiche MSB und das MSB des Ergebnisses ist 1 
--	-> Carry, weil ein Uebertrag in das MSB (Cin) aufgetreten sein muss
--	C wird mit dem _Gegenteil_ des Uebertrags gesetzt	
--------------------------------------------------------------------------------
						ALU_C_OUT <=not (((not a) and b) or (a and b and z) or ((not a) and (not b) and z));
--------------------------------------------------------------------------------
--	Overflow: Subtraktion von Operanden mit gleichem Vorzeichen:
--	Ergebnis erzeugt keinen Overflow 
--	Unterschiedliche MSBs: Minuend und Ergebnis haben unterschiedliche
--	Vorzeichen -> Overflow:
--	Minuend +, Subtrahend -, Differenz -: die Subtraktion einer negativen
--	Zahl kann nicht zum negativen Ergebnis fuehren
--	Minuened -, Subtrahend +, Differnez +: Die Subtraktion einer Positiven
--	Zahl von einer negativen kann nicht zum positiven Ergebnis fuehren
--------------------------------------------------------------------------------
						ALU_V_OUT <= (a and (not b) and (not z)) or ((not a) and b and z );
					when OP_RSB | OP_RSC		=>
--------------------------------------------------------------------------------
--	Wie die Subtraktion, aber die Rollen von a und b sind vertauscht
--	C wird mit dem Gegenteil des Uebertrags gesetzt
--------------------------------------------------------------------------------
						ALU_C_OUT <=not ((a and (not b)) or (a and b and z) or ((not a) and (not b) and z));
						ALU_V_OUT <= ((not a) and b and (not z)) or (a and (not b) and z );
					when OP_ADD | OP_CMN | OP_ADC 	=>
--------------------------------------------------------------------------------
--	Carry wenn die MSBs beider Operanden = 1 sind oder eines der MSBs 1 ist
--	und das MSB des Ergebnisses 0 (In diesem Fall fand ein Uebertrag in die
--	hoechste Stelle statt).
--------------------------------------------------------------------------------
						ALU_C_OUT <= (Rn(31) and Operand2(31)) or ((not Rd(31)) and (Rn(31) xor Operand2(31)));
--------------------------------------------------------------------------------
--	Ueberlauf der Addition: beide Operanden haben das
--	gleiche Vorzeichen(bit) und dieses Vorzeichen entspricht nicht dem des
--	Ergebnisses => C = !a!bz + ab!z
--------------------------------------------------------------------------------
						ALU_V_OUT <= (((not a) and (not b)) and z) or (a and b and (not z));
					when others =>
--------------------------------------------------------------------------------
--	OP_AND | OP_TST | OP_EOR | OP_TEQ | OP_ORR | OP_MOV | OP_BIC | OP_MVN
--	Logische Operationen aendern V nicht, C wurde ggf. vom Shifter veraendert
--	und durch die ALU durchgereicht.		
--------------------------------------------------------------------------------
						ALU_C_OUT <= ALU_C_IN;
						ALU_V_OUT <= ALU_V_IN;		
				end case;
			end process compute_flags;
	end generate GEN_ALU_V1;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Version 2 der ALU (irrelevant fuer das HWPR!)
--	Die ALU instanziiert zwei Teilkomponenten fuer die logische und 
--	arithmetische Verknuepfungen.
--------------------------------------------------------------------------------
	GEN_ALU_V2 : if USE_ALU_V2 generate

--------------------------------------------------------------------------------
--	Signale fuer die einzelnen Bestandteile der CC-Eingangsvektoren
--	und Ausgangsvektoren um die Lesbarkeit zu verbesern.
--------------------------------------------------------------------------------
		signal ALU_C_IN, ALU_V_IN : std_logic := '0';
		signal ALU_N_OUT, ALU_Z_OUT, ALU_C_OUT, ALU_V_OUT : std_logic := '0';

--	Signale fuer Ergebnisse und CC-Bits der einzelnen Teilkomponenten
		signal LOGIC_RES	: std_logic_vector(31 downto 0);
		signal ARITH_RES	: std_logic_vector(31 downto 0);
		signal ARITH_C,ARITH_V	: std_logic;
		signal LOGIC_Z_OUT, ARITH_Z_OUT : std_logic;

		signal Rd : std_logic_vector(31 downto 0);


--------------------------------------------------------------------------------
--	Deklaration und Instanziierung beider Teilkomponenten
--------------------------------------------------------------------------------
		component ArmALULogicPart
		port(
			OP1	: in std_logic_vector(31 downto 0);
			OP2	: in std_logic_vector(31 downto 0);
			ALU_CTRL: in std_logic_vector(3 downto 0);          
			RES	: out std_logic_vector(31 downto 0);
			Z_OUT	: out std_logic
		);
		end component;

		component ArmALUArithPart
		port(
			OP1	: in std_logic_vector(31 downto 0);
			OP2	: in std_logic_vector(31 downto 0);
			CTRL	: in std_logic_vector(3 downto 0);
			C_IN	: in std_logic;          
			RES	: out std_logic_vector(31 downto 0);
			C_OUT	: out std_logic;
			Z_OUT	: out std_logic;		
			V_OUT	: out std_logic
		);
		end component;

	begin
		Inst_ArmALULogicPart: ArmALULogicPart port map(
			OP1	=> ALU_OP1,
			OP2	=> ALU_OP2,
			ALU_CTRL=> ALU_CTRL,
			RES	=> LOGIC_RES,
			Z_OUT	=> LOGIC_Z_OUT
		);

		Inst_ArmALUArithPart: ArmALUArithPart 
		port map(
			OP1	=> ALU_OP1,
			OP2	=> ALU_OP2,
			CTRL	=> ALU_CTRL,
			C_IN	=> ALU_C_IN,
			RES	=> ARITH_RES,
			C_OUT	=> ARITH_C,
			Z_OUT	=> ARITH_Z_OUT,
			V_OUT	=> ARITH_V 
		);

		ALU_C_IN <= ALU_CC_IN(1);
		ALU_V_IN <= ALU_CC_IN(0);

		ALU_CC_OUT(3) <= ALU_N_OUT;
		ALU_CC_OUT(2) <= ALU_Z_OUT;
		ALU_CC_OUT(1) <= ALU_C_OUT;
		ALU_CC_OUT(0) <= ALU_V_OUT;	
		
--------------------------------------------------------------------------------
--	Multiplexen des ALU-Ergebnisausgangs in Abhaengigkeit
--	von der Operation. Entweder wird das Ergebnis der
--	arithmetischen oder das der logischen Teilkomponente
--	uebernommen.	
--------------------------------------------------------------------------------
		compute_res : process(ALU_CTRL,LOGIC_RES,ARITH_RES)is		
		begin	
			Rd <= (others => '0');
			case ALU_CTRL is
				when OP_SUB | OP_CMP | OP_CMN | OP_RSB | OP_ADD | OP_ADC | OP_SBC | OP_RSC =>
					Rd <= ARITH_RES;
				when others => 
					Rd <= LOGIC_RES;
			end case;
		end process compute_res;

--------------------------------------------------------------------------------
--	Erzeugung des Z-Bits
--	Mit USE_DEDICATED_ARITH_ZERO_ALU = true erzeugen beide Teilkomponenten
--	der ALU ihr Z-Bit selbst. Die Idee ist, dass dies speziell f�r 
--	arithmetische Verknuepfungen in einer zweiten Carry-Chain parallel fuer 
--	die der Ergebnisse geschehen kann.
--	Mit USE_DEDICATED_ARITH_ZERO_ALU = false erfolgt die Bestimmung des 
--	Z-Bits mit einer gemeinsamen Schaltung auf Basis des gemultiplexten
--	Ergbnisses.
--	Ueberraschenderweise bringen die beiden separaten Z-Schaltungen
--	keinen Laufzeitgewinn in der isolierten ALU, sehr wohl aber bei
--	Verwendung im Datenpfad. Der Grund hierfuer ist nicht bekannt.
--	Die Erzeugung des Z-Bits fuehrt in jedem Fall zu einer zusaetzlichen
--	Verzoegerung (bis 2 ns) der Gesamtschaltung. Die anderen CC-Bits haben
--	diese Wirkung naturgemaess nicht. Allerdings durchlaufen das C- und V-
--	Bit im Datenpfad noch einen weiteren Multiplexer so dass die Strafe 
--	durch das Z-Bit letztlich weniger ausgepraegt ist als es bei isolierter
--	Betrachtung der ALU den Anschein hat.
--------------------------------------------------------------------------------
		compute_z : process(Rd,ALU_CTRL,ARITH_Z_OUT,LOGIC_Z_OUT)is
		begin
			if not USE_DEDICATED_ARITH_ZERO_ALU then
				if Rd = X"00000000" then
					ALU_Z_OUT <= '1';
				else
					ALU_Z_OUT <= '0';
				end if;	
			else
				case ALU_CTRL is
					when OP_SUB | OP_CMP | OP_CMN | OP_RSB | OP_ADD | OP_ADC | OP_SBC | OP_RSC =>
						ALU_Z_OUT <= ARITH_Z_OUT;
					when others => 
						ALU_Z_OUT <= LOGIC_Z_OUT;
				end case;
			end if;
		end process compute_z;

--------------------------------------------------------------------------------
-- 	Das N-Bit entspricht unmittelbar dem MSB des Ergebnisses.
--------------------------------------------------------------------------------
		ALU_N_OUT <= Rd(31);
		ALU_RES <= Rd;
		
--------------------------------------------------------------------------------
--		C- und V-Bit werden bei logischen Verknuepfungen durch die ALU
--		geleitet und bei Arithmetischen Verkn�pfungen durch die 
--		arithmetische Teilkomponente neu gesetzt.
--------------------------------------------------------------------------------
		compute_flags : process(ALU_CTRL,ALU_C_IN,ALU_V_IN,ARITH_C,ARITH_V)
		begin			
			case ALU_CTRL is
				when OP_SUB | OP_CMP | OP_CMN | OP_RSB | OP_ADD | OP_ADC | OP_SBC | OP_RSC =>
					ALU_C_OUT <= ARITH_C;
					ALU_V_OUT <= ARITH_V;		
				when others => 
					ALU_C_OUT <= ALU_C_IN;
					ALU_V_OUT <= ALU_V_IN;
			end case;
		end process compute_flags;
	end generate GEN_ALU_V2;
end BEHAVE;
