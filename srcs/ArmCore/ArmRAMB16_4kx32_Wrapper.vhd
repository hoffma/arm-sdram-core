--------------------------------------------------------------------------------
--	Wrapper um Spartan3E-Blockram fuer den RAM des ARM-SoC
--------------------------------------------------------------------------------
--	Datum:		14.05.2010
--	Version:	1.00
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ArmRAMB16_4kx32_Wrapper is
	generic(
--------------------------------------------------------------------------------
--	Jeder Byte-Ramblock im Spartan3E umfasst 2k Adressen, maximal koennen
--	4 x 4 = 16 Rambloecke alloziert werden, minimal 4 x 1 = 4.
--	Sinnvollerweise sollten immer Zweierpotenzen verwendet werden.
--	SELECT_LINES gibt an, wie viele Bit einer Datenbusadresse zusaetzlich
--	zu den minimal notwendigen 11 Bit herangezogen werden, um
--	die Block-"Zeile" auszuwaehlen.	Bei nur 2Ki Adressen wird keine weitere 
--	Adressleitung benoetigt. Bei 4Ki-Adressen und zwei Blockzeilen ist 
--	eine zusaetzliche Leitung noetig, dies ist die Variante im HWPR. Im
--	Maximalausbau des Speichers fuer den Spartan3E 500 sind 4 Zeilen
--	moeglich und damit zwei zusaetzliche Zeilen.
--------------------------------------------------------------------------------
		SELECT_LINES : natural range 0 to 2 := 1	
	);
    port(
	RAM_CLK	: in  std_logic;
        RAM_RST : in  std_logic;
	WEA 	: in  std_logic_vector(3 downto 0);
        ENA	: in  std_logic;
--	Fuer 2 Speicherblockzeilen werden nur die Adressbits 11 downto 0 benoetigt
	ADDRA	: in  std_logic_vector (10 + SELECT_LINES downto 0);
        DIA	: in  std_logic_vector (31 downto 0);
        WEB	: in  std_logic_vector(3 downto 0);
        ENB	: in  std_logic;
	ADDRB	: in  std_logic_vector (10 + SELECT_LINES downto 0);
        DIB	: in  std_logic_vector (31 downto 0);
        DOA	: out  std_logic_vector (31 downto 0);
        DOB	: out  std_logic_vector (31 downto 0)
	);
end ArmRAMB16_4kx32_Wrapper;

architecture BEHAVE of ArmRAMB16_4kx32_Wrapper is
	component ArmRAMB16_2kx8_Wrapper
	port(
		RAM_CLK : in std_logic;
		RAM_RST : in std_logic;
		WEA : in std_logic;
		ENA : in std_logic;
		ADDRA : in std_logic_vector(10 downto 0);
		DIA : in std_logic_vector(7 downto 0);
		WEB : in std_logic;
		ENB : in std_logic;
		ADDRB : in std_logic_vector(10 downto 0);
		DIB : in std_logic_vector(7 downto 0);          
		DOA : out std_logic_vector(7 downto 0);
		DOB : out std_logic_vector(7 downto 0)
		);
	end component;
--------------------------------------------------------------------------------
--	Zur Auswahl zwischen der vollstaendigen Fassung
--	des Speichers fuer das ARM-SoC und der 
--	vereinfachten Fassung fuer das HWPR.
--------------------------------------------------------------------------------
	constant HWPR_VERSION : boolean := true;

	type		LOCAL_DO_TYPE is array((2 ** SELECT_LINES)-1 downto 0) of std_logic_vector(31 downto 0);
	constant	RAM_LINES : natural := 2** SELECT_LINES;
	signal		LOCAL_DOA : LOCAL_DO_TYPE;
	signal		LOCAL_DOB : LOCAL_DO_TYPE;
begin
		
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Einfache Fassung des Speichers fuer das HWPR mit fester Zahl
--	instanziierter Bloecke (Musterloesung zu Aufgabenblatt 4).
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
	GEN_HWPR_VERSION : if HWPR_VERSION generate
	begin
	
	GEN_RAM_LINE : for RAM_LINE in 1 downto 0 generate
		signal LOCAL_WEA : std_logic_vector(3 downto 0);
		signal LOCAL_WEB : std_logic_vector(3 downto 0);
	begin	
--------------------------------------------------------------------------------
--	Jede "Zeile" benoetigt eigene Write-Enable-Signale
--	in Abhaengigkeit vom hoechsten Adressbit.
--------------------------------------------------------------------------------
		DETERMINE_WRITE_ENABLES : process(WEA,WEB,ADDRA,ADDRB) 
		begin
--------------------------------------------------------------------------------
--	In jeder Speicher"zeile" gibt es vier Spalten, um byteweise
--	Schreibzugriffe zu ermoeglichen
--------------------------------------------------------------------------------
			for i in 0 to 3 loop
				if RAM_LINE = 1 then
					LOCAL_WEA(i) <= WEA(i) and ADDRA(11);
					LOCAL_WEB(i) <= WEB(i) and ADDRB(11);			
				else
					LOCAL_WEA(i) <= WEA(i) and NOT ADDRA(11);			
					LOCAL_WEB(i) <= WEB(i) and NOT ADDRB(11);			
				end if;
			end loop;	
		end process DETERMINE_WRITE_ENABLES;

		GEN_COLUMN : for RAM_COLUMN in 3 downto 0 generate
			BYTE_BLOCK : entity work.ArmRAMB16_2kx8_Wrapper(BEHAVE) port map(
				RAM_CLK		=> RAM_CLK,
				RAM_RST		=> RAM_RST,
				WEA		=> LOCAL_WEA(RAM_COLUMN),
				ENA		=> ENA,
				ADDRA		=> ADDRA(10 downto 0),
				DIA		=> DIA(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				WEB		=> LOCAL_WEB(RAM_COLUMN),
				ENB		=> ENB,
				ADDRB		=> ADDRB(10 downto 0),
				DIB		=> DIB(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				DOA		=> LOCAL_DOA(RAM_LINE)(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				DOB		=> LOCAL_DOB(RAM_LINE)(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8)
			);
		end generate GEN_COLUMN;	

	end generate GEN_RAM_LINE;

--	Auswahl des richtigen Ausgangs
	DOA	<= LOCAL_DOA(1) when ADDRA(11)='1' else
		   LOCAL_DOA(0);
	DOB	<= LOCAL_DOB(1) when ADDRB(11)='1' else
		   LOCAL_DOB(0);

	end generate GEN_HWPR_VERSION;
--------------------------------------------------------------------------------
--	Ende der HWPR-Musterloesung
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Vollstaendige Fassung mit waehlbarer Groesse des Speichers.
--	Irrelevant fuer das HWPR.
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
	GEN_NON_HWPR_VERSION : if not HWPR_VERSION generate 

	GEN_RAM_LINE : for RAM_LINE in RAM_LINES-1 downto 0 generate	
		signal LOCAL_WEA : std_logic_vector(3 downto 0);
		signal LOCAL_WEB : std_logic_vector(3 downto 0);
	begin
--------------------------------------------------------------------------------
--		Jede "Zeile" benoetigt eigene Write-Enable-Signale
--		in Abhaengigkeit vom hoechsten Adressbit.
--------------------------------------------------------------------------------
		DETERMINE_WRITE_ENABLES : PROCESS(WEA,WEB,ADDRA,ADDRB) 
		begin
--------------------------------------------------------------------------------
--	In jeder Speicher"zeile" gibt es vier Spalten, um byteweise
--	Schreibzugriffe zu ermoeglichen
--------------------------------------------------------------------------------
			for i in 0 to 3 loop
				if RAM_LINES = 1 then
					LOCAL_WEA(i) <= WEA(i);			
					LOCAL_WEB(i) <= WEB(i);			
				elsif RAM_LINES = 2 then
					if RAM_LINE = 1 then
						LOCAL_WEA(i) <= WEA(i) and ADDRA(11);
						LOCAL_WEB(i) <= WEB(i) and ADDRB(11);			
					else
						LOCAL_WEA(i) <= WEA(i) and not ADDRA(11);			
						LOCAL_WEB(i) <= WEB(i) and not ADDRB(11);			
					end if;
				else
					case RAM_LINE is
					when 3 =>
						LOCAL_WEA(i) <= WEA(i) and ADDRA(12) and ADDRA(11);			
						LOCAL_WEB(i) <= WEB(i) and ADDRB(12) and ADDRB(11);			
					when 2 =>
						LOCAL_WEA(i) <= WEA(i) and ADDRA(12) and not ADDRA(11);			
						LOCAL_WEB(i) <= WEB(i) and ADDRB(12) and not ADDRB(11);			
					when 1 =>
						LOCAL_WEA(i) <= WEA(i) and (not ADDRA(12)) and ADDRA(11);			
						LOCAL_WEB(i) <= WEB(i) and (not ADDRB(12)) and ADDRB(11);			
					when others =>
						LOCAL_WEA(i) <= WEA(i) and (not ADDRA(12)) and not ADDRA(11);			
						LOCAL_WEB(i) <= WEB(i) and (not ADDRB(12)) and not ADDRB(11);			
					end case;			
				end if;
			end loop;	
		end PROCESS DETERMINE_WRITE_ENABLES;
		GEN_COLUMN : for RAM_COLUMN in 3 downto 0 generate
			BYTE_BLOCK : entity work.ArmRAMB16_2kx8_Wrapper(BEHAVE) port map(
				RAM_CLK		=> RAM_CLK,
				RAM_RST		=> RAM_RST,
				WEA		=> LOCAL_WEA(RAM_COLUMN),
				ENA		=> ENA,
				ADDRA		=> ADDRA(10 downto 0),
				DIA		=> DIA(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				WEB		=> LOCAL_WEB(RAM_COLUMN),
				ENB		=> ENB,
				ADDRB		=> ADDRB(10 downto 0),
				DIB		=> DIB(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				DOA		=> LOCAL_DOA(RAM_LINE)(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8),
				DOB		=> LOCAL_DOB(RAM_LINE)(((RAM_COLUMN + 1)* 8) - 1 downto RAM_COLUMN * 8)
			);
		end generate GEN_COLUMN;
	end generate GEN_RAM_LINE;

--------------------------------------------------------------------------------
--	Unsaubere Auswahl von ADDRA und ADDRB, ADDRA(10) wird nicht benoetigt,
-- 	aber so kann fuer SELECT_LINES = 0 eine ungueltige Auswahl 
-- 	von 10 (11 + 0 -1) downto 11 vermieden werden.
--------------------------------------------------------------------------------
	SET_DATA_OUT : process (ADDRA(11 + SELECT_LINES -1 downto 10),ADDRB(11 + SELECT_LINES - 1 downto 10), LOCAL_DOA, LOCAL_DOB) is
	begin
		if RAM_LINES = 1 then
			DOA <= LOCAL_DOA(0);
			DOB <= LOCAL_DOB(0);
		elsif RAM_LINES = 2 then
			if ADDRA(11) = '1' then
				DOA <= LOCAL_DOA(1);
			else
				DOA <= LOCAL_DOA(0);
			end if;		
			if ADDRB(11) = '1' then
				DOB <= LOCAL_DOB(1);
			else
				DOB <= LOCAL_DOB(0);
			end if;		
		else
			case ADDRA(12 downto 11) is
				when "11" =>
					DOA <= LOCAL_DOA(3);
				when "10" =>
					DOA <= LOCAL_DOA(2);
				when "01" =>
					DOA <= LOCAL_DOA(1);
				when others =>
					DOA <= LOCAL_DOA(0);
			end case;
			case ADDRB(12 downto 11) is
				when "11" =>
					DOB <= LOCAL_DOB(3);
				when "10" =>
					DOB <= LOCAL_DOB(2);
				when "01" =>
					DOB <= LOCAL_DOB(1);
				when others =>
					DOB <= LOCAL_DOB(0);
			end case;
		end if;
	end process SET_DATA_OUT;

	end generate GEN_NON_HWPR_VERSION;

end BEHAVE;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Statische Realisierung des Speichers durch direkte Instanziierung
--	Es handelt sich hier um die urspruengliche, auf 8 Blockrams
--	festgelegte Loesung ohne generate-Anweisung. Die Loesung ist
--	wahrscheinlich leichter zu verstehen, aber auch sehr unflexibel
--	und umfangreich
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--164 Zeilen ein/auskommentieren
--architecture BEHAVE of ArmRAMB16_4kx32_Wrapper is
--
--	COMPONENT ArmRAMB16_2kx8_Wrapper
--	PORT(
--		RAM_CLK : IN std_logic;
--		RAM_RST : IN std_logic;
--		WEA : IN std_logic;
--		ENA : IN std_logic;
--		ADDRA : IN std_logic_vector(10 downto 0);
--		DIA : IN std_logic_vector(7 downto 0);
--		WEB : IN std_logic;
--		ENB : IN std_logic;
--		ADDRB : IN std_logic_vector(10 downto 0);
--		DIB : IN std_logic_vector(7 downto 0);          
--		DOA : OUT std_logic_vector(7 downto 0);
--		DOB : OUT std_logic_vector(7 downto 0)
--		);
--	END COMPONENT;
--	SIGNAL DOA_LOWER : std_logic_vector(31 downto 0);
--	SIGNAL DOB_LOWER : std_logic_vector(31 downto 0);
--	SIGNAL DOA_UPPER : std_logic_vector(31 downto 0);
--	SIGNAL DOB_UPPER : std_logic_vector(31 downto 0);
--
----	Notwendig, weil die Funktionen zum Bilden dieser Signale
----	nicht direkt in die Instanzzuweisungen geschrieben werden 
----	koennen.
--	SIGNAL WEA_LOWER : STD_LOGIC_VECTOR(3 downto 0);
--	SIGNAL WEB_LOWER : STD_LOGIC_VECTOR(3 downto 0);
--	SIGNAL WEA_UPPER : STD_LOGIC_VECTOR(3 downto 0);
--	SIGNAL WEB_UPPER : STD_LOGIC_VECTOR(3 downto 0);
--
--begin
--
--	DOA <= DOA_UPPER when ADDRA(11)='1' else
--	       DOA_LOWER;
--	DOB <= DOB_UPPER when ADDRB(11)='1' else
--	       DOB_LOWER;
--
--	DETERMINE_WRITE_ENABLES : PROCESS(WEA,WEB,ADDRA,ADDRB) 
--	begin
--		for i in 0 to 3 loop
--			WEA_LOWER(i) <= WEA(i) AND NOT ADDRA(11);
--			WEB_LOWER(i) <= WEB(i) AND NOT ADDRB(11);
--			WEA_UPPER(i) <= WEA(i) AND ADDRA(11);
--			WEB_UPPER(i) <= WEB(i) AND ADDRB(11);
--		end loop;	
--	end PROCESS DETERMINE_WRITE_ENABLES;	
--
----	Die unteren 2048 Worte
--	BYTE0_LOWER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_LOWER(0),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(7 downto 0),
--		WEB => WEB_LOWER(0),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(7 downto 0),
--		DOA => DOA_LOWER(7 downto 0),
--		DOB => DOB_LOWER(7 downto 0)
--	);
--	BYTE1_LOWER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_LOWER(1), 
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(15 downto 8),
--		WEB => WEB_LOWER(1),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(15 downto 8),
--		DOA => DOA_LOWER(15 downto 8),
--		DOB => DOB_LOWER(15 downto 8)
--	);
--	BYTE2_LOWER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_LOWER(2),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(23 downto 16),
--		WEB => WEB_LOWER(2),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(23 downto 16),
--		DOA => DOA_LOWER(23 downto 16),
--		DOB => DOB_LOWER(23 downto 16)
--	);
--	BYTE3_LOWER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_LOWER(3),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(31 downto 24),
--		WEB => WEB_LOWER(3),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(31 downto 24),
--		DOA => DOA_LOWER(31 downto 24),
--		DOB => DOB_LOWER(31 downto 24)
--	);
--
----	Die oberen 2048 Worte
--	BYTE0_UPPER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_UPPER(0),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(7 downto 0),
--		WEB => WEB_UPPER(0),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(7 downto 0),
--		DOA => DOA_UPPER(7 downto 0),
--		DOB => DOB_UPPER(7 downto 0)
--	);
--	BYTE1_UPPER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_UPPER(1),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(15 downto 8),
--		WEB => WEB_UPPER(1), 
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(15 downto 8),
--		DOA => DOA_UPPER(15 downto 8),
--		DOB => DOB_UPPER(15 downto 8)
--	);
--	BYTE2_UPPER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_UPPER(2),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(23 downto 16),
--		WEB => WEB_UPPER(2),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(23 downto 16),
--		DOA => DOA_UPPER(23 downto 16),
--		DOB => DOB_UPPER(23 downto 16)
--	);
--	BYTE3_UPPER: ArmRAMB16_2kx8_Wrapper PORT MAP(
--		RAM_CLK => RAM_CLK,
--		RAM_RST => RAM_RST,
--		WEA => WEA_UPPER(3),
--		ENA => ENA,
--		ADDRA => ADDRA(10 downto 0),
--		DIA => DIA(31 downto 24),
--		WEB => WEB_UPPER(3),
--		ENB => ENB,
--		ADDRB => ADDRB(10 downto 0),
--		DIB => DIB(31 downto 24),
--		DOA => DOA_UPPER(31 downto 24),
--		DOB => DOB_UPPER(31 downto 24)
--	);
--end BEHAVE;
