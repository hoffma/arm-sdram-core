library IEEE; use IEEE.STD_LOGIC_1164.ALL;
library UNISIM; use UNISIM.VCOMPONENTS.ALL; 
entity BRAM_wrapper is
  port (
    BRAM_PORTA_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTA_clk : in STD_LOGIC;
    BRAM_PORTA_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_en : in STD_LOGIC;
    BRAM_PORTA_we : in STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTB_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTB_clk : in STD_LOGIC;
    BRAM_PORTB_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_en : in STD_LOGIC;
    BRAM_PORTB_we : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end BRAM_wrapper;

architecture STRUCTURE of BRAM_wrapper is
  component BRAM is
  port (
    BRAM_PORTA_en : in STD_LOGIC;
    BRAM_PORTA_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_we : in STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTA_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTA_clk : in STD_LOGIC;
    BRAM_PORTB_en : in STD_LOGIC;
    BRAM_PORTB_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_we : in STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTB_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTB_clk : in STD_LOGIC
  );
  end component BRAM;
begin
BRAM_i: component BRAM
    port map (
      BRAM_PORTA_addr(10 downto 0) => BRAM_PORTA_addr(10 downto 0),
      BRAM_PORTA_clk => BRAM_PORTA_clk,
      BRAM_PORTA_din(7 downto 0) => BRAM_PORTA_din(7 downto 0),
      BRAM_PORTA_dout(7 downto 0) => BRAM_PORTA_dout(7 downto 0),
      BRAM_PORTA_en => BRAM_PORTA_en,
      BRAM_PORTA_we(0) => BRAM_PORTA_we(0),
      BRAM_PORTB_addr(10 downto 0) => BRAM_PORTB_addr(10 downto 0),
      BRAM_PORTB_clk => BRAM_PORTB_clk,
      BRAM_PORTB_din(7 downto 0) => BRAM_PORTB_din(7 downto 0),
      BRAM_PORTB_dout(7 downto 0) => BRAM_PORTB_dout(7 downto 0),
      BRAM_PORTB_en => BRAM_PORTB_en,
      BRAM_PORTB_we(0) => BRAM_PORTB_we(0)
    );
end STRUCTURE;
