--------------------------------------------------------------------------------
--	Shifter des ARM-Datenpfades, enthaelt einen Barrelshifter 
--	fuer Operationen zwischen 0 (1 in der Neufassung der hochsprachlichen
--	Beschreibung) und 31 Bit und erzeugt die Ergebnisse fuer weitere
--	Operationen selbst (fuer Rotationen um mehr als 31 Stellen wird
--	ebenfalls der Barrelshifter verwendet)
--------------------------------------------------------------------------------
--	Datum:		10.05.2010
--	Version:	1.3
--------------------------------------------------------------------------------
--	Aenderungen:
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ArmTypes.all;
use work.ArmConfiguration.USE_SHIFTER_V2;
use work.ArmConfiguration.BARRELSHIFTER_FULL_COMPATIPLE;
use work.ArmConfiguration.SHIFTER_TYPE;

entity ArmShifter is
	port (
		SHIFT_OPERAND	: in	STD_LOGIC_VECTOR(31 downto 0);
		SHIFT_AMOUNT	: in	STD_LOGIC_VECTOR(7 downto 0);
		SHIFT_TYPE_IN	: in	STD_LOGIC_VECTOR(1 downto 0);
		SHIFT_C_IN	: in	STD_LOGIC;
		SHIFT_RXX	: in	STD_LOGIC;
		SHIFT_RESULT	: out	STD_LOGIC_VECTOR(31 downto 0);
		SHIFT_C_OUT	: out	STD_LOGIC    		
 	);
end ArmShifter;

architecture BEHAVE of ArmShifter is
--------------------------------------------------------------------------------
--	Im Folgenden sind zwei Varianten dieser Schaltung beschrieben, 
--	V2 ist neuer und in der Theorie etwas besser, V1 (unten) 
--	als Musterloesung fuer das HWPR verstaendlicher. Ausserdem
--	kann der Barrelshifter in zwei verschiedenen Varianten 
--	eingebunden werden, die Loesung aus dem HWPR ist kompakter, 
--	dafuer aber auch deutlich langsamer.
--------------------------------------------------------------------------------
--	Version 1 sollte immer gemeinsam mit BARRELSHIFTER_FULL_COMPATIPLE=true
--	in ArmConfiguration verwendet werden.
--------------------------------------------------------------------------------
--	Ergaenzung:
--	Nach der automatischen Optimierung waehrend der Synthese ist Version 1
--	nur minimal langsamer aber deutlich kompakter und sollte daher 
--	vorgezogen werden. Die damit einhergehende Kompatibilitaet mit
--	der bisherigen Fassung ist offenbar nicht nachteilig
--------------------------------------------------------------------------------
-- 	constant SHIFTER_USE_VERSION_2	: boolean := false;
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--	Instanz des strukturellen Barrelshifters sowie notwendige
--	Steuersignale, alle Shifts erfolgen auf einers gemeinsamen
--	Multiplexerstruktur.
--------------------------------------------------------------------------------
	component ArmBarrelShifter
	generic(
		OPERAND_WIDTH : integer;
		SHIFTER_DEPTH : integer
	);	
	port(
		    OPERAND	: in std_logic_vector(31 downto 0);
		    MUX_CTRL	: in std_logic_vector(1 downto 0);
		    AMOUNT	: in std_logic_vector(4 downto 0);
		    ARITH_SHIFT : in std_logic;
		    C_IN	: in std_logic;          
		    DATA_OUT	: out std_logic_vector(31 downto 0);
		    C_OUT	: out std_logic
	    );
	end component;

--------------------------------------------------------------------------------
--	Hochsprachlicher Barrelshifter, verwendet einzelne, sehr
--	schnelle Shifter-Makros.
--------------------------------------------------------------------------------
	component ArmBarrelShifter_HILEVEL
	generic(
		OPERAND_WIDTH : integer;
		SHIFTER_DEPTH : integer
	);
	port(
		OPERAND 	: in std_logic_vector(31 downto 0);
		MUX_CTRL 	: in std_logic_vector(1 downto 0);
		AMOUNT 		: in std_logic_vector(4 downto 0);
		ARITH_SHIFT 	: in std_logic;
		C_IN 		: in std_logic;          
		DATA_OUT 	: out std_logic_vector(31 downto 0);
		C_OUT 		: out std_logic
		);
	end component;

	signal INT_AM		: integer range 0 to 255 := 0;
	signal RES 		: std_logic_vector(31 downto 0);
	signal MUX_CTRL		: std_logic_vector(1 downto 0)	:= (others => '0');
	signal ARITH_SHIFT	: std_logic			:= '0';
	signal BARREL_AMOUNT	: std_logic_vector(4 downto 0)	:= (others => '0');
	signal BARREL_RESULT	: std_logic_vector(31 downto 0);
	signal BARREL_C_OUT	: std_logic;
begin
--------------------------------------------------------------------------------
--	Auswahl des verwendeten Barrelshifters, fuer das 
--	HWPR sollte das "SMALL" sein.
--------------------------------------------------------------------------------
	GEN_SMALL_BARRELSHIFTER : if SHIFTER_TYPE = ARM_SHIFTER_TYPE(SMALL) generate
	Inst_ArmBarrelShifter_SMALL: ArmBarrelShifter 
	generic map(
			   OPERAND_WIDTH => 32,
			   SHIFTER_DEPTH => 5
		   )
	port map(
			OPERAND 	=> SHIFT_OPERAND,
			MUX_CTRL	=> MUX_CTRL,
			AMOUNT		=> BARREL_AMOUNT,
			ARITH_SHIFT	=> ARITH_SHIFT,
			C_IN		=> SHIFT_C_IN,
			DATA_OUT	=> BARREL_RESULT,
			C_OUT		=> BARREL_C_OUT
		);
	end generate GEN_SMALL_BARRELSHIFTER;

	GEN_FAST_BARRELSHIFTER : if SHIFTER_TYPE = FAST generate
	Inst_ArmBarrelShifter_FAST: ArmBarrelShifter_HILEVEL 
	generic map(
			   OPERAND_WIDTH => 32,
			   SHIFTER_DEPTH => 5
		   )
	port map(
			OPERAND 	=> SHIFT_OPERAND,
			MUX_CTRL	=> MUX_CTRL,
			AMOUNT		=> BARREL_AMOUNT,
			ARITH_SHIFT	=> ARITH_SHIFT,
			C_IN		=> SHIFT_C_IN,
			DATA_OUT	=> BARREL_RESULT,
			C_OUT		=> BARREL_C_OUT
		);
	end generate GEN_FAST_BARRELSHIFTER;

--	Shiftweite als Integer zur Verfuegung stellen,
--	macht die Abfrage leichter lesbar.
	INT_AM <=  to_integer(unsigned(SHIFT_AMOUNT));
--	Der Barrelshifter sieht die 5 niederwertigen
--	Bits der Schiebeweite.
	BARREL_AMOUNT <= SHIFT_AMOUNT(4 downto 0);

--	Prozess, in dem die Steuersignale des Barrelshifters erzeugt werden
	GEN_MUX_CTRL : PROCESS(SHIFT_TYPE_IN,SHIFT_OPERAND(31))
	begin
--------------------------------------------------------------------------------
--	Die Steuerleitungen des Barrelshifters muessen folgendermassen codiert
--	werden:
--		LSL : 01
--		(L/A)SR : 10
--		ROR : 11
--		00 kommt hier nicht vor, da die ARM-Shiftbefehle das nicht
--		vorsehen! Der Unterschied ASR<>LSR wird durch die Steuerleitung
--		"ARITH_SHIFT" beruecksichtigt.
--------------------------------------------------------------------------------
		case SHIFT_TYPE_IN is
			when SH_LSL =>
				MUX_CTRL <= "01";
			when SH_LSR =>
				MUX_CTRL <= "10";
			when SH_ASR =>
				MUX_CTRL <= "10";
			when others => --SH_ROR =>	
				MUX_CTRL <= "11";
--			when others =>
--				MUX_CTRL <= "00";
--				ARITH_SHIFT <= '0';
		end case;
		if BARRELSHIFTER_FULL_COMPATIPLE then
			if SHIFT_TYPE_IN = SH_ASR then
				ARITH_SHIFT <= '1';
			else
				ARITH_SHIFT <= '0';
			end if;
		else
--------------------------------------------------------------------------------
--	Falls keine volle Kompatibilitaet zum strukturell beschriebenen
--	Barrelshifter hergestellt werden muss, kann bereits an dieser Stelle
--	das korrekte Vorzeichenbit fuer den logischen/arithmetischen 
--	Rechtsshift im HILEVEL-Barrelshifter in einem LUT erzeugt werden,
--	ARITH_SHIFT gibt damit nicht mehr nur an, dass ein ASR stattfindet,
--	sondern bildet bereits das korrekte nachzuziehende VZ.
--	(irrelevant fuer das HWPR!).		
--------------------------------------------------------------------------------
			if (SHIFT_TYPE_IN = SH_ASR) and (SHIFT_OPERAND(31) = '1') then
				ARITH_SHIFT <= '1';
			else
				ARITH_SHIFT <= '0';
			end if;
		end if;
	end process GEN_MUX_CTRL;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Urspruengliche Version der Schaltung, das Verhalten ist identisch
--	mit Version 2, aber der Entscheidungsprozess besser nachvollziehbar
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
GEN_VERSION_1 : if not USE_SHIFTER_V2 generate
--------------------------------------------------------------------------------
--	Ergebnisermittlung nach folgendem Schema:
--	Bei Operationen der Breite 0..31 (ausser RXX) wird das Ergebnis des
--	Barrelshifters uebernommen, bei groesseren Breiten ein abweichendes
--	Ergebnis produziert.	
--------------------------------------------------------------------------------
	process(SHIFT_C_IN,INT_AM,SHIFT_RXX,SHIFT_OPERAND,SHIFT_TYPE_IN,BARREL_RESULT,BARREL_C_OUT,SHIFT_AMOUNT) is
	begin 
		if SHIFT_RXX='1' then
			SHIFT_C_OUT <= SHIFT_OPERAND(0);
			RES <= SHIFT_C_IN & SHIFT_OPERAND(31 downto 1);
		else
			case SHIFT_TYPE_IN is
				when SH_LSL =>
					if (INT_AM < 32 ) then
						RES <= BARREL_RESULT;
						SHIFT_C_OUT <= BARREL_C_OUT;
					elsif (INT_AM = 32) then
						RES <= (others => '0');
						SHIFT_C_OUT <= SHIFT_OPERAND(0);
					else
						RES <= (others => '0');
						SHIFT_C_OUT <= '0';
					end if;	
				when SH_LSR =>
					if(INT_AM < 32 ) then
						RES <= BARREL_RESULT;
						SHIFT_C_OUT <= BARREL_C_OUT;
					elsif (INT_AM = 32) then
						RES <= (others => '0');
						SHIFT_C_OUT <= SHIFT_OPERAND(31);
					else
						RES <= (others => '0');
						SHIFT_C_OUT <= '0';
					end if;	
				when SH_ASR =>
					if(INT_AM < 32 )then
						RES <= BARREL_RESULT;
						SHIFT_C_OUT <= BARREL_C_OUT;
					else 
						RES <= (others => SHIFT_OPERAND(31));
						SHIFT_C_OUT <= SHIFT_OPERAND(31);				
					end if;					
				when others => --SH_ROR	
					if((SHIFT_AMOUNT(7 downto 0) /= X"00") AND (SHIFT_AMOUNT(4 downto 0)= "00000")) then
--------------------------------------------------------------------------------
--	Es wurde das gesamte Register um ein Vielfaches von 32 rotiert, das
--	Ergebnis entspricht dem Operanden, das C-Flag muss gesetzt werden
--------------------------------------------------------------------------------
						RES <= SHIFT_OPERAND;
						SHIFT_C_OUT <= SHIFT_OPERAND(31);
					else
--------------------------------------------------------------------------------
--	Die Rotation hat nicht um ein Vielfaches von 32, evtl. aber um mehr als
--	32 Stellen stattgefunden. In beiden Faellen kann das Ergebnis des
--	Barrelshifters verwendet werden.					
--------------------------------------------------------------------------------
						RES <= BARREL_RESULT;
						SHIFT_C_OUT <= BARREL_C_OUT;
					end if;
			end case;
		end if;
	end process;

end generate GEN_VERSION_1;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--	Ueberarbeitete, aber schwer verstaendliche Fassung
--	der Schifterschaltung
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
GEN_VERSION_2 : if USE_SHIFTER_V2 generate
	signal LOCAL_RESULT	: std_logic_vector(31 downto 0) := (others => '0');
	signal LOCAL_C		: std_logic 	:= '0';
	signal USE_LOCAL_RESULT : boolean	:= false;
	signal AM_EQ_32		: boolean	:= false;
	signal AM_SL_32		: boolean	:= false;
begin
	AM_EQ_32 <= INT_AM = 32;
	AM_SL_32 <= INT_AM < 32;

--------------------------------------------------------------------------------
--	Feststellen, ob einer der Faelle vorliegt, in denen nicht
--	das Ergebnis des Barrelshifters uebernommen werden darf.	
--------------------------------------------------------------------------------
	USE_LOCAL_RESULT <= AM_EQ_32 or 
			    (SHIFT_RXX = '1') or 
			    (INT_AM = 0 and not BARRELSHIFTER_FULL_COMPATIPLE) or 
			    ((SHIFT_TYPE_IN /= SH_ROR) and not AM_SL_32) or 
			    ((SHIFT_TYPE_IN = SH_ROR) and (((SHIFT_AMOUNT(7 downto 0) /= X"00") AND (SHIFT_AMOUNT(4 downto 0)= "00000"))));

	process(SHIFT_C_IN,INT_AM,SHIFT_RXX,SHIFT_OPERAND,SHIFT_TYPE_IN,BARREL_RESULT,BARREL_C_OUT,SHIFT_AMOUNT,AM_EQ_32, AM_SL_32) is
	begin 
--------------------------------------------------------------------------------
--	In der ueberarbeiteten Version wird das Carry fuer Schiebeweite 0 nicht
--	korrekt durch den Barrelshifter geleitet
--------------------------------------------------------------------------------
		if INT_AM = 0 and not BARRELSHIFTER_FULL_COMPATIPLE then
			LOCAL_C <= SHIFT_C_IN;
		elsif (SHIFT_RXX = '1') or (SHIFT_TYPE_IN = SH_LSL and AM_EQ_32)then 
			LOCAL_C <= SHIFT_OPERAND(0);	
		elsif(((SHIFT_TYPE_IN = SH_LSL) or (SHIFT_TYPE_IN = SH_LSR)) and (not AM_EQ_32))then
			LOCAL_C <= '0'; 
		else
			LOCAL_C <= SHIFT_OPERAND(31);	
		end if;	

		if SHIFT_RXX='1' then
			LOCAL_RESULT <= SHIFT_C_IN & SHIFT_OPERAND(31 downto 1);
		elsif INT_AM = 0 and not BARRELSHIFTER_FULL_COMPATIPLE then
			LOCAL_RESULT <= SHIFT_OPERAND;
		else
			case SHIFT_TYPE_IN is
				when SH_LSL =>
					LOCAL_RESULT <= (others => '0');
				when SH_LSR =>
					LOCAL_RESULT <= (others => '0');
				when SH_ASR =>
					LOCAL_RESULT <= (others => SHIFT_OPERAND(31));
				when others =>	
--				when SH_ROR =>
					LOCAL_RESULT <= SHIFT_OPERAND;
			end case;
		end if;

	end process;
	
--------------------------------------------------------------------------------
--	Auswahl zwischen Barrelshifter und lokal erzeugten Ergebnissen.
--	Hinter dem Barrelshifter folgt auf diese Weise nur eine LUT-Stufe,
--	die alternativen Ergebnisse werden parallel erzeugt
--------------------------------------------------------------------------------
	SELECT_RESULT : process(USE_LOCAL_RESULT,LOCAL_RESULT,BARREL_RESULT,LOCAL_C,BARREL_C_OUT)is
	begin
		if USE_LOCAL_RESULT then
			RES 		<= LOCAL_RESULT;
			SHIFT_C_OUT	<= LOCAL_C;
		else
			RES		<= BARREL_RESULT;
			SHIFT_C_OUT	<= BARREL_C_OUT;
		end if;

	end process SELECT_RESULT;
end generate GEN_VERSION_2;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

	SHIFT_RESULT <= RES;
end BEHAVE;

