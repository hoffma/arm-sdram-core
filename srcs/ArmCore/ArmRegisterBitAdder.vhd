--------------------------------------------------------------------------------
--	Schaltung fuer das Zaehlen von Einsen in einem 16-Bit-Vektor, realisiert
-- 	als Baum von Addierern.
-- 	Insgesamt enthaelt die Datei drei Varianten der Architecture
-- 	mit unterschiedlichen Baeumen. In der letztlich verwendeten Fassung
-- 	bilden explizit durch Funktionen fuer Summe und Uebertrag 
-- 	beschriebene 1-Bit-Volladdierer die erste Stufe. Evtl. kann die 
-- 	Schaltung durch eine staerkere Anpassung auf die FPGA-Struktur
-- 	weiter beschleunig werden.	
--------------------------------------------------------------------------------
--	Datum:		12.06.2010
--	Version:	1.01
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ArmRegisterBitAdder is
	Port (
		RBA_REGLIST 	: in  std_logic_vector(15 downto 0);
		RBA_NR_OF_REGS 	: out  std_logic_vector(4 downto 0)
	);
end ArmRegisterBitAdder;
--	Addiererbaum mit Volladdierern in der ersten Stufe
architecture STRUCTURE of ArmRegisterBitAdder is
	signal V4,V3,V2,V1,V0	: std_logic_vector(1 downto 0) := "00";
	signal VA,VB,VC		: std_logic_vector(2 downto 0) := "000";
	signal VD		: std_logic_vector(3 downto 0) := "0000";
	signal VE		: std_logic_vector(4 downto 0) := "00000";
	signal TEMP		: std_logic_vector(2 downto 0) := "000";
        alias R			: std_logic_vector(15 downto 0) is RBA_REGLIST;
	alias NR_OF_REGS	: std_logic_vector(4 downto 0) is RBA_NR_OF_REGS;
begin
--------------------------------------------------------------------------------
-- 	Erste Stufe
--------------------------------------------------------------------------------
	V4 <= ((R(15) and R(14)) or (R(13) and (R(15) xor R(14)))) & (R(15) xor R(14) xor R(13)); 
	V3 <= ((R(12) and R(11)) or (R(10) and (R(12) xor R(11)))) & (R(12) xor R(11) xor R(10)); 
	V2 <= ((R(9) and R(8)) or (R(7) and (R(9) xor R(8)))) & (R(9) xor R(8) xor R(7)); 
	V1 <= ((R(6) and R(5)) or (R(4) and (R(6) xor R(5)))) & (R(6) xor R(5) xor R(4)); 
	V0 <= ((R(3) and R(2)) or (R(1) and (R(3) xor R(2)))) & (R(3) xor R(2) xor R(1)); 
--------------------------------------------------------------------------------
-- 	Zweite Stufe
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- 	Das einzelne in der ersten Stufe nicht verarbeitete Bit des 
-- 	Vektors erweitern und dann mit verrechnen.
--------------------------------------------------------------------------------
	TEMP <= "00" & R(0);
	VA <= std_logic_vector(unsigned("0" & V4) + unsigned("0" & V3));
	VB <= std_logic_vector(unsigned("0" & V2) + unsigned("0" & V1));
	VC <= std_logic_vector(unsigned("0" & V0) + unsigned(TEMP));
--------------------------------------------------------------------------------
-- 	Vierte Stufe, nur ein Addierer
--------------------------------------------------------------------------------
	VD <= std_logic_vector(unsigned("0" & VA) + unsigned("0" & VB));
--------------------------------------------------------------------------------
-- 	Abschliessende Additon
--------------------------------------------------------------------------------
	VE <= std_logic_vector(unsigned("0" & VD) + unsigned("00" & VC));

	NR_OF_REGS <= VE;
end STRUCTURE;

--------------------------------------------------------------------------------
--	Alternative Implementierung mit Halbaddierern in der ersten Stufe
--------------------------------------------------------------------------------
--architecture STRUCTURE2 of ArmRegisterBitAdder is
--       signal V3, V2, V1, V0	: std_logic_vector(1 downto 0) := "00";	
--       signal V9, V8, V7, V6	: std_logic_vector(1 downto 0) := "00";	
--       signal VA, VB		: std_logic_vector(2 downto 0) := "000";
--       signal VX, VY		: std_logic_vector(2 downto 0) := "000";
--       signal VC		: std_logic_vector(3 downto 0) := "0000";
--       signal VD		: std_logic_vector(3 downto 0) := "0000";
--       signal VE		: std_logic_vector(4 downto 0) := "00000";
--       alias R		: std_logic_vector(15 downto 0) is REGLIST;
--begin
--		V3 <= (R(7) and R(6)) & (R(7) xor R(6));
--		V2 <= (R(5) and R(4)) & (R(5) xor R(4));
--		V1 <= (R(3) and R(2)) & (R(3) xor R(2));
--		V0 <= (R(1) and R(0)) & (R(1) xor R(0));
--
--		V9 <= (R(15) and R(14)) & (R(15) xor R(14));
--		V8 <= (R(13) and R(12)) & (R(13) xor R(12));
--		V7 <= (R(11) and R(10)) & (R(11) xor R(10));
--		V6 <= (R(9) and R(8)) & (R(9) xor R(8));
--
--		VA <= std_logic_vector(unsigned("0" & V3) + unsigned("0" & V2));
--		VB <= std_logic_vector(unsigned("0" & V1) + unsigned("0" & V0));
--		VX <= std_logic_vector(unsigned("0" & V9) + unsigned("0" & V8));
--		VY <= std_logic_vector(unsigned("0" & V7) + unsigned("0" & V6));
--
--		VC <= std_logic_vector(unsigned("0" & VA) + unsigned("0" & VB));
--		VD <= std_logic_vector(unsigned("0" & VX) + unsigned("0" & VY));
--		VE <= std_logic_vector(unsigned("0" & VC) + unsigned("0" & VD));
--		NR_OF_REGS <= VE;
--end Behavioral;

--------------------------------------------------------------------------------
--	Alternative Implementierung mit einem 4-Bit-Rom in der ersten Stufe,
-- 	die Laufzeiten sollten aehnlich ausfallen. Das ROM ist in einer
--	zusaetzlichen Designeinheit beschrieben.
--------------------------------------------------------------------------------
--architecture STRUCTURE3 of ArmRegisterBitAdder is
--	signal V3, V2, V1, V0	: std_logic_vector(2 downto 0);
--	signal VA, VB		: std_logic_vector(3 downto 0);
--	signal VC		: std_logic_vector(4 downto 0);
--begin
--	NIBBLE3 : entity work.Arm4BitCount(STRUCTURE)
--	port map(
--		BC_4BIT_VECTOR => REGLIST(15 downto 12),
--		BC_3BIT_COUNT => V3
--	);
--
--	NIBBLE2 : entity work.Arm4BitCount(STRUCTURE)
--	port map(
--		BC_4BIT_VECTOR => REGLIST(11 downto 8),
--		BC_3BIT_COUNT => V2
--	);
--	
--	NIBBLE1 : entity work.Arm4BitCount(STRUCTURE)
--	port map(
--		BC_4BIT_VECTOR => REGLIST(7 downto 4),
--		BC_3BIT_COUNT => V1
--	);
--
--	NIBBLE0 : entity work.Arm4BitCount(STRUCTURE)
--	port map(
--		BC_4BIT_VECTOR => REGLIST(3 downto 0),
--		BC_3BIT_COUNT => V0
--	);
--
--	VA <= std_logic_vector(unsigned("0" & V3) + unsigned("0" & V2));
--	VB <= std_logic_vector(unsigned("0" & V1) + unsigned("0" & V0));
--	VC <= std_logic_vector(unsigned("0" & VA) + unsigned("0" & VB));
--
--	NR_OF_REGS <= VC;
--end STRUCTURE3;
