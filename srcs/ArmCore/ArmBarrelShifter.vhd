--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- 	Allgemeiner n-Bit-Shifter fuer LSL,LSR,ASR,ROR mit Shiftweiten von 0 bis
--	n-1 Bit. Das Modul war urspruenglich nur als 4-Bit-Shifter gedacht, ist
--	jetzt aber generisch.
--------------------------------------------------------------------------------
--	Datum:		06.05.2010
--	Version:	1.1
--------------------------------------------------------------------------------
--	Aenderungen:	Shift-Rotate wird durch den others-Zweig abgedeckt 
--			um die zahlreiche Synthesewarnungen zu vermeiden,
--			dass der (sinnlose) Zweig herausoptimiert wird
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ArmBarrelShifter is
--------------------------------------------------------------------------------
--	Breite der Operanden (n) und die Zahl der notwendigen
--	Multiplexerstufen (m) um Shifts von 0 bis n-1 Stellen realisieren zu
--	koennen. Es muss gelten: n = 2^m
--------------------------------------------------------------------------------
	generic (	SHIFTER_DEPTH : integer := 5;
			OPERAND_WIDTH : integer := 32	 -- :=2**SHIFTER_DEPTH
	 );

	Port ( 	OPERAND 	: in std_logic_vector(OPERAND_WIDTH-1 downto 0);	--bei generischer Implementierung: (OPERAND_WIDTH-1 downto 0);
    		MUX_CTRL 	: in std_logic_vector(1 downto 0);
    		AMOUNT 		: in std_logic_vector(SHIFTER_DEPTH-1 downto 0);	--bei generischer Implementierung: (SHIFTER_DEPTH-1 downto 0); 
    		ARITH_SHIFT 	: in std_logic; 
    		C_IN 		: in std_logic;
           	DATA_OUT 	: out std_logic_vector(OPERAND_WIDTH-1 downto 0);	--bei generischer Implementierung: (OPERAND_WIDTH-1 downto 0);
    		C_OUT 		: out std_logic
	);
end ArmBarrelShifter;

architecture STRUCTURE of ArmBarrelShifter is
--	Signalleitung fuer das bei Rechtsshifts nachzuziehende Bit
	signal SHIFT_RIGHT_BIT	: std_logic;
--	Codierung der Operationen an MUX_CTRL
	constant SHIFT_NONE 	: std_logic_vector(1 downto 0) := "00";
	constant SHIFT_LEFT 	: std_logic_vector(1 downto 0) := "01";
	constant SHIFT_RIGHT 	: std_logic_vector(1 downto 0) := "10";
	constant SHIFT_ROTATE 	: std_logic_vector(1 downto 0) := "11";
--------------------------------------------------------------------------------
--	Typen fuer die Beschreibung der Shifterstufen. Die Stufe mit dem
--	hoechsten Index entspricht den Eingangsoperanden, die niedrigeren
--	Indizes entsprechen jeweils einer Multiplexerstufe, Index 0 entspricht
--	den "untersten" Multiplexern, die gleichzeitig das Ergebniss bilden.
--------------------------------------------------------------------------------
	type RES_STAGE_VEC_TYPE is array (SHIFTER_DEPTH downto 0) of std_logic_vector((OPERAND_WIDTH-1) downto 0);
--------------------------------------------------------------------------------
--	Die Multiplexer zur Bildung des Carrybits werden separat beschrieben
--------------------------------------------------------------------------------
	type C_STAGE_VEC_TYPE is array (SHIFTER_DEPTH downto 0) of std_logic;
--------------------------------------------------------------------------------
-- 	Kontrollsignale aller Shifterstufen, werden ermittelt durch Verundung
--	des globalen Kontrollsignals (MUX_CTRL) mit je einem Bit der Shiftweite.
-- 	Hochster Index = ersten Stufe.
--------------------------------------------------------------------------------
	type CTRL_STAGE_VEC_TYPE is array (SHIFTER_DEPTH-1 downto 0) of std_logic_vector(1 downto 0);
	signal CTRL_STAGE : CTRL_STAGE_VEC_TYPE;
	
begin
--------------------------------------------------------------------------------
--	Bestimmung des bei Rechtsshifts nachzuziehenden Bits, abhaengig von
--	Operationstyp und MSB des Operanden
--------------------------------------------------------------------------------
	SHIFT_RIGHT_BIT <= '0' when ARITH_SHIFT = '0' else OPERAND(OPERAND'left); 

--	Initialisierung der Multiplexer-Steuerleigungen jeder Stufe
	init_ctrl : process(MUX_CTRL,AMOUNT)
	begin
		for i in (SHIFTER_DEPTH -1) downto 0 loop
			CTRL_STAGE(i) <= MUX_CTRL and (AMOUNT(i) & AMOUNT(i));
		end loop;
	end process init_ctrl;

--------------------------------------------------------------------------------
--	Strukturbeschreibung, getrennt fuer Ergebnis und Carry in einem
--	Prozess, die Grafik veranschaulicht die verwendeten Indizes am
--	Beispiel des 4-Bit-Shifters
--------------------------------------------------------------------------------
--j\i     3     2     1     0	  C_IN
---|--------------------------------------
--2|      |     |     |     |       |
-- |     ____  ____  ____  ____    ____
--1|     \__/  \__/  \__/  \__/    \__/
-- |     ____  ____  ____  ____    ____
--0|     \__/  \__/  \__/  \__/    \__/
-- |      |     |     |     |       |
---|-------------------------------------
--		DATA_OUT	  C_OUT    
--------------------------------------------------------------------------------
	
	process (CTRL_STAGE,SHIFT_RIGHT_BIT, OPERAND, C_IN)
		variable C_STAGE : C_STAGE_VEC_TYPE;
		variable RES_STAGE : RES_STAGE_VEC_TYPE;
	begin
--------------------------------------------------------------------------------
--	Hilfreich fuer die Fehlersuche bei der Verhaltenssimulation ist eine
--	Initialisierung mit U, muss fuer die Synthese auskommentiert werden.
--------------------------------------------------------------------------------
--	RES_STAGE := (others => (others => 'U'));

--------------------------------------------------------------------------------
--	Initialisierung der obersten Logikstufe mit OPERAND und C_IN, damit die
--	nachfolgende Beschreibung keine zusaetzliche Entscheidungsebene fuer die
--	erste Multiplexerstufe benoetigt
--------------------------------------------------------------------------------
		RES_STAGE(SHIFTER_DEPTH) := OPERAND;
		C_STAGE(SHIFTER_DEPTH) := C_IN;
--------------------------------------------------------------------------------
--	Beschreibung fuer jedes Bit der 2-dimensionalen Struktur.		
--	Der Shift jeder Multiplexerstufe erfolgt um 2**j Stellen, wobei j
--	zwischen Shifter_DEPTH-1 und 0 liegt. An den "Raendern" muss jeweils 0
--	oder das VZ-Bit nachgezogen werden.			
--------------------------------------------------------------------------------
		for j in SHIFTER_DEPTH-1 downto 0 loop
			for i in OPERAND_WIDTH-1 downto 0 loop
				case CTRL_STAGE(j) is
					when SHIFT_NONE =>
						RES_STAGE(j)(i) := RES_STAGE(j+1)(i);
					when SHIFT_LEFT =>
						if((i-(2**j))>=0)then
							RES_STAGE(j)(i) := RES_STAGE(j+1)(i-(2**j));
						else
							RES_STAGE(j)(i) := '0';
						end if;
					when SHIFT_RIGHT =>
						if(i+(2**j) <= OPERAND_WIDTH-1)then
							RES_STAGE(j)(i) := RES_STAGE(j+1)(i+(2**j));
						else
							RES_STAGE(j)(i) := SHIFT_RIGHT_BIT;
						end if;	
					when others =>	-- SHIFT_ROTATE
						RES_STAGE(j)(i) := RES_STAGE(j+1)((i+(2**j)) mod OPERAND_WIDTH);
				end case;	
			end loop;
		end loop;

--	Das Carry-Bit benoetigt in jeder Stufe nur einen Multiplexer
		for j in SHIFTER_DEPTH-1 downto 0 loop
			case CTRL_STAGE(j) is
				when SHIFT_NONE =>
					C_STAGE(j) := C_STAGE(j+1);
				when SHIFT_LEFT =>
					C_STAGE(j) := RES_STAGE(j+1)(OPERAND_WIDTH-(2**j));
				when SHIFT_RIGHT =>
					C_STAGE(j) := RES_STAGE(j+1)((2**j)-1);
				when others =>
					-- SHIFT_ROTATE
					C_STAGE(j) := RES_STAGE(j+1)((2**j)-1);
			end case;	
		end loop;

--	Ausgangssignale zuweisen		
		C_OUT <= C_STAGE(0);	
		DATA_OUT <= RES_STAGE(0);
	end process;
--	Ausgabetest der Testbench
--	DATA_OUT <= DATA_OUT_TEMP(31 downto 1) & '0';
end STRUCTURE;

