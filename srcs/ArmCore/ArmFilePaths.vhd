PACKAGE ArmFilePaths is 

--	Pfad zum Testvektorverzeichnis, betriebssystem- und und Projektabhängig;
	-- auf guardian fuer den Gesamttest direkt auf Output des GCC verweisen
	constant TESTVECTOR_FOLDER_PATH : STRING := "c:\alutestdata\";
END ArmFilePaths;

PACKAGE BODY ArmFilePaths is
	end ArmFilePaths;
