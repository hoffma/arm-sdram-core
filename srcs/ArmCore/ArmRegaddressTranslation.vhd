------------------------------------------------------------------------------
--	Paket fuer die Funktionen zur die Abbildung von ARM-Registeradressen
-- 	auf Adressen des physischen Registerspeichers (5-Bit-Adressen)
--	(HWPR-Fassung)
------------------------------------------------------------------------------
--	Datum:		03.05.2010
--	Version:	1.0
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
library work;
use work.ArmTypes.all;

package ArmRegaddressTranslation is
  
	function GET_INTERNAL_ADDRESS(EXT_ADDRESS: std_logic_vector(3 downto 0);THIS_MODE: std_logic_vector(4 downto 0);USER_BIT : std_logic) return std_logic_vector;
end ArmRegaddressTranslation;

package body ArmRegAddressTranslation is

function GET_INTERNAL_ADDRESS(EXT_ADDRESS: STD_LOGIC_VECTOR(3 downto 0);THIS_MODE: STD_LOGIC_VECTOR(4 downto 0);USER_BIT : std_logic) return STD_LOGIC_VECTOR is
		variable INTERNAL_ADDRESS : STD_LOGIC_VECTOR(4 downto 0);
		variable THIS_MODE_FILTERED : std_logic_vector(4 downto 0);
	begin
--------------------------------------------------------------------------------		
--	Bei gesetztem USER-Bit ist der aktuelle Modus zu ignorieren
--	und vom User-Modus auszugehen
--------------------------------------------------------------------------------		
		if USER_BIT = '1' then
			THIS_MODE_FILTERED := USER;
		else
			THIS_MODE_FILTERED := THIS_MODE;
		end if;
		case EXT_ADDRESS is
			when R0|R1|R2|R3|R4|R5|R6|R7 =>
				INTERNAL_ADDRESS := '0' & EXT_ADDRESS;
			when R8|R9|R10|R11|R12 =>
				if(THIS_MODE_FILTERED = FIQ)then
					INTERNAL_ADDRESS := '1' & EXT_ADDRESS;
				else	
					INTERNAL_ADDRESS := '0' & EXT_ADDRESS;
				end if;
			when R15 =>
				INTERNAL_ADDRESS := "01111";
			when R13 =>
				case THIS_MODE_FILTERED is
					when FIQ =>
						INTERNAL_ADDRESS := "10000";
					when IRQ =>
						INTERNAL_ADDRESS := "10010";
					when SUPERVISOR =>
						INTERNAL_ADDRESS := "10100";
					when ABORT =>
						INTERNAL_ADDRESS := "10110";
					when UNDEFINED =>	
						INTERNAL_ADDRESS := "11101";
					--	USER/SYSTEM
					when others =>
--------------------------------------------------------------------------------
--	Fehler fuer den Test der Test Bench
--						INTERNAL_ADDRESS := '1' & EXT_ADDRESS;
						INTERNAL_ADDRESS := '0' & EXT_ADDRESS;
--------------------------------------------------------------------------------
				end case;	
			when R14 =>
				case THIS_MODE_FILTERED is
--------------------------------------------------------------------------------
					when FIQ =>
						INTERNAL_ADDRESS := "10001";
--	Fehler zum Test der Testbench
--						when FIQ =>
--						INTERNAL_ADDRESS := "10011";
--------------------------------------------------------------------------------
					when IRQ =>
						INTERNAL_ADDRESS := "10011";
					when SUPERVISOR =>
						INTERNAL_ADDRESS := "10101";
					when ABORT =>
						INTERNAL_ADDRESS := "10111";
					when UNDEFINED =>	
						INTERNAL_ADDRESS := "11110";				
					when others => 	-- USER/SYSTEM
						INTERNAL_ADDRESS := '0' & EXT_ADDRESS;
				end case;
			when others =>		
				INTERNAL_ADDRESS := '0' & EXT_ADDRESS;
		end case;	
	return INTERNAL_ADDRESS;			
end GET_INTERNAL_ADDRESS;	
	 
end ArmRegAddressTranslation;
