--------------------------------------------------------------------------------
--	Test auf Erfuellen der Instruktionsbedingungen im Datenpfad des ARM-SoC
--------------------------------------------------------------------------------
--	Datum:		08.12.09
--	Version:	1.0
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
library WORK;
use WORK.ArmTypes.all;

--------------------------------------------------------------------------------
-- 	CONDITION_FIELD: Bedingungsfeld im Befehlswort
-- 	CONDITION_CODE:  Durch alle vorherigen Befehle erzeugter Conditioncode im Datenpfad
-- 	Anordnung der CC-Bits im Statusregister: NZCV
--------------------------------------------------------------------------------

entity ArmConditionCheck is
	port(
		CDC_CONDITION_FIELD	: in std_logic_vector(3 downto 0);
		CDC_CONDITION_CODE	: in std_logic_vector(3 downto 0);
		CDC_CONDITION_MET	: out STD_LOGIC
	    );
end ArmConditionCheck;


architecture BEHAVE of ArmConditionCheck is
begin

	CHECK_CONDITION : PROCESS(CDC_CONDITION_FIELD, CDC_CONDITION_CODE)is
		alias FIELD : STD_LOGIC_VECTOR(3 downto 0) is CDC_CONDITION_FIELD;
		alias N : STD_LOGIC is CDC_CONDITION_CODE(3);
		alias Z : STD_LOGIC is CDC_CONDITION_CODE(2);
		alias C : STD_LOGIC is CDC_CONDITION_CODE(1);
		alias V : STD_LOGIC is CDC_CONDITION_CODE(0);
		alias MET : STD_LOGIC is CDC_CONDITION_MET;

	begin
--		Defaultzuweisung
		CDC_CONDITION_MET <= '0';

--		assert FIELD /= NV report "ArmConditionCheck: 'NEVER' sollte nicht als Bedingung verwendet werden" severity warning;
		CASE FIELD is
			-- zwei jeweils untereinander stehende F�lle repr�sentieren gerade gegenteilige Bedingungen
			when EQ 	=> MET <= Z;
			when NE 	=> MET <= NOT Z;
			when CS 	=> MET <= C;				-- entspricht auch HS
			when CC 	=> MET <= NOT C; 			-- entspricht auch LO
			when MI 	=> MET <= N;
			when PL 	=> MET <= NOT N;
			when VS 	=> MET <= V;
			when VC 	=> MET <= NOT V;
			when HI 	=> MET <= C AND (NOT Z);
			when LS 	=> MET <= (NOT C) OR Z; 		-- NOT(C AND (NOT Z))
			when GE 	=> MET <= N XNOR V;			-- �quivalenz
			when LT 	=> MET <= N XOR V;
			when GT 	=> MET <= (NOT Z) AND (N XNOR V);
			when LE 	=> MET <= Z OR (N XOR V);
			when AL 	=> MET <= '1';
			-- others umfasst nur den NV-Fall
			when others 	=> MET <= '0';
		end CASE;
	end PROCESS CHECK_CONDITION;
end BEHAVE;

