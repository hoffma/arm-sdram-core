------------------------------------------------------------------------------
--	Ueberarbeitete Fassung der urspruenglichen Implementierung
-- 	des Registerspeichers
------------------------------------------------------------------------------
--	Datum:		04.05.2010
--	Version:	1.2
------------------------------------------------------------------------------
-- 	Aenderungen
-- 	Das globale Enable wurde entfernt, die gleiche Funktion kann mit 
-- 	wenig Zusatzaufwand direkt im Kontrollpfad oder Datenpfad durch
-- 	Manipulation der lokalen Enable-Signale erreicht werden.
-- 	Die Adressuebersetzung wurde entfernt, sie findet jetzt direkt
-- 	im Decode-Teil des Kontrollpfades statt. Dafuer sind alle Register-
-- 	adressen nun 5 Bit breit.
-- 	Das Reset wird nur noch verwendet, wenn die Konfigurationsoption
-- 	USE_OPTIONAL_RESET gesetzt ist.
--	In der vorliegenden, vereinfachten HWPR-Fassung ist
--	die Zahl der Ports nicht mehr generisch. OPTIONAL_RESET
--	wird innerhalb dieser Loesung verwendet, ist im HWPR aber
--	nicht verlangt.
------------------------------------------------------------------------------

library work;
use work.ArmTypes.all;
use work.ArmRegAddressTranslation.all;
use work.ArmGlobalProbes.all;
use work.ArmConfiguration.all;
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;


entity ArmRegfile is
--	GENERIC ( 
--		NR_READ_PORTS : integer range 1 to 3 := 3;
--		NR_WRITE_PORTS : integer range 1 to 2 := 2
--	);
	Port ( REF_CLK 		: in std_logic;
	       REF_RST 		: in  std_logic;

	       REF_W_PORT_A_ENABLE	: in std_logic;
	       REF_W_PORT_B_ENABLE	: in std_logic;
	       REF_W_PORT_PC_ENABLE	: in std_logic;

	       REF_W_PORT_A_ADDR 	: in std_logic_vector(4 downto 0);
	       REF_W_PORT_B_ADDR 	: in std_logic_vector(4 downto 0);

	       REF_R_PORT_A_ADDR 	: in std_logic_vector(4 downto 0);
	       REF_R_PORT_B_ADDR 	: in std_logic_vector(4 downto 0);
	       REF_R_PORT_C_ADDR 	: in std_logic_vector(4 downto 0);

	       REF_W_PORT_A_DATA 	: in std_logic_vector(31 downto 0);   
	       REF_W_PORT_B_DATA 	: in std_logic_vector(31 downto 0);   
	       REF_W_PORT_PC_DATA 	: in std_logic_vector(31 downto 0);   

	       REF_R_PORT_A_DATA 	: out std_logic_vector(31 downto 0);   
	       REF_R_PORT_B_DATA 	: out std_logic_vector(31 downto 0);   
	       REF_R_PORT_C_DATA 	: out std_logic_vector(31 downto 0)
       );	
end ArmRegfile;

architecture BEHAVE of ArmRegfile is

--------------------------------------------------------------------------------
--	Konstanten statt Generics fuer die Zahl
--	der Ports im HWPR.
--------------------------------------------------------------------------------
	constant NR_READ_PORTS : integer := 3;
	constant NR_WRITE_PORTS : integer := 2;

--------------------------------------------------------------------------------
--	Theoretisch reichen 31 Register, dann muss darf die Adressabbildungs-
--	funktion Adresse 0 oder 31 nicht verwenden. Fuer eine voellig freie
--	Adressabbildung sind hier aber 32 Register vorgesehen.
--------------------------------------------------------------------------------
	constant MAX_REG_NR : integer := 31;
	type REGFILE_TYPE is array (0 to MAX_REG_NR) of std_logic_vector(31 downto 0);
	signal REGFILE : REGFILE_TYPE := (others =>(others => '0'));
--------------------------------------------------------------------------------
--	Das fuer den PC verwendete Register muss zur entsprechenden physischen Adresse
--	der Adressabbildungsfunktion passen
--------------------------------------------------------------------------------
	constant W_PORT_PC_ADDR : std_logic_vector(4 downto 0) := GET_INTERNAL_ADDRESS("1111",USER,'0');


	signal REG_N_ENABLE	: std_logic_vector(0 to MAX_REG_NR) := (others => '0');
	signal USE_W_PORT_B	: std_logic_vector(0 to MAX_REG_NR) := (others => '0');
	signal USE_W_PORT_PC	: std_logic := '0';

--	Integers aus den Adressen zur einfacheren Adressierung des Registerspeichers.
	signal REF_W_PORT_A_ADDRESS_INTEGER	: INTEGER range 0 to MAX_REG_NR;
	signal REF_W_PORT_B_ADDRESS_INTEGER	: INTEGER range 0 to MAX_REG_NR; 
	signal W_PORT_PC_ADDRESS_INTEGER	: INTEGER range 0 to MAX_REG_NR;

--	Bei der Adressierung des Registerarrays treten bei direkter Verwendung
--	von Konvertierungsfunktionen Warnungen auf, daher werden Hilfssignale
--	mit geeigneter Bereichseinschränkung verwendet (ist nur fuer die
--	Variante mit 31 statt 32 Registern wirklich notwendig)
	signal REF_R_PORT_A_ADDRESS_INTEGER : integer range 0 to MAX_REG_NR;
	signal REF_R_PORT_B_ADDRESS_INTEGER : integer range 0 to MAX_REG_NR;
	signal REF_R_PORT_C_ADDRESS_INTEGER : integer range 0 to MAX_REG_NR;


begin

	REF_W_PORT_A_ADDRESS_INTEGER 	<= to_integer(unsigned(REF_W_PORT_A_ADDR)); 
	REF_W_PORT_B_ADDRESS_INTEGER 	<= to_integer(unsigned(REF_W_PORT_B_ADDR)) when (NR_WRITE_PORTS > 1) else 0;
	W_PORT_PC_ADDRESS_INTEGER 	<= to_integer(unsigned(W_PORT_PC_ADDR));

--------------------------------------------------------------------------------
--	Im ersten Prozess werden Enable-Signale fuer jedes einzelne Register
--	festgelegt. Verweist wenigstens einer der Schreibports auf ein
--	bestimmtes Register, wird das ENABLE des Registers aktiviert.
--	32 weitere Steuersignale zeigen an, ueber Port A auf ein bestimmtes
--	Register zugegriffen werden soll. Ist das nicht der Fall, zeigt das
--	Steuersignal an, dass ein Zugriff ueber Port B moeglich ist.
--	Ein weiteres Steuersignal ermoeglich den Zugriff auf auf den PC durch
--	den PC-Schreibport, wenn kein anderweitiger Zugriff auf den PC vorliegt.
--------------------------------------------------------------------------------
	DETERMINE_VALID_PORTS : process(REF_W_PORT_A_ENABLE, REF_W_PORT_B_ENABLE, REF_W_PORT_PC_ENABLE, REF_W_PORT_A_ADDR, REF_W_PORT_B_ADDR,
					REF_W_PORT_A_ADDRESS_INTEGER,REF_W_PORT_B_ADDRESS_INTEGER)
	begin
		USE_W_PORT_PC <= '0';
		for i in 0 to MAX_REG_NR loop
			if (i = REF_W_PORT_A_ADDRESS_INTEGER and REF_W_PORT_A_ENABLE = '1') or (i = REF_W_PORT_B_ADDRESS_INTEGER and REF_W_PORT_B_ENABLE = '1' AND NR_WRITE_PORTS > 1) then
				REG_N_ENABLE(i) <= '1';
				if (i = REF_W_PORT_A_ADDRESS_INTEGER and REF_W_PORT_A_ENABLE = '1') then
					USE_W_PORT_B(i) <= '0';
				else
					if NR_WRITE_PORTS > 1 then
						USE_W_PORT_B(i) <= '1';
					else
						USE_W_PORT_B(i) <= '0';
					end if;	
				end if;	
			else
				USE_W_PORT_B(i) <= '0';
				if i = W_PORT_PC_ADDRESS_INTEGER then
					USE_W_PORT_PC <= REF_W_PORT_PC_ENABLE;
					REG_N_ENABLE(i) <= REF_W_PORT_PC_ENABLE;
				else
					REG_N_ENABLE(i) <= '0';

				end if;
			end if;
		end loop;
	end process DETERMINE_VALID_PORTS;


--------------------------------------------------------------------------------
--	In Schreib-Prozess werden die oben erzeugten Steuersignale
--	fuer jedes Register verarbeitet und die entsprechenden
--	Dateneingaenge auf die Register geschaltet.
--------------------------------------------------------------------------------
	WRITE_TO_REGFILE : process(REF_CLK)
	begin
				if REF_CLK='1' and REF_CLK'event then
--					Synchrones Reset
					if REF_RST='1' and USE_OPTIONAL_RESET then
						REGFILE <= (others =>(others => '0'));
					else
						for i in 0 to MAX_REG_NR loop
							if i /= W_PORT_PC_ADDRESS_INTEGER then
								if REG_N_ENABLE(i) = '1' then
									if USE_W_PORT_B(i) = '1' then
										REGFILE(i) <= REF_W_PORT_B_DATA;
									else
										REGFILE(i) <= REF_W_PORT_A_DATA;
									end if;
								else
									REGFILE(i) <= REGFILE(i);
								end if;
							else
--------------------------------------------------------------------------------
--	Der PC wird gesondert behandelt							
--------------------------------------------------------------------------------
								if REG_N_ENABLE(i) = '1' then
									if USE_W_PORT_PC = '1' then
										REGFILE(i) <= REF_W_PORT_PC_DATA;
									else	
										if USE_W_PORT_B(i) = '1' then
											REGFILE(i) <= REF_W_PORT_B_DATA;
										else
											REGFILE(i) <= REF_W_PORT_A_DATA;
										end if;
									end if;	
								else
									REGFILE(i) <= REGFILE(i);
								end if;
							end if;
						end loop;	
					end if;
				end if;
	end process WRITE_TO_REGFILE;

--------------------------------------------------------------------------------
--	Lesezugriffe, asynchron
--------------------------------------------------------------------------------
				
	REF_R_PORT_A_ADDRESS_INTEGER <= to_integer(unsigned(REF_R_PORT_A_ADDR)) ;
	REF_R_PORT_B_ADDRESS_INTEGER <= to_integer(unsigned(REF_R_PORT_B_ADDR)) when NR_READ_PORTS > 1 else 0;
	REF_R_PORT_C_ADDRESS_INTEGER <= to_integer(unsigned(REF_R_PORT_C_ADDR)) when NR_READ_PORTS > 2 else 0;

	REF_R_PORT_A_DATA <= REGFILE(REF_R_PORT_A_ADDRESS_INTEGER);
	REF_R_PORT_B_DATA <= REGFILE(REF_R_PORT_B_ADDRESS_INTEGER) when (NR_READ_PORTS > 1) else (others => '0');
	REF_R_PORT_C_DATA <= REGFILE(REF_R_PORT_C_ADDRESS_INTEGER) when (NR_READ_PORTS > 2) else (others => '0');
		
--------------------------------------------------------------------------------
--	Zuweisungen interner Signale an globale Signale zu Testzwecken
--------------------------------------------------------------------------------

-- synthesis translate_off	
	AGP_PHY_R0	<= REGFILE(0);
	AGP_PHY_R1	<= REGFILE(1);
	AGP_PHY_R2	<= REGFILE(2);
	AGP_PHY_R3	<= REGFILE(3);
	AGP_PHY_R4	<= REGFILE(4);
	AGP_PHY_R5	<= REGFILE(5);
	AGP_PHY_R6	<= REGFILE(6);
	AGP_PHY_R7	<= REGFILE(7);
	AGP_PHY_R8	<= REGFILE(8);
	AGP_PHY_R9	<= REGFILE(9);
	AGP_PHY_R10	<= REGFILE(10);
	AGP_PHY_R11	<= REGFILE(11);
	AGP_PHY_R12	<= REGFILE(12);
	AGP_PHY_R13	<= REGFILE(13);
	AGP_PHY_R14	<= REGFILE(14);
	AGP_PHY_R15	<= REGFILE(15);
	AGP_PHY_R16	<= REGFILE(16);
	AGP_PHY_R17	<= REGFILE(17);
	AGP_PHY_R18	<= REGFILE(18);
	AGP_PHY_R19	<= REGFILE(19);
	AGP_PHY_R20	<= REGFILE(20);
	AGP_PHY_R21	<= REGFILE(21);
	AGP_PHY_R22	<= REGFILE(22);
	AGP_PHY_R23	<= REGFILE(23);
	AGP_PHY_R24	<= REGFILE(24);
	AGP_PHY_R25	<= REGFILE(25);
	AGP_PHY_R26	<= REGFILE(26);
	AGP_PHY_R27	<= REGFILE(27);
	AGP_PHY_R28	<= REGFILE(28);
	AGP_PHY_R29	<= REGFILE(29);
	AGP_PHY_R30	<= REGFILE(30);
	AGP_PHY_R31	<= REGFILE(31);
-- synthesis translate_on	

end BEHAVE;

