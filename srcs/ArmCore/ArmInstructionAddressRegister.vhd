--------------------------------------------------------------------------------
--	Instruktionsadressregister-Modul fuer den HWPR-Prozessor
--------------------------------------------------------------------------------
--	Datum:		07.06.2010
--	Version:	1.00
--------------------------------------------------------------------------------
--	Aenderungen:
--	Dem PC wurde ein History Buffer hinzugefuegt um bei Data Aborts
--	die Adresse der verursachenden Instruktion eindeutig rekonstruieren zu 
--	koennen.
--	Mit IAR_REVOKE wird auf IAR_NEXT_ADDR_OUT der Eintrag im HB an Adresse
--	IAR_HISTORY_ID statt des naechsten Wertes von PC ausgegeben	
--	Der Puffer kann bei Bedarf auf bis zu 16 Eintraege ohne weiteren
--	Ressourcenverbrauch vergroessert werden, sofern die Abbildung auf
--	LUT-Schieberegister (kein Reset!) erfolgt.
--------------------------------------------------------------------------------
--	Statt des urspruenglichen CAM wird der HB nun unmittelbar mit einem
--	1-Port RAM realisiert. Dies ist moeglich, solange die IDs unmittelbar
--	als RAM-Adressen aufgefasst werden koennen. Diese Loesung ist weniger
--	flexibel (und nicht so elegant, weil sie nicht direkt den Verlauf des
--	PC abbildet) aber schneller und kompakter.
--	Statt des urspruenglichen CAM wird der HB nun unmittelbar mit einem
--------------------------------------------------------------------------------
--	Grundlegende Aenderung:
--	Die zwischenzeitliche Loesung mit zwei Registern fuer die aktuelle
--	und naechste Adresse wurde verworfen. In der aktuellen Fassung
--	wird der Instruktionsspeicher generell aus dem einzelnen Register
--	gespeist.
--	IAR_RST (highaktiv) schreibt den Reset-Exceptionvektor in das
--	Register. Es nimmt jeweils nur die hochwertigen 30 Bit einer Adresse
--	auf, da Instruktionen immer ausgerichtet im Speicher liegen.
--	Falls der Prozessor zur Unterstuetzung von Thumb-Instruktionen
--	erweitert werden soll, muss auch Bit 1 der Adresse geschpeichert werden.
--	Die Aktualisierung des Registers mit der steigenden 
--	Systemtaktflanke erfolgt entweder mit dem aktuellen oder
--	inkrementierten Registerinhalt oder mit einem neuen Datum von
--	Eingang IAR_ADDR_IN, gesteuert durch IAR_INC und IAR_LOAD, wobei das 
--	Ladesignal priorisiert ist. 
--	Der inkrementierte Registerinhalt wird zusaetzlich zum aktuellen
--	Registerinhalt ans IAR_NEXT_ADDR_OUT ausgegeben und von dort
--	in den Registerspeicher geschrieben. Fuer die universelle
--	Registerspeicherrealisierung im HWPR geschieht dies bereits zur
--	fallenden Systemtaktflanke, es steht also nur T/2 fuer Inkrement,
--	Transport und Schreiben der inkrementierten Adresse zur Verfuegung.
--	Das ist gegenwaertig akzeptabel, da der kritische Pfad des Gesamt-
--	systems mehr als die doppelte Laufzeit des Adressinkrementers 
--	benoetigt. 
--------------------------------------------------------------------------------
--	Speziell zur Unterstuetzung von Data-Aborts (zukuenftig koennte
--	der Ansatz auch zur Behandlung der uebrigen Exceptions interessant
--	sein) wird das Adressregister um einen kleinen Puffer ergaenzt, der
--	die letzten 4,8 oder 16 (konfigurierbar) Instruktionsadressen aufnimmt.
--	In der Fetch-Stufe wird jeder neuen Instruktion eine eindeutige ID
--	mit 2 bis 4 Bit Breite zugeordnet und die Fetch-Adresse mit der ID
--	als Adresse in den Puffer geschrieben. Im Fall eines Data-Aborts
--	wird die Instruktionsadresse aus dem Puffer rekonstruiert, sobald
--	die fehlerhafte Instruktion die WB-Stufe (nicht MEM-Stufe!) erreicht,
--	und ueber den NEXT_ADDR-Ausgang in den Registerspeicher geschrieben,
--	dann gemaess der Regeln fuer die Behandlung von Aborts angepasst und
--	dann in das Link-Register geschrieben.
--	Die Schaltung erzeugt im Takt, in dem die erste Instruktion
--	nach einem Sprung geholt wird, eine fehlerhafte Adresse im PC, zu 
--	diesem Zeitpunkt befindet sich aber keine Instruktion in der ID-Stufe,
--	sodass dieses Verhalten akzeptabel ist.
--	IAR_UPDATE_CONDITION wurde vollstaendig entfernt, Schreibzugriffe
--	auf den Puffer werden ausschliesslich durch den Steuereingang
--	IAR_UPDATE_HB gesteuern.
--------------------------------------------------------------------------------
--	Zwei veraltete globale Probesignale entfernt.
--	Einen Testprozess hinzugefuegt, der Warnungen ausgibt, wenn
--	IAR_LOAD und IAR_INC gleichzeitig aktiv sind. Zwar ist LOAD
--	priorisiert, dennoch deutet dieses Ereignis auf eine unsaubere
--	Steuerung hin.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.ArmTypes.INSTRUCTION_ID_WIDTH;
use work.ArmTypes.VCR_RESET;

entity ArmInstructionAddressRegister is
	port(
		IAR_CLK 	: in std_logic;
		IAR_RST 	: in std_logic;
		IAR_INC		: in std_logic;
		IAR_LOAD 	: in std_logic;
		IAR_REVOKE	: in std_logic;
		IAR_UPDATE_HB	: in std_logic;
--------------------------------------------------------------------------------
--	INSTRUCTION_ID_WIDTH  ist ein globaler Konfigurationsparameter
--	zur Einstellung der Breite der Instruktions-IDs und damit der Tiefe
--	der verteilten Puffer. Eine Breite von 3 Bit genuegt fuer die 
--	fuenfstufige Pipeline definitiv.
--------------------------------------------------------------------------------
		IAR_HISTORY_ID	: in std_logic_vector(INSTRUCTION_ID_WIDTH-1 downto 0);
		IAR_ADDR_IN 	: in std_logic_vector(31 downto 2);
		IAR_ADDR_OUT 	: out std_logic_vector(31 downto 2);
		IAR_NEXT_ADDR_OUT : out std_logic_vector(31 downto 2)
	    );
end ArmInstructionAddressRegister;

architecture BEHAVE of ArmInstructionAddressRegister is

--	Puffer mit konfigurierbarer Breite und Tiefe
	component ArmRamBuffer
	generic(
		ARB_ADDR_WIDTH : natural range 1 to 4 := 3;
		ARB_DATA_WIDTH : natural range 1 to 64 := 32
	       );
	port(
		ARB_CLK 	: in std_logic;
		ARB_WRITE_EN	: in std_logic;
		ARB_ADDR	: in std_logic_vector(ARB_ADDR_WIDTH-1 downto 0);
		ARB_DATA_IN	: in std_logic_vector(ARB_DATA_WIDTH-1 downto 0);          
		ARB_DATA_OUT	: out std_logic_vector(ARB_DATA_WIDTH-1 downto 0)
		);
	end component;

	signal ADDRESS_REGISTER 	: std_logic_vector(31 downto 2) := (others => '0');
	signal INCREMENTED_ADDRESS 	: std_logic_vector(31 downto 2);
	signal IAR_NEXT_ADDRESS		: std_logic_vector(31 downto 2) := (others => '0');
	signal IAR_HISTORY_ENTRY	: std_logic_vector(31 downto 2);

begin

	IAR_HISTORY_BUFFER: ArmRamBuffer 
	generic map(
--------------------------------------------------------------------------------
--	Die Tiefe des Puffers entspricht sinnvollerweise 2** der
--	Breite der Instruktions-IDs.
--------------------------------------------------------------------------------
			ARB_ADDR_WIDTH => INSTRUCTION_ID_WIDTH,
			ARB_DATA_WIDTH => 30
		   )
	port map(
		ARB_CLK		=> IAR_CLK,
		ARB_WRITE_EN	=> IAR_UPDATE_HB,
		ARB_ADDR	=> IAR_HISTORY_ID,
		ARB_DATA_IN	=> ADDRESS_REGISTER,
		ARB_DATA_OUT	=> IAR_HISTORY_ENTRY
	);

--------------------------------------------------------------------------------
--	Die zum PC uebertragene Adresse entspricht dem Inkrement der aktuellen
--	Adresse oder dem gerade adressierten Eintrag des History Buffers.
--------------------------------------------------------------------------------
	SET_NEXT_ADDRESS_OUT : process(IAR_REVOKE,INCREMENTED_ADDRESS, IAR_HISTORY_ENTRY)is	
	begin
	
		if IAR_REVOKE= '1' then
			IAR_NEXT_ADDR_OUT	<= IAR_HISTORY_ENTRY;
		else
			IAR_NEXT_ADDR_OUT	<= INCREMENTED_ADDRESS;
		end if;
	end process SET_NEXT_ADDRESS_OUT;

--------------------------------------------------------------------------------
--	Zwei Multiplexer, gesteuert durch IAR_LOAD und IAR_INC zur Auswahl
--	der Instruktionsadresse, die in das Register zurueckgeschrieben wird.
--	IAR_LOAD ist priorisiert, es ist aber auch sonst nicht
--	zu erwarten, dass LOAD und INC gleichzeitig gesetzt sind.
--------------------------------------------------------------------------------
	IAR_NEXT_ADDRESS	<= IAR_ADDR_IN when IAR_LOAD = '1' else INCREMENTED_ADDRESS when IAR_INC = '1' else ADDRESS_REGISTER;
	IAR_ADDR_OUT		<= ADDRESS_REGISTER;
	INCREMENTED_ADDRESS	<= std_logic_vector(unsigned(ADDRESS_REGISTER) + 1);

	SET_INSTRUCTION_ADDRESS_REGISTER : process(IAR_CLK)is
	begin
		if(IAR_CLK'event and IAR_CLK ='1') then
			if(IAR_RST = '1') then
--	Resetvektoradresse aus ArmTypes
				ADDRESS_REGISTER <= VCR_RESET(31 downto 2);
			else
				ADDRESS_REGISTER <= IAR_NEXT_ADDRESS;
			end if;
		end if;	
	end process SET_INSTRUCTION_ADDRESS_REGISTER;

-- synthesis translate_off
--	LOAD_INC_MUTUAL_EXCLUSIVE : process(IAR_LOAD,IAR_INC) is
--	begin
--		assert(IAR_LOAD and IAR_INC = '0') report "InstructionAddressRegister: LOAD = INC = 1" severity warning;
--	end process LOAD_INC_MUTUAL_EXCLUSIVE;
-- synthesis translate_on	

end BEHAVE;
