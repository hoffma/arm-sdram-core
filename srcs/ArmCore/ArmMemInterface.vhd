--------------------------------------------------------------------------------
--	Schnittstelle zur Anbindung des RAM an die ARM-Busse
--------------------------------------------------------------------------------
--	Datum:		16.05.2010
--	Version:	1.3
--------------------------------------------------------------------------------
--	Aenderungen:
--	Saemtlicher Code fuer die Realisierung des Speichers mit
--	bidirektionalen Datenleitungen auf der Datenseite wurde entfernt da
--	diese Variante auch perspektivisch nicht genutzt wird.
--	Eine Alternative Realisierung des Adresstests der Instruktions-
--	seite wurde hinzugefuegt.
--------------------------------------------------------------------------------
--	Ausblick:
--	Langfristig sollte die Schnittstelle zu den Bussen so 
--	umgestaltet werden, dass nur noch die tatsaechlich benoetigten
--	Adressleitungen angeschlossen werden. Der Adresstest der Instruktions-
--	seite sollte um eine Hierarchieebene nach oben verlagert werden. 
--	Der Alignementtest der Datenschnittstelle kann besser direkt im
--	CSG stattfinden statt ihn in jedem Peripheriemodul einzeln
--	zu realisieren. Mit diesen Aenderungen wuerde ArmMemInterface
--	praktisch ueberfluessig. 
--------------------------------------------------------------------------------
library work;
use work.ArmConfiguration.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ArmMemInterface is
	generic(
--------------------------------------------------------------------------------
--	Beide Generics sind fuer das HWPR nicht relevant. In einer flexibleren
--	Implementierung des Speichers dient SELECT_LINES zur Konfiguration
--	der Speichertiefe und EXTERNAL_ADDRESS_DECODING_INSTRUCTION legt fest,
--	ob der Test auf Validitaet der Instruktionsadressen in die naechst
--	hoehere Hierarchiestufe verlegt wird.
--------------------------------------------------------------------------------
		SELECT_LINES				: natural range 0 to 2 := 1;
		EXTERNAL_ADDRESS_DECODING_INSTRUCTION	: boolean := false
	       );
	Port (  RAM_CLK	: in  std_logic;
	        RAM_RST	: in  std_logic;
--	Instruction-Interface	
       		IDE	: in std_logic;	
		IA	: in std_logic_vector(31 downto 2);
		ID	: out std_logic_vector(31 downto 0);	
		IABORT	: out std_logic;
--	Data-Interface
		DDE	: in std_logic;
		DnRW	: in std_logic;
		DMAS	: in std_logic_vector(1 downto 0);
		DA 	: in std_logic_vector(31 downto 0);
		DDIN	: in std_logic_vector(31 downto 0);
		DDOUT	: out std_logic_vector(31 downto 0);
		DABORT	: out std_logic
       );
end ArmMemInterface;

architecture BEHAVE of ArmMemInterface is	
    COMPONENT blk_mem_gen_0
      PORT (
        clka : IN STD_LOGIC;
        rsta : IN STD_LOGIC;
        ena : IN STD_LOGIC;
        wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        clkb : IN STD_LOGIC;
        rstb : IN STD_LOGIC;
        enb : IN STD_LOGIC;
        web : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        rsta_busy : OUT STD_LOGIC;
        rstb_busy : OUT STD_LOGIC
      );
    END COMPONENT;

--	Byteorientiertes Enable-Signal fuer die Datenseite
	signal DWE		: std_logic_vector(3 downto 0);
--	Interne Datenausgaenge
	signal DOB		: std_logic_vector(31 downto 0);
	signal DOA		: std_logic_vector(31 downto 0);
--	Interne Steuersignale zur Anzeige von Zugriffsfehlern
	signal DATA_MISALIGNED 		: std_logic;
	signal INST_WRONG_ADDRESS	: std_logic;
--	Generische Verbindung zwischen Datenbus und Port B des RAM 
	signal IA_TO_MEM		: std_logic_vector(10 + SELECT_LINES downto 0); -- 12 bit == 32x4096 
	signal DA_TO_MEM		: std_logic_vector(10 + SELECT_LINES downto 0);

	constant ADDR_TEST_V2		: boolean := true;
	
	signal PREV_IDE, PREV_DDE : std_logic;
	signal DDELAY_COUNT, IDELAY_COUNT : std_logic_vector(31 downto 0);
	signal IBUSY_INTERNAL : std_logic;
begin

--	Nur bei Lesezugriffen auf den Instruktionsspeicher wird ID getrieben
	ID <= DOA when IDE = '1' else (others => 'Z');
--	Nur bei Lesezugriffen auf den Datenspeicher wird DDOUT getrieben
	DDOUT <= DOB when (DDE = '1' and DnRW = '0') else (others => 'Z');
--	DDOUT <= DOB when (DDE = '1') else (others => 'Z');


--------------------------------------------------------------------------------
--	Test auf Gueltigkeit der Instruktionsadresse. Solange nur der einzelne
--	Instruktionsspeicher am Instruktionsbus angeschlossen ist, kann dies
--	innerhalb von MemInterface geschehen, ist aber eigentlich
--	keine schoene Loesung. 
--	Die erste Variante realisiert den Adresstest durch Vergleichs-
--	operationen, dies ist eine geeignete Musterloesung fuer das HWPR.
--	Die zweite Variante kommt ohne <|>-Vergleich und damit
--	ohne Dualarithmetik aus. Implizit wird hier vorausgesetzt (sinnvoll,
--     	aber im HWPR nicht explizit festgelegt), dass der Instruktionsspeicher
--	eine sinnvolle (Block)groesse (4Ki, 8Ki, 16Ki,..)aufweist und seine
--	Startadresse dem Vielfachen der Mindestblockgroesse entspricht
--	(wie bei den Peripheriekomponenten am Datenbus).
--------------------------------------------------------------------------------
	GEN_ADDR_TEST_V1 : if not ADDR_TEST_V2 generate
	begin
		INST_WRONG_ADDRESS <= '0' when EXTERNAL_ADDRESS_DECODING_INSTRUCTION or
				(
				(unsigned(IA(31 downto 2) & "00") >= unsigned(INST_LOW_ADDR)) and
				(unsigned(IA(31 downto 2) & "00") <= unsigned(INST_HIGH_ADDR))
				) else '1';
	end generate GEN_ADDR_TEST_V1;	

--------------------------------------------------------------------------------
--	Etwas schoenere Variante des Tests der Instruktionsadressen
--	ohne arithemtische Vergleiche, stellt einige Anforderungen
--	an die Form von Start- und Endadresse des Instruktionsspeichers.
--------------------------------------------------------------------------------

	GEN_ADDR_TEST_V2 : if ADDR_TEST_V2 generate 
	begin
		TEST_DATA_ADDRESS : process(IA)is
			variable LOWER_BOUND : natural range 0 to 31 := 31;
			variable OUT_OF_BOUND : std_logic := '0';
		begin
			OUT_OF_BOUND := '0';
			if not EXTERNAL_ADDRESS_DECODING_INSTRUCTION then	
--------------------------------------------------------------------------------
--	In dieser Uebergangsloesung darf der Instruktionsspeicher
--	maximal die Haelfte des Adressraums umfassen.	
--------------------------------------------------------------------------------
				for i in 31 downto 2 loop
--------------------------------------------------------------------------------
--	Die Instruktionsadresse muss nur von Bit 31 bis exklusive
--	des Index getestet werden, an dem sich obere und untere
--	Adressgrenze erstmals unterscheiden.	
--------------------------------------------------------------------------------
					if INST_LOW_ADDR(i) /= INST_HIGH_ADDR(i)then
						LOWER_BOUND := i;
					end if;
					exit when INST_LOW_ADDR(i) /= INST_HIGH_ADDR(i);
				end loop;
				OUT_OF_BOUND := '0';
				for i in 31 downto LOWER_BOUND + 1 loop
					if IA(i) /= INST_HIGH_ADDR(i) then
					OUT_OF_BOUND := '1';
					end if;	
					exit when IA(i) /= INST_HIGH_ADDR(i);
				end loop;
			end if;	
			INST_WRONG_ADDRESS <= OUT_OF_BOUND;
		end process TEST_DATA_ADDRESS;
	end generate GEN_ADDR_TEST_V2;

--------------------------------------------------------------------------------

	IABORT <= INST_WRONG_ADDRESS when IDE = '1' else 'Z';	

--	Test auf korrekte Ausrichtung des Datenspeicherzugriffs
	CHECK_DATA_READ_ALIGNEMENT : process(DMAS(1 downto 0),DA(1 downto 0))
	begin
		DATA_MISALIGNED <= '0';
		case DMAS(1 downto 0) is
			when "10" =>
				-- Wort
				if (DA(1 downto 0) /= "00")then
					DATA_MISALIGNED <= '1';
				end if;	
			when "01" =>
				--Halbwort
				if (DA(0) /= '0') then
					DATA_MISALIGNED <= '1';
				end if;
			when "00" =>
				--Byte
				null;	
			when others =>
				DATA_MISALIGNED <= '1';
		end case;
	end process CHECK_DATA_READ_ALIGNEMENT;

	DABORT <= DATA_MISALIGNED when DDE = '1' else 'Z';

--------------------------------------------------------------------------------
--	Erzeugung der byteorientierten Enable-Signale bei Datenspeicherzugriffen.
--------------------------------------------------------------------------------
--	Die byteweisen Zugriffssignale muessen aus DMAS und der Adresse
--	abgeleitet werden: 
--	DMAS = 	00 : Bytezugriff
--		01 : Halbwortzugriff
--		10 : Wortzugriff
--		11 : nicht definiert, der Schreibzugriff findet nicht statt
--
--	DA(1:0) muss bei Wortzugriffen = 00 sein
--		muss bei Halbwortzugriffen = 00 oder 10 sein	
--------------------------------------------------------------------------------
	DETERMINE_DWE_SIGNALS : process(DDE,DnRW,DMAS(1 downto 0),DA(1 downto 0))
	begin
		DWE <= "0000";
		if(DDE = '1' and DnRW = '1')then
			case DMAS(1 downto 0) is
				when "10" =>
					if (DA(1 downto 0) = "00")then
						DWE <= "1111";
					else
					end if;	
				when "01" =>
					if (DA(0) = '0') then
						if(DA(1) = '0')then
							DWE <= "0011";
						else
							DWE <= "1100";
						end if;
					else
					end if;	
				when "00" =>
					case DA(1 downto 0)is
					when "00" =>
						DWE <= "0001";
					when "01" =>
						DWE <= "0010";
					when "10" =>
						DWE <= "0100";
					when others =>
						DWE <= "1000";
					end case;	
				when others =>
					DWE <= "0000";
			end case;
		end if;
	end PROCESS DETERMINE_DWE_SIGNALS;	

--------------------------------------------------------------------------------	
--	Die Menge der Adressleitungen zum Speicherblock haengt
--	von dessen Groesse ab, die wiederum durch SELECT_LINES (0, 1, 2)
--	bestimmt wird. Fuer den HWPR-Prozessor sind die korrekten
--	Indizes 13 downto 2. Die beiden niederwertigen Adressbits werden
--	nicht mit dem Speicherblock verbunden, da sowieso immer ein 
--	vollstaendiges Wort angelegt oder ausgelesen werden muss.
	
--	Schreibzugriffe auf Port A (Instruktionen) finden nicht statt, daher
--	sind alle zugehoerigen Steuerleitungen fest auf 0 gelegt.
--------------------------------------------------------------------------------	
	IA_TO_MEM <= IA(12 + SELECT_LINES downto 2);
	DA_TO_MEM <= DA(12 + SELECT_LINES downto 2);

--	DATA_PROGRAMM_MEMORY: entity WORK.ArmRAMB16_4kx32_Wrapper(BEHAVE)
--	generic map(
--		   SELECT_LINES => SELECT_LINES
--		   )
--	port map(
--		RAM_CLK	=> RAM_CLK,
--		RAM_RST => RAM_RST,

--		WEA	=> "0000",
--		ENA	=> IDE,
--		ADDRA	=> IA_TO_MEM,
--		DIA	=> (others => '0'),

--		WEB	=> DWE,
--		ENB	=> DDE,
--		ADDRB	=> DA_TO_MEM,
--		DIB	=> DDIN,

--		DOA	=> DOA,
--		DOB	=> DOB
--	);
	DATA_PROGRAMM_MEMORY: blk_mem_gen_0
	port map(
		clka	=> RAM_CLK,
		rsta => RAM_RST,

		wea	=> "0000",
		ena	=> IDE,
		addra	=> IA_TO_MEM,
		dina	=> (others => '0'),
        
        clkb => RAM_CLK,
        rstb => RAM_RST,
		web	=> DWE,
		enb	=> DDE,
		addrb	=> DA_TO_MEM,
		dinb	=> DDIN,

		douta	=> DOA,
		doutb	=> DOB,
		
		rsta_busy => open,
		rstb_busy => open
	);
	
end BEHAVE;

