--------------------------------------------------------------------------------
--	Topmodul des ARM-SoC
--------------------------------------------------------------------------------
--	Datum:		04.07.2010
--	Version:	1.00
--------------------------------------------------------------------------------
--	Aenderungen:
--	Erstmalige Verwendung von (n)Wait-Signalen zwischen Systemcontroller
--	und Prozessorkern, Waitsignale von Datenbus und Instruktionsbus werden
--	zusammengefasst.
-- 	Waitsignale highaktiv, individuelle Modusbits fuer beide Busse.
--	DBUS_DBE gegen CORE_DBE ersetzt, da im Prinzip jeder Master ein
--	ACK-Signal benoetigt und es deshalb den individuellen Namen des
--	Masters fuehren sollte.	
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
library work;
use work.ArmGlobalProbes.all;
use work.ArmConfiguration.all;

--------------------------------------------------------------------------------
--	Simulationsmodelle fuer die Verhaltenssimulation bei fehlerhafter Implementierung
--------------------------------------------------------------------------------
--library ARM_SIM_LIB;
--	 use ARM_SIM_LIB.ArmRS232Interface;
--	 use ARM_SIM_LIB.ArmMemInterface;

--------------------------------------------------------------------------------
--	Anweisung fuer die Synthese, die eigenen Modelle zu verwenden (wenn diese fehlerfrei sind)
--	sollte nur einkommentiert werden, wenn waehrend der Synthese ungewollt die ngc-Dateien aus /opt/rt/ARM_LIB verwendet werden
--------------------------------------------------------------------------------
library work;
	use work.ArmRS232Interface;
	use work.ArmMemInterface;

entity ArmTop is
    Generic (
        CACHE_ENABLE : boolean := FALSE
    );
	Port (
		EXT_RST : in  std_logic;
		INT_CLK : in  std_logic;
		INT_INV_CLK: in std_logic;
		EXT_LDP : in  std_logic;
		EXT_RXD : in  std_logic;
		EXT_TXD : out std_logic;
		EXT_LED : out std_logic;
		DCM_LOCKED: in std_logic;
		
		SDRAM_CLK : in std_logic;
		CLK166 : in std_logic;
		ddr3_dq       : inout std_logic_vector(15 downto 0);
        ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
        ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
        
        ddr3_addr     : out   std_logic_vector(13 downto 0);
        ddr3_ba       : out   std_logic_vector(2 downto 0);
        ddr3_ras_n    : out   std_logic;
        ddr3_cas_n    : out   std_logic;
        ddr3_we_n     : out   std_logic;
        ddr3_reset_n  : out   std_logic;
        ddr3_ck_p     : out   std_logic_vector(0 downto 0);
        ddr3_ck_n     : out   std_logic_vector(0 downto 0);
        ddr3_cke      : out   std_logic_vector(0 downto 0);
        ddr3_cs_n     : out   std_logic_vector(0 downto 0);
        ddr3_dm       : out   std_logic_vector(1 downto 0);
        ddr3_odt      : out   std_logic_vector(0 downto 0)
	);
end ArmTop;
 

architecture BEHAVE of ArmTop is
--	Signale des Instruktionsbus
	signal IBUS_IA 		: std_logic_vector(31 downto 2);
	signal IBUS_ID 		: std_logic_vector(31 downto 0);
	signal IBUS_ABORT 	: std_logic;
	signal IBUS_WAIT 	: std_logic := '0';	
	signal IBUS_IBE		: std_logic;
	signal IBUS_IEN		: std_logic;
	signal IBUS_MODE	: std_logic_vector(4 downto 0);
--------------------------------------------------------------------------------
--	Signale des Datenbus, die Benennung der unidirektioalen Datenleitungen
--	ist aus Prozessorsicht zu lesen.
--------------------------------------------------------------------------------
	signal DBUS_DA		: std_logic_vector(31 downto 0);
	signal DBUS_DDOUT	: std_logic_vector(31 downto 0);
	signal DBUS_DDIN	: std_logic_vector(31 downto 0);
	signal DBUS_WAIT	: std_logic := '0';
	signal DBUS_ABORT	: std_logic;
	signal DBUS_DnRW	: std_logic;
	signal DBUS_DMAS	: std_logic_vector(1 downto 0);
	signal DBUS_MODE	: std_logic_vector(4 downto 0);
	signal DBUS_DEN		: std_logic;
	signal DBUS_CS_RS232	: std_logic;
	signal DBUS_CS_MEM	: std_logic;
--	Bus-Requests beider Master 
	signal CTRL_DEN		: std_logic;
	signal CORE_DEN		: std_logic;
--------------------------------------------------------------------------------
--	Bus-ACK fuer den Kern, ersetzt BUS_DBE. Der System-Controller braucht
--	kein ACK, weil er immer priorisiert wird.
--------------------------------------------------------------------------------
	signal CORE_DBE		: std_logic;	

--	Entprelltes Load-Signal	
	signal INT_LDP		: std_logic;

	signal CORE_WAIT	: std_logic;

	signal CSG_CS_LINES	: std_logic_vector(0 to 7);
	signal s_TXD: std_logic;

	component ArmCore
	port(
		CORE_CLK	: IN std_logic;
		CORE_INV_CLK	: IN std_logic;
		CORE_RST	: IN std_logic;
		CORE_WAIT	: IN std_logic;
		CORE_IBE	: IN std_logic;
		CORE_ID		: IN std_logic_vector(31 downto 0);
		CORE_IABORT	: IN std_logic;
		CORE_DDIN	: IN std_logic_vector(31 downto 0);
		CORE_DABORT	: IN std_logic;
		CORE_DBE	: IN std_logic;
		CORE_FIQ	: IN std_logic;
		CORE_IRQ	: IN std_logic;          
		CORE_IMODE	: out std_logic_vector(4 downto 0);
		CORE_DMODE	: out std_logic_vector(4 downto 0);
		CORE_IEN	: OUT std_logic;
		CORE_IA		: OUT std_logic_vector(31 downto 2);
		CORE_DA		: OUT std_logic_vector(31 downto 0);
		CORE_DDOUT	: OUT std_logic_vector(31 downto 0);
		CORE_DMAS	: OUT std_logic_vector(1 downto 0);
		CORE_DnRW	: OUT std_logic;
		CORE_DEN	: OUT std_logic
		);
	end component;

	component ArmSwitchDebounce
	port(
		SYS_RST 	: in std_logic;
		SYS_CLK 	: in std_logic;
		SDB_ASYNC_INPUT : in std_logic;
		SDB_SYNC_OUTPUT : out std_logic
	);
	end component;
	
	component ArmChipSelectGenerator
	port(
		CSG_DA 		: in std_logic_vector(31 downto 0);          
		CSG_DEN		: in std_logic;
		CSG_MODE	: in std_logic_vector(4 downto 0);
		CSG_ABORT 	: out std_logic;
		CSG_CS_LINES	: out std_logic_vector(0 to 7)
		);
	end component;

	component ArmMemInterface
	generic(
		SELECT_LINES : natural range 0 to 2 := 1
	       );
	port(
		RAM_CLK 	: in std_logic;
		RAM_RST 	: in std_logic;
		IDE 		: in std_logic;
		IA 		: in std_logic_vector(31 downto 2);
		ID 		: out std_logic_vector(31 downto 0);
		IABORT 		: out std_logic;
		
		DDE 		: in std_logic;
		DnRW 		: in std_logic;
		DMAS 		: in std_logic_vector(1 downto 0);
		DA 		: in std_logic_vector(31 downto 0);
		DDIN 		: in std_logic_vector(31 downto 0);          
		DDOUT 		: out std_logic_vector(31 downto 0);
		DABORT 		: out std_logic
	);
	end component;

	component ArmSystemController
	port(
		EXT_RST 	: in std_logic;
		SYS_RST 	: out std_logic;
		CTRL_DnRW 	: out std_logic;
		CTRL_DMAS 	: out std_logic_vector(1 downto 0);
		CTRL_DA 	: out std_logic_vector(31 downto 0);
		CTRL_DDIN 	: in std_logic_vector(31 downto 0);
		CTRL_DDOUT 	: out std_logic_vector(31 downto 0);
		CTRL_DABORT	: in std_logic;
		CTRL_DEN	: out std_logic;
		CTRL_LDP	: in std_logic;
		CTRL_IA		: in std_logic_vector(9 downto 2);
		CTRL_STATUS_LED : out std_logic_vector(7 downto 0);
		CTRL_WAIT	: out std_logic;
		EXT_DCM_LOCKED  : in std_logic;
		INT_CLK      : in std_logic;
		INT_INV_CLK : in std_logic
		);
	end component;

	component ArmRS232Interface
	port(
		SYS_CLK		: in std_logic;
		SYS_RST		: in std_logic;
		RS232_CS	: in std_logic;
		RS232_DnRW	: in std_logic;
		RS232_DMAS	: in std_logic_vector(1 downto 0);
		RS232_DA	: in std_logic_vector(3 downto 0);
		RS232_DDIN	: in std_logic_vector(31 downto 0);
		RS232_DDOUT	: out std_logic_vector(31 downto 0);
		RS232_DABORT	: out std_logic;
		RS232_IRQ	: out std_logic;
		RS232_RXD	: in std_logic;
		RS232_TXD	: out std_logic
	    );
	end component;

	signal SYS_CLK		: std_logic;
	signal SYS_INV_CLK	: std_logic;
	signal SYS_RST		: std_logic;
	signal SYS_RST_N : std_logic;
	signal RAM_RST		: std_logic;

	signal CTRL_STATUS	: std_logic_vector(7 downto 0);
	
    signal DBUS_CS_DUMMYREG : std_logic;
    signal DBUS_CS_SDRAM : std_logic;
    signal SDRAM_BUSY : std_logic;
    
    signal INT_CLK166 : std_logic;

    signal RST_COUNTER : std_Logic_vector(31 downto 0) := (others => '0');
begin
    INT_CLK166 <= CLK166;

	SYS_CLK <= INT_CLK;
	SYS_INV_CLK <= INT_INV_CLK;

	IBUS_WAIT <= '0';
	-- IBUS_WAIT <= '0';
--	EXT_LED(7 downto 0) <= CTRL_STATUS(7 downto 0);
--	EXT_LED(7) <= EXT_RXD;
--	EXT_LED(6) <= s_TXD;
	CORE_WAIT <= IBUS_WAIT or DBUS_WAIT;
	
--	EXT_LED(5) <= DBUS_DDOUT(0);

	RAM_RST <= SYS_RST;
	SYS_RST_N <= not SYS_RST;

--	Der Bus wird dem Prozessor immer zugewiesen sofern der 
--	Systemcontroller keine Zugriffe w�nscht.
--	DBUS_DEN wird hier nicht mehr beruecksichtigt, denn sonst hat der Bus gar keinen
--	Treiber, solange der Prozessorkern keine Speicherzugriffe wuenscht
--	Arbiter
	CORE_DBE <= not CTRL_DEN;
	IBUS_IBE <= '1';
	DBUS_DEN <= CORE_DEN or CTRL_DEN;
	


	Inst_ArmCore: ArmCore
	port map(
		CORE_CLK	=> SYS_CLK,
		CORE_INV_CLK	=> SYS_INV_CLK,
		CORE_RST	=> SYS_RST,
		CORE_WAIT	=> CORE_WAIT,
--	Instruktionsbus
		CORE_IBE	=> IBUS_IBE,
		CORE_IEN	=> IBUS_IEN,
		CORE_IA		=> IBUS_IA,
		CORE_ID		=> IBUS_ID,
		CORE_IABORT	=> IBUS_ABORT,
--	Datenbus
		CORE_DA		=> DBUS_DA,
		CORE_DDOUT	=> DBUS_DDOUT,
		CORE_DDIN	=> DBUS_DDIN,
		CORE_DMAS	=> DBUS_DMAS,
		CORE_DnRW	=> DBUS_DnRW,
		CORE_DABORT	=> DBUS_ABORT,
		CORE_DEN	=> CORE_DEN,
		CORE_DBE	=> CORE_DBE,
--	Keine Interrupts im HWPR
		CORE_FIQ	=> '0',
		CORE_IRQ	=> '0',
		CORE_IMODE	=> IBUS_MODE,
		CORE_DMODE	=> DBUS_MODE
	);


	Inst_ArmChipSelectGenerator: ArmChipSelectGenerator 
	port map(
		CSG_DA 		=> DBUS_DA,
		CSG_DEN		=> DBUS_DEN,
		CSG_MODE	=> DBUS_MODE,
		CSG_ABORT 	=> DBUS_ABORT,
		CSG_CS_LINES	=> CSG_CS_LINES
	);

	DBUS_CS_MEM	<= CSG_CS_LINES(0);
	DBUS_CS_RS232	<= CSG_CS_LINES(1);
	DBUS_CS_DUMMYREG <= CSG_CS_LINES(2);
	DBUS_CS_SDRAM <= CSG_CS_LINES(3);

	Inst_ArmMemInterface: ArmMemInterface
	generic map(
		   SELECT_LINES => SELECT_LINES
		   )
	port map(
		RAM_CLK 	=> INT_INV_CLK,
		RAM_RST 	=> RAM_RST,
		IDE 		=> IBUS_IEN,
		IA 		=> IBUS_IA,
		ID 		=> IBUS_ID,
		IABORT 		=> IBUS_ABORT,
		
		DDE 		=> DBUS_CS_MEM,
		DnRW 		=> DBUS_DnRW,
		DMAS 		=> DBUS_DMAS,
		DA 		=> DBUS_DA,
		DDIN 		=> DBUS_DDOUT,
		DDOUT 		=> DBUS_DDIN,
		DABORT 		=> DBUS_ABORT
	);

	Debouncing_EXT_LDP : ArmSwitchDebounce
	port map(
		SYS_RST 	=> SYS_RST,
		SYS_CLK 	=> INT_CLK,
		SDB_ASYNC_INPUT => EXT_LDP,
		SDB_SYNC_OUTPUT => INT_LDP
	);

	Inst_ArmSystemController: ArmSystemController
	port map(
		EXT_RST 	=> EXT_RST,
		SYS_RST 	=> SYS_RST,
		CTRL_DnRW 	=> DBUS_DnRW,
		CTRL_DMAS	=> DBUS_DMAS,
		CTRL_DA 	=> DBUS_DA,
		CTRL_DDIN 	=> DBUS_DDIN,
		CTRL_DDOUT 	=> DBUS_DDOUT,
		CTRL_DABORT	=> DBUS_ABORT,
		CTRL_DEN	=> CTRL_DEN,
		CTRL_LDP	=> INT_LDP,
		CTRL_IA		=> IBUS_IA(9 downto 2),
		CTRL_STATUS_LED => CTRL_STATUS,
		CTRL_WAIT	=> open,
		EXT_DCM_LOCKED   => DCM_LOCKED,
		INT_CLK      => INT_CLK,
		INT_INV_CLK  => INT_INV_CLK
	);

	Inst_ArmRS232Interface : ArmRS232Interface port map(
		SYS_CLK 	=> SYS_CLK,
		SYS_RST 	=> SYS_RST,
		RS232_CS 	=> DBUS_CS_RS232,
		RS232_DnRW 	=> DBUS_DnRW,
		RS232_DMAS	=> DBUS_DMAS,
		RS232_DA	=> DBUS_DA(3 downto 0),
		RS232_DDIN	=> DBUS_DDOUT,
		RS232_DDOUT	=> DBUS_DDIN,
		RS232_DABORT	=> DBUS_ABORT,
		RS232_IRQ	=> open,
		RS232_RXD	=> EXT_RXD,
		RS232_TXD	=> s_TXD
	);
	
	inst_dummyreg : entity work.DummyRegister port map (
	   SYS_CLK => SYS_CLK,
	   SYS_RST => SYS_RST,
	   CS => DBUS_CS_DUMMYREG,
	   DnRW => DBUS_DnRW,
	   DMAS => DBUS_DMAS,
	   DA => DBUS_DA,
	   DDIN => DBUS_DDOUT,
	   DDOUT => DBUS_DDIN,
	   DABORT => DBUS_ABORT,
	   IRQ => open
	);
        
    inst_ArmSdramInterface : entity work.ArmSdramInterface generic map (
        CACHE_ENABLE => CACHE_ENABLE 
    ) port map
    (
        SYS_CLK => SYS_CLK,
        SYS_RST => EXT_RST,
        CS => DBUS_CS_SDRAM,
        nRW => DBUS_DnRW,
        DMAS => DBUS_DMAS,
        ADDR => DBUS_DA,
        DIN => DBUS_DDOUT,
        DOUT => DBUS_DDIN,
        DABORT => DBUS_ABORT,
        IRQ => open,
        BUSY => SDRAM_BUSY,
        
        sys_clk166 => INT_CLK166,
        ref_clk200 => SDRAM_CLK,
        -- hardware interface
        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n,
        ddr3_cke => ddr3_cke,
        ddr3_cs_n => ddr3_cs_n,
        ddr3_dm => ddr3_dm,
        ddr3_odt => ddr3_odt
    );

--	Im HWPR wird das Wartesignal nicht verwendet
	-- DBUS_WAIT <= '0';
    DBUS_WAIT	<= SDRAM_BUSY when SYS_RST = '0' else '0';
	
--    process(INT_CLK)
--    begin
--        if rising_edge(INT_CLK) then
--            if unsigned(rst_counter) > 0 then
--                rst_counter <= std_logic_vector(unsigned(rst_counter) - 1);
--            end if;
--            if EXT_RST = '1' then
--                RST_COUNTER <= x"2000_0000";
--            end if;
--        end if;
--    end process;

--------------------------------------------------------------------------------
--	Testsignale der Verhaltenssimulation
--------------------------------------------------------------------------------
-- synthesis translate_off
	AGP_I_ADDRESS	<= IBUS_IA & "00";
	AGP_D_ADDRESS	<= DBUS_DA;
	AGP_CS_MEM	<= DBUS_CS_MEM;
	AGP_CS_RS232	<= DBUS_CS_RS232;
	AGP_IBUS_IBE	<= IBUS_IBE;
	AGP_DBUS_DBE	<= CORE_DBE;
-- synthesis translate_on
--------------------------------------------------------------------------------

    EXT_TXD <= s_TXD;
    EXT_LED <= '1';

end BEHAVE;

