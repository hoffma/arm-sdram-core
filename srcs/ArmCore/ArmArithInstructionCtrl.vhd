--------------------------------------------------------------------------------
-- 	Teilsteuerung Arithmetisch-logischer Instruktionen im Kontrollpfad
--	des HWPR-Prozessors.
--------------------------------------------------------------------------------
--	Datum:		06.07.2010
--	Version:	1.1
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.ArmTypes.all;

entity ArmArithInstructionCtrl is
	port(
		AIC_DECODED_VECTOR	: in std_logic_vector(15 downto 0);
		AIC_INSTRUCTION		: in std_logic_vector(31 downto 0);
		AIC_IF_IAR_INC		: out std_logic;
		AIC_ID_R_PORT_A_ADDR	: out std_logic_vector(3 downto 0);
		AIC_ID_R_PORT_B_ADDR	: out std_logic_vector(3 downto 0);
		AIC_ID_R_PORT_C_ADDR	: out std_logic_vector(3 downto 0);
		AIC_ID_REGS_USED	: out std_logic_vector(2 downto 0);
		AIC_ID_IMMEDIATE	: out std_logic_vector(31 downto 0);	
		AIC_ID_OPB_MUX_CTRL	: out std_logic;
		AIC_EX_ALU_CTRL		: out std_logic_vector(3 downto 0);
		AIC_MEM_RES_REG_EN	: out std_logic;
		AIC_MEM_CC_REG_EN	: out std_logic;
		AIC_WB_RES_REG_EN	: out std_logic;
		AIC_WB_CC_REG_EN	: out std_logic;	
		AIC_WB_W_PORT_A_ADDR	: out std_logic_vector(3 downto 0);
		AIC_WB_W_PORT_A_EN	: out std_logic;	
		AIC_WB_IAR_MUX_CTRL	: out std_logic;
		AIC_WB_IAR_LOAD		: out std_logic;
		AIC_WB_PSR_EN		: out std_logic;
		AIC_WB_PSR_SET_CC	: out std_logic;
		AIC_WB_PSR_ER		: out std_logic;
		AIC_DELAY		: out std_logic_vector(1 downto 0);
--------------------------------------------------------------------------------
--	Verwendung eines Typs aus ArmTypes weil die Codierung der Zustaende 
--	nicht vorgegeben ist.
--------------------------------------------------------------------------------
		AIC_ARM_NEXT_STATE	: out ARM_STATE_TYPE
	    );
end ArmArithInstructionCtrl;

architecture BEHAVE of ArmArithInstructionCtrl is
	alias ALU_CTRL : std_logic_vector(3 downto 0) is AIC_INSTRUCTION(24 downto 21);
	alias WPA_ADDR : std_logic_vector(3 downto 0) is AIC_INSTRUCTION(15 downto 12);
begin
	
	SET_ARITH_SIGNALS : process(AIC_INSTRUCTION, AIC_DECODED_VECTOR, ALU_CTRL, WPA_ADDR)is
		variable V_WPA_EN	: std_logic := '0';
	begin
		AIC_WB_PSR_ER		<= '0';
		AIC_WB_PSR_EN		<= '0';
		AIC_WB_PSR_SET_CC	<= '0';
		AIC_MEM_CC_REG_EN	<= '0';
		AIC_WB_CC_REG_EN	<= '0';
		AIC_WB_IAR_MUX_CTRL	<= '0';
--	Operand A Leseadresse entspricht immer demselben Teilvektor		
		AIC_ID_R_PORT_A_ADDR	<= AIC_INSTRUCTION(19 downto 16);
--------------------------------------------------------------------------------
--	Auch die anderen Registeradressen sind immer an der gleichen
--	Stelle codiert.		
--------------------------------------------------------------------------------
		AIC_ID_R_PORT_C_ADDR	<= AIC_INSTRUCTION(11 downto 8);
		AIC_ID_R_PORT_B_ADDR	<= AIC_INSTRUCTION(3 downto 0);
--	Die ALU kann immer unmittelbar mit Opcode gesteuert werden.		
		AIC_EX_ALU_CTRL		<= AIC_INSTRUCTION(24 downto 21);
	
--------------------------------------------------------------------------------
--	Direktoperand permanent aus dem niederwertigen Instruktionsbyte
--	erzeugen, wird nur wenn benoetigt im Datenpfad verarbeitet und muss auch
--	nur dann korrekt sein.		
--------------------------------------------------------------------------------
		AIC_ID_IMMEDIATE 	<= X"000000" & AIC_INSTRUCTION(7 downto 0);

--------------------------------------------------------------------------------
--	Die Menge der benoetigten Registeroperanden haengt von der 
--	vorliegenden Instruktionsgruppe ab.		
--	Nur fuer die erste Gruppe ist Operand B ein Direktoperand.	
--------------------------------------------------------------------------------
		if AIC_DECODED_VECTOR = CD_ARITH_IMMEDIATE then 
			AIC_ID_OPB_MUX_CTRL	<= '1';	
			AIC_ID_REGS_USED	<= "001";			
		elsif AIC_DECODED_VECTOR = CD_ARITH_REGISTER then
			AIC_ID_OPB_MUX_CTRL	<= '0';	
			AIC_ID_REGS_USED	<= "011";			
		else
			AIC_ID_OPB_MUX_CTRL	<= '0';
			AIC_ID_REGS_USED	<= "111";			
		end if;

--------------------------------------------------------------------------------
--	Die vier Test-Opcodes sollten als "Ziel" Register 0 anzeigen,
--	insbesondere duerfen sie nicht R15 als Ziel enthalten und damit einen
--	Sprung verursachen, sie Schreiben niemals in den Registerspeicher.
--------------------------------------------------------------------------------
		if (ALU_CTRL = OP_TST) or (ALU_CTRL = OP_TEQ) or (ALU_CTRL = OP_CMP) or (ALU_CTRL = OP_CMN) then
			V_WPA_EN		:= '0';
			AIC_MEM_RES_REG_EN	<= '0';
			AIC_WB_RES_REG_EN	<= '0';

		else			
--------------------------------------------------------------------------------
--	Jede andere Instruktion schreibt das Ergebnis zurueck.			
--	Im Fall eines Sprungs kann WPA_EN gesetzt bleiben.
--------------------------------------------------------------------------------
			V_WPA_EN		:= '1';
			AIC_MEM_RES_REG_EN	<= '1';
			AIC_WB_RES_REG_EN	<= '1';
		end if;

--------------------------------------------------------------------------------
--	Default-Werte fuer einige noch nicht behandelte Steuersignale.
--	Wenn kein Sprung stattfindet, wird kein Wartetakt nach der Instruktion
--	eingelegt, die Instruktionsadresse inkrementiert und im naechsten
--	Takt die naechste Instruktion decodiert.	
--------------------------------------------------------------------------------
		AIC_DELAY		<= "00";
		AIC_ARM_NEXT_STATE	<= STATE_DECODE;
		AIC_IF_IAR_INC		<= '1';
		AIC_WB_IAR_LOAD		<= '0';

--	Spezielle Wirkungen Arithmetisch-logischer Befehle	
		if AIC_INSTRUCTION(20) = '1' then
--	S Bit gesetzt, PSR wird in irgendeiner Form manipuliert.
			AIC_WB_PSR_EN	<= '1';					
			if WPA_ADDR = "1111" and V_WPA_EN = '1' then
--------------------------------------------------------------------------------
--	Ruecksprung aus Ausnahmebehandlung, wie jeder Sprung verursacht das ein
--	Leeren der Pipeline. 
--	Keine Instruktion mit Opcode 10-- (weil diese Rd = PC nicht zulassen).
--------------------------------------------------------------------------------
				AIC_WB_PSR_ER		<= '1';
				AIC_WB_IAR_MUX_CTRL	<= '0';
				AIC_WB_IAR_LOAD		<= '1';			
				AIC_IF_IAR_INC		<= '0';
				AIC_ARM_NEXT_STATE	<= STATE_WAIT_TO_FETCH;
				AIC_DELAY		<= "10";
			else
--	Kein Sprung -> nur Condition Code aktualisieren.
				AIC_MEM_CC_REG_EN	<= '1';
				AIC_WB_CC_REG_EN	<= '1';
				AIC_WB_PSR_SET_CC	<= '1';	
			end if;
		else
--	S-Bit nicht gesetzt			
			if WPA_ADDR = "1111" then
--	Sprung, aber kein Moduswechsel.
				AIC_WB_IAR_MUX_CTRL	<= '0';
				AIC_WB_IAR_LOAD		<= '1';			
				AIC_IF_IAR_INC		<= '0';
				AIC_ARM_NEXT_STATE	<= STATE_WAIT_TO_FETCH;
				AIC_DELAY		<= "10";
			else
--	Kein Sprung, nur ggf. in den Registerspeicher schreiben.				
				AIC_ARM_NEXT_STATE	<= STATE_DECODE;
				AIC_DELAY		<= "00";
				AIC_IF_IAR_INC		<= '1';
			end if;
		end if;
		AIC_WB_W_PORT_A_EN <= V_WPA_EN;
		AIC_WB_W_PORT_A_ADDR <= WPA_ADDR;
	end process SET_ARITH_SIGNALS;
end BEHAVE;
