--------------------------------------------------------------------------------
--	Steuerung der 5 Bypassmultiplexer der Execute-Stufe, zeigt einen
--	Load-Use-Konflikt an, wenn Bypaesse zur Loesung des Konflikts
--	nicht geeignet sind.
--------------------------------------------------------------------------------
--	Datum:		22.06.2010
--	Version:	1.00
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ArmBypassCtrl is
--------------------------------------------------------------------------------
--	Bedeutung von INST0, INST1, INST2: drei gleichzeitig in der Pipeline
--	befindliche Instruktionen.
--	INST0: Instruktion in der Instruction Decode-Stufe
--	INST1: Instruktion in der Execute-Stufe
--     	INST2: Instruktion in der Memory-Stufe	
--------------------------------------------------------------------------------
	port(	
--	Leseadressen der drei zu lesenden Operanden der ID-Stufe (INST0)	
		ABC_INST0_R_PORT_A_ADDR	: in std_logic_vector(4 downto 0);
		ABC_INST0_R_PORT_B_ADDR	: in std_logic_vector(4 downto 0);
		ABC_INST0_R_PORT_C_ADDR	: in std_logic_vector(4 downto 0);
--	Register-Schreibadressen der unmittelbar vorhergehenden Instruktion
		ABC_INST1_W_PORT_A_ADDR : in std_logic_vector(4 downto 0);
		ABC_INST1_W_PORT_B_ADDR : in std_logic_vector(4 downto 0);
--	Register-Schreibadressen der vor INST1 decodierten Instruktion (INST2)
		ABC_INST2_W_PORT_A_ADDR : in std_logic_vector(4 downto 0);
		ABC_INST2_W_PORT_B_ADDR : in std_logic_vector(4 downto 0);
--	Registerspeicher-Write-Enable-Signale der vorhergehenden Instruktion (INST1)
		ABC_INST1_W_PORT_A_EN	: in std_logic;
		ABC_INST1_W_PORT_B_EN	: in std_logic;
--	Registerspeicher-Write-Enable-Signale der vorletzten Instruktion (INST2)
		ABC_INST2_W_PORT_A_EN	: in std_logic;
		ABC_INST2_W_PORT_B_EN	: in std_logic;
--	Steuersignale fuer Statusregisterzugriffe von Instruktion INST1
		ABC_INST1_WB_PSR_EN	: in std_logic;
		ABC_INST1_WB_PSR_SET_CC : in std_logic;
--	Steuersignale fuer Statusregisterzugriffe von Instruktion INST2
		ABC_INST2_WB_PSR_EN	: in std_logic;
		ABC_INST2_WB_PSR_SET_CC : in std_logic;
--------------------------------------------------------------------------------
--	Informationen darueber, welche Registeroperanden (C|B|A) INST0 wirklich
--	verwendet.
--------------------------------------------------------------------------------
		ABC_INST0_REGS_USED	: in std_logic_vector(2 downto 0); --CBA
--------------------------------------------------------------------------------
--	Angabe, ob die Operationsweite des Shifters aus einem Register gelesen
--	wird.	
--------------------------------------------------------------------------------
		ABC_INST0_SHIFT_REG_USED : in std_logic;
--------------------------------------------------------------------------------
--	Erzeugte Steuersignale der Bypassmultiplexer sowie Anzeige eines
--	nicht durch Bypaesse zu loesenden Load-Use-Konflikts
--------------------------------------------------------------------------------
		ABC_INST0_OPA_BYPASS_MUX_CTRL	: out std_logic_vector(1 downto 0);
		ABC_INST0_OPB_BYPASS_MUX_CTRL	: out std_logic_vector(1 downto 0);
		ABC_INST0_OPC_BYPASS_MUX_CTRL	: out std_logic_vector(1 downto 0);
		ABC_INST0_SHIFT_BYPASS_MUX_CTRL	: out std_logic_vector(1 downto 0);
		ABC_INST0_CC_BYPASS_MUX_CTRL	: out std_logic_vector(1 downto 0);
		ABC_LOAD_USE_CONFLICT 		: out std_logic
	);
end ArmBypassCtrl;

architecture BEHAVE of ArmBypassCtrl is

--	Aliase fuer die Verkuerzung der Leseadressen von Instruktion 0
	alias ADDR_A0 : std_logic_vector(4 downto 0) is ABC_INST0_R_PORT_A_ADDR;
	alias ADDR_B0 : std_logic_vector(4 downto 0) is ABC_INST0_R_PORT_B_ADDR;
	alias ADDR_C0 : std_logic_vector(4 downto 0) is ABC_INST0_R_PORT_C_ADDR;

--	Aliase fuer die Verkuerzung der Schreibadressen von Instruktion 1
	alias ADDR_A1 : std_logic_vector(4 downto 0) is ABC_INST1_W_PORT_A_ADDR;
	alias ADDR_B1 : std_logic_vector(4 downto 0) is ABC_INST1_W_PORT_B_ADDR;

--	Aliase fuer die Verkuerzung der Schreibadressen von Instruktion 2
	alias ADDR_A2 : std_logic_vector(4 downto 0) is ABC_INST2_W_PORT_A_ADDR;
	alias ADDR_B2 : std_logic_vector(4 downto 0) is ABC_INST2_W_PORT_B_ADDR;

--	Aliase fuer beide Registerspeicher Write-Enable-Signale von Instruktion 1
	alias WEN_A1 : std_logic is ABC_INST1_W_PORT_A_EN;
	alias WEN_B1 : std_logic is ABC_INST1_W_PORT_B_EN;

--	Aliase fuer beide Registerspeicher Write-Enable-Signale von Instruktion 2
	alias WEN_A2 : std_logic is ABC_INST2_W_PORT_A_EN;
	alias WEN_B2 : std_logic is ABC_INST2_W_PORT_B_EN;

--	Aliase fuer PSR-Steuersignale von Instruktion 1
	alias PSR_EN_1 : std_logic is ABC_INST1_WB_PSR_EN;
	alias PSR_SET_CC_1 : std_logic is ABC_INST1_WB_PSR_SET_CC;

--	Aliase fuer PSR-Steuersignale von Instruktion 2
	alias PSR_EN_2 : std_logic is ABC_INST2_WB_PSR_EN;
	alias PSR_SET_CC_2 : std_logic is ABC_INST2_WB_PSR_SET_CC;

--	Vergleich von R_A_ADDR mit den 4 Write-Adressen vorheriger Instruktionen
	signal A0_equal_A1 : boolean;
	signal A0_equal_B1 : boolean;
	signal A0_equal_A2 : boolean;
	signal A0_equal_B2 : boolean;

--	Vergleich von R_B_ADDR mit den 4 Write-Adressen vorheriger Instruktionen
	signal B0_equal_A1 : boolean;
	signal B0_equal_B1 : boolean;
	signal B0_equal_A2 : boolean;
	signal B0_equal_B2 : boolean;

--	Vergleich von R_C_ADDR mit den 4 Write-Adressen vorheriger Instruktionen
	signal C0_equal_A1 : boolean;
	signal C0_equal_B1 : boolean;
	signal C0_equal_A2 : boolean;
	signal C0_equal_B2 : boolean;

	signal LOAD_USE_CONFLICT_A : std_logic;
	signal LOAD_USE_CONFLICT_B : std_logic;
	signal LOAD_USE_CONFLICT_C : std_logic;

	signal INST0_OPC_BYPASS_MUX_CTRL : std_logic_vector(1 downto 0);

begin
	ABC_LOAD_USE_CONFLICT <= LOAD_USE_CONFLICT_A or LOAD_USE_CONFLICT_B or LOAD_USE_CONFLICT_C;

	A0_equal_A1 <= true when ADDR_A0 = ADDR_A1 else false;
	A0_equal_B1 <= true when ADDR_A0 = ADDR_B1 else false;
	A0_equal_A2 <= true when ADDR_A0 = ADDR_A2 else false;
	A0_equal_B2 <= true when ADDR_A0 = ADDR_B2 else false;

	B0_equal_A1 <= true when ADDR_B0 = ADDR_A1 else false;
	B0_equal_B1 <= true when ADDR_B0 = ADDR_B1 else false;
	B0_equal_A2 <= true when ADDR_B0 = ADDR_A2 else false;
	B0_equal_B2 <= true when ADDR_B0 = ADDR_B2 else false;

	C0_equal_A1 <= true when ADDR_C0 = ADDR_A1 else false;
	C0_equal_B1 <= true when ADDR_C0 = ADDR_B1 else false;
	C0_equal_A2 <= true when ADDR_C0 = ADDR_A2 else false;
	C0_equal_B2 <= true when ADDR_C0 = ADDR_B2 else false;	

	SET_OPA_MUX_CTRL : process(A0_equal_A1, WEN_A1, A0_equal_B1, WEN_B1, A0_equal_A2, WEN_A2, A0_equal_B2, WEN_B2, ABC_INST0_REGS_USED)is
		variable OPA_MCTRL : std_logic_vector(1 downto 0) := "00"; 
	begin
		LOAD_USE_CONFLICT_A <= '0';
		OPA_MCTRL := "00";
		if ABC_INST0_REGS_USED(0) = '1' then
			if A0_equal_A1 and WEN_A1 = '1' then			
				OPA_MCTRL := "01";
			elsif A0_equal_B1 and WEN_B1 = '1' then			
				LOAD_USE_CONFLICT_A <= '1';
				OPA_MCTRL := "00";			
			elsif A0_equal_A2 and WEN_A2 = '1' then
				OPA_MCTRL := "10";
			elsif A0_equal_B2 and WEN_B2 = '1' then
				OPA_MCTRL := "11";
			else
				OPA_MCTRL := "00";
			end if;
		else
--------------------------------------------------------------------------------
--	Stammt das Datum nicht aus dem Registerspeicher oder wurde es im
--	Register nicht geaendert, darf kein Wert gebypasst werden.
--------------------------------------------------------------------------------
			OPA_MCTRL := "00";
		end if;
		ABC_INST0_OPA_BYPASS_MUX_CTRL <= OPA_MCTRL;
	end process SET_OPA_MUX_CTRL;


	SET_OPB_MUX_CTRL : process(B0_equal_A1, WEN_A1, B0_equal_B1, WEN_B1, B0_equal_A2, WEN_A2, B0_equal_B2, WEN_B2, ABC_INST0_REGS_USED )is
		variable OPB_MCTRL : std_logic_vector(1 downto 0) := "00"; 
	begin
		LOAD_USE_CONFLICT_B <= '0';
		OPB_MCTRL := "00";

		if ABC_INST0_REGS_USED(1) = '1' then
			if B0_equal_A1 and WEN_A1 = '1' then
				OPB_MCTRL := "01";			
			elsif B0_equal_B1 and WEN_B1 = '1' then			
				LOAD_USE_CONFLICT_B <= '1';
				OPB_MCTRL := "00";
			elsif B0_equal_A2 and WEN_A2 = '1' then
				OPB_MCTRL := "10";
			elsif B0_equal_B2 and WEN_B2 = '1' then
				OPB_MCTRL := "11";
			else
				OPB_MCTRL := "00";
			end if;
		else
			OPB_MCTRL := "00";
		end if;

		ABC_INST0_OPB_BYPASS_MUX_CTRL <= OPB_MCTRL;
	end process SET_OPB_MUX_CTRL;


	SET_OPC_MUX_CTRL : process(C0_equal_A1, WEN_A1, C0_equal_B1, WEN_B1,C0_equal_A2, WEN_A2, C0_equal_B2, WEN_B2, ABC_INST0_REGS_USED)is
		variable OPC_MCTRL : std_logic_vector(1 downto 0) := "00"; 
	begin
		LOAD_USE_CONFLICT_C <= '0';
		OPC_MCTRL := "00";	
		if ABC_INST0_REGS_USED(2) = '1' then
			if C0_equal_A1 and WEN_A1 = '1' then			
				OPC_MCTRL := "01";			
			elsif C0_equal_B1 and WEN_B1 = '1' then
				LOAD_USE_CONFLICT_C <= '1';
				OPC_MCTRL := "00";
			elsif C0_equal_A2 and WEN_A2 = '1' then
				OPC_MCTRL := "10";
			elsif C0_equal_B2 and WEN_B2 = '1' then
				OPC_MCTRL := "11";
			else
				OPC_MCTRL := "00";
			end if;
		else
			OPC_MCTRL := "00";			
		end if;	
	
		INST0_OPC_BYPASS_MUX_CTRL <= OPC_MCTRL;
	end process SET_OPC_MUX_CTRL;

		ABC_INST0_OPC_BYPASS_MUX_CTRL <= INST0_OPC_BYPASS_MUX_CTRL;

	SET_CC_MUX_CTRL : process(PSR_EN_1, PSR_EN_2, PSR_SET_CC_1, PSR_SET_CC_2)is
		variable CC_MCTRL : std_logic_vector(1 downto 0) := "00"; 
	begin
		CC_MCTRL := "00";	
		if(PSR_EN_1 = '1' and PSR_SET_CC_1 = '1')then
			CC_MCTRL := "01";			
		elsif(PSR_EN_2 = '1' and PSR_SET_CC_2 = '1')then	
			CC_MCTRL := "10";
		else
			CC_MCTRL := "00";
		end if;	
		ABC_INST0_CC_BYPASS_MUX_CTRL <= CC_MCTRL;
	end process SET_CC_MUX_CTRL;

--------------------------------------------------------------------------------
--	Steuersignale des Shift-Multiplexers koennen weitgehend von den bereits
--	erzeugten Steuersignalen des OPC-Multiplexers abgeleitet werden.
--------------------------------------------------------------------------------
	SET_SHIFT_MUX_CTRL : process(ABC_INST0_SHIFT_REG_USED, INST0_OPC_BYPASS_MUX_CTRL)is
	begin
		if ABC_INST0_SHIFT_REG_USED = '1' then
--------------------------------------------------------------------------------
--	Die Schiebeweite entspricht Byte 0 von Operand C, daher muss der Shift-
--	Multiplexer so gesteuert werden wie EX_OPC_MUX.
--	Die Erkennung von Load-Use-Konflikten geschieht ebenfalls in der
--	Schaltung fuer Operand C.			
--------------------------------------------------------------------------------
			ABC_INST0_SHIFT_BYPASS_MUX_CTRL <= INST0_OPC_BYPASS_MUX_CTRL;
		else
--------------------------------------------------------------------------------
--	Die Schiebeweite wird direkt aus der Instruktion uebernommen, neu
--	codiert und in den Datenpfad eingebracht, Bypaesse werden nicht
--	geschaltet.
--------------------------------------------------------------------------------
			ABC_INST0_SHIFT_BYPASS_MUX_CTRL <= "00";
		end if;
	end process SET_SHIFT_MUX_CTRL;
end BEHAVE;
