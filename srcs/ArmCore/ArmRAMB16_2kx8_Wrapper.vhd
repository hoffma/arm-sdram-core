--------------------------------------------------------------------------------
--	Wrapper um einen Spartan3E-Blockram fuer den RAM des ARM-SoC
--------------------------------------------------------------------------------
--	Datum:		14.05.2010
--	Version:	1.00
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ArmRAMB16_2kx8_Wrapper is
    port ( RAM_CLK	: in  std_logic;
           RAM_RST	: in  std_logic;
           WEA		: in  std_logic;
           ENA		: in  std_logic;
           ADDRA	: in  std_logic_vector (10 downto 0);
           DIA		: in  std_logic_vector (7 downto 0);
           WEB		: in  std_logic;
           ENB		: in  std_logic;
           ADDRB	: in  std_logic_vector (10 downto 0);
           DIB		: in  std_logic_vector (7 downto 0);
           DOA		: out  std_logic_vector (7 downto 0);
           DOB		: out  std_logic_vector (7 downto 0));
end ArmRAMB16_2kx8_Wrapper;

architecture BEHAVE of ArmRAMB16_2kx8_Wrapper is

  component BRAM is
  port (
    BRAM_PORTA_en : in STD_LOGIC;
    BRAM_PORTA_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTA_we : in STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTA_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTA_clk : in STD_LOGIC;
    BRAM_PORTB_en : in STD_LOGIC;
    BRAM_PORTB_dout : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_din : in STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_we : in STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTB_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    BRAM_PORTB_clk : in STD_LOGIC
  );
  end component BRAM;
  
begin



 
   BRAM_i: component BRAM
      port map (
        BRAM_PORTA_addr(10 downto 0) => ADDRA,
        BRAM_PORTA_clk => RAM_CLK,
        BRAM_PORTA_din(7 downto 0) => DIA,
        BRAM_PORTA_dout(7 downto 0) => DOA,
        BRAM_PORTA_en => ENA,
        BRAM_PORTA_we(0) => WEA,
        BRAM_PORTB_addr(10 downto 0) => ADDRB,
        BRAM_PORTB_clk => RAM_CLK,
        BRAM_PORTB_din(7 downto 0) => DIB,
        BRAM_PORTB_dout(7 downto 0) => DOB,
        BRAM_PORTB_en => ENB,
        BRAM_PORTB_we(0) => WEB
      );
 
--   port map (
--      DOA => DOA,      -- Port A 8-bit Data Output
--      DOB => DOB,      -- Port B 8-bit Data Output
--      DOPA => open,    -- Port A 1-bit Parity Output
--      DOPB => open,    -- Port B 1-bit Parity Output
--      ADDRA => ADDRA,  -- Port A 11-bit Address Input
--      ADDRB => ADDRB,  -- Port B 11-bit Address Input
--      CLKA => RAM_CLK,    -- Port A Clock
--      CLKB => RAM_CLK,   -- Port B Clock
--      DIA => DIA,      -- Port A 8-bit Data Input
--      DIB => DIB,      -- Port B 8-bit Data Input
--      DIPA => "0",    -- Port A 1-bit parity Input
--      DIPB => "0",    -- Port-B 1-bit parity Input
--      ENA => ENA,      -- Port A RAM Enable Input
--      ENB => ENB,      -- PortB RAM Enable Input
--      SSRA => RAM_RST,    -- Port A Synchronous Set/Reset Input
--      SSRB => RAM_RST,    -- Port B Synchronous Set/Reset Input
--      WEA => WEA,      -- Port A Write Enable Input
--      WEB => WEB       -- Port B Write Enable Input
--   );

   -- End of RAMB16_S9_S9_inst instantiation
end BEHAVE;

