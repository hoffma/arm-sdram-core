--------------------------------------------------------------------------------
--	Prioritaetsencoder fuer das Finden des niederwertigsten
-- 	gesetzten Bits in einem 16 Bit Vektor.
-- 	In der architecture sind zwei Varianten beschrieben, eine stark
-- 	strukturorientierte fuer den Sparta-3E und eine hochsprachliche
-- 	Variante. Die Auswahl erfolgt durch eine Konstante in 
-- 	ArmConfiguration.vhd.
--------------------------------------------------------------------------------
--	Datum:		13.06.2010
--	Version:	1.01
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library work;
use work.ArmConfiguration.USE_STRUCTURAL_PRIORITY_ENCODER;

entity ArmPriorityVectorFilter is
	port(
		PVF_VECTOR_UNFILTERED	: in std_logic_vector(15 downto 0);
		PVF_VECTOR_FILTERED	: out std_logic_vector(15 downto 0)
	    );
end ArmPriorityVectorFilter;

--------------------------------------------------------------------------------
--	Implementierung: Prioritaetsencoder mit look-ahead-Struktur
-- 	und Funktionen von 4 Variablen fuer die LUTs des Spartan-3E.
--------------------------------------------------------------------------------
architecture STRUCTURE of ArmPriorityVectorFilter is
begin
GEN_STRUCTURAL_ENCODER : if USE_STRUCTURAL_PRIORITY_ENCODER generate
begin
	process(PVF_VECTOR_UNFILTERED) is
		variable OR_VECTOR : std_logic_vector(0 to 3) := (others => '0');
		variable UF : std_logic_vector(15 downto 0) := (others => '0'); 
		variable FI : std_logic_vector(15 downto 0) := (others => '0'); 

	begin
		UF := PVF_VECTOR_UNFILTERED;
--	Oder-Gatter als Ahead-Struktur	       	
		OR_VECTOR(0) := UF(0) or UF(1) or UF(2) or UF(3);
		OR_VECTOR(1) := OR_VECTOR(0) or UF(4) or UF(5) or UF(6);
		OR_VECTOR(2) := OR_VECTOR(1) or UF(7) or UF(8) or UF(9);
		OR_VECTOR(3) := OR_VECTOR(2) or UF(10) or UF(11) or UF(12);
--	die erste Leitung und die ersten 3 Filtergatter
		FI(0) := UF(0);
		FI(1) := UF(1) and not UF(0);
		FI(2) := UF(2) and not UF(0) and not UF(1);
		FI(3) := UF(3) and not UF(0) and not UF(1) and not UF(2);
--	4 Strukturen mit jeweils 3 Gattern 	
		for i in 1 to 4 loop
			for j in 1 to 3 loop
				case j is
					when 1 =>
						FI((i*3) + 1) := UF((i*3) + 1) and not OR_VECTOR(i - 1); 
					when 2 =>
						FI((i*3) + 2) := UF((i*3) + 2) and not OR_VECTOR(i - 1) and not UF((i*3) + 1); 
					when others =>
						FI((i*3) + 3) := UF((i*3) + 3) and not OR_VECTOR(i - 1) and not UF((i*3) + 1) and not UF((i*3) + 2);
				end case;	       	
			end loop;
		end loop;
		PVF_VECTOR_FILTERED <= FI;
	end process;
end generate GEN_STRUCTURAL_ENCODER;

GEN_HILEVEL_ENCODER : if not USE_STRUCTURAL_PRIORITY_ENCODER generate
begin
--------------------------------------------------------------------------------
--	Alternative Implementierung, eine Schleife ueber dem Vektor.
--	Der Code ist synthetisierbar und liefert ein aehnlich gutes Resultat.
--------------------------------------------------------------------------------
	process(PVF_VECTOR_UNFILTERED)is
		alias UF : std_logic_vector(15 downto 0) is PVF_VECTOR_UNFILTERED; 
		alias FI : std_logic_vector(15 downto 0) is PVF_VECTOR_FILTERED; 
	begin
		FI(0) <= UF(0);
		for i in 1 to 15 loop
			FI(i) <= UF(i);
			for j in 0 to i - 1 loop
				if(UF(j) = '1')then
					FI(i) <= '0';
				end if;
			end loop;

		end loop;
	end process;
end generate GEN_HILEVEL_ENCODER;

end STRUCTURE;

