--------------------------------------------------------------------------------
--	Multiplizierer fuer den Datenpfad des ARM-SoC
--	Einzige Operation OP1 x OP2 => RES; 32Bit x 32Bit => 32Bit(!) 
--------------------------------------------------------------------------------
--	Datum:		15.10.09
--	Version:	0.60
--------------------------------------------------------------------------------
--	Aenderungen:
--	Neben der urspruenglichen Version die sich voellig auf die Abbildung
--	der VHDL-Multiplikation verlassen hat ist eine alternative Fassung
--	hinzugekommen. Letztere verknueft explizit 3 18Bit-Multiplizierer und 
--	addierte ihre Ergebnisse zu einem gemeinsamen 32Bit Resultat. Mit einem
--	weiteren Multiplizierer einem breiteren Addierer koennte die Schaltung 
--	ein echtes 64Bit Ergebnis produzieren.
--	Die Strukturelle Schaltung kann zusaetzlich den Ausgang der 
--	Multiplizierer registrieren um den kritischen Pfad des Prozessorkerns
--	zu verk�rzen, dann muss die Steuerung zwei Takte fuer eine 
--	Multiplikation vorsehen (faktisch ist die Pipeline fuer 
--	Multiplikationen dann 6 Stufen tief).
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.ArmTypes.all;


Library UNISIM;
use UNISIM.vcomponents.all;


entity ArmMultiplier is
	generic(
		USE_STRUCTURAL	: boolean := false;
		USE_PREG 	: natural range 0 to 1 := 0
	);
	Port (
		MUL_CLK		: in std_logic;
    		MUL_RST		: in std_logic;
    		MUL_EN		: in std_logic;
		MUL_OP1 	: in  STD_LOGIC_VECTOR (31 downto 0);	-- Rm
		MUL_OP2 	: in  STD_LOGIC_VECTOR (31 downto 0);	-- Rs
		MUL_RES 	: out  STD_LOGIC_VECTOR (31 downto 0)	-- Rd bzw. RdLo         	
	);
end ArmMultiplier;

architecture BEHAVE of ArmMultiplier is
	signal res_unsigned_mul_64 : unsigned(63 downto 0);
--	signal res_unsigned_mul_64 : unsigned(31 downto 0);	

begin



--	res_unsigned_mul_64 	<= unsigned(MUL_OP1(15 downto 0)) * unsigned(MUL_OP2(15 downto 0));-- when MUL_USE_64BIT else (unsigned(MUL_OP1) * unsign	ed(MUL_OP2))(31 downto 0);

	GEN_BEHAVIORAL : if not USE_STRUCTURAL generate
	begin
		res_unsigned_mul_64 	<= unsigned(MUL_OP1) * unsigned(MUL_OP2);-- when MUL_USE_64BIT else (unsigned(MUL_OP1) * unsigned(MUL_OP2))(31 downto 0);
--------------------------------------------------------------------------------
		GEN_RES_REG : if USE_PREG = 1 generate
			signal RES_REG : std_logic_vector(31 downto 0);
		begin
			REGISTER_OUTPUT : process(MUL_CLK)is
			begin
				if MUL_CLK'event and MUL_CLK = '1' then
					if MUL_RST = '1' then
						RES_REG <= (others => '0');
					else
						if MUL_EN = '1' then
							RES_REG <= std_logic_vector(res_unsigned_mul_64(31 downto 0));
						else
							RES_REG <= RES_REG;
						end if;
					end if;
				end if;

			end process REGISTER_OUTPUT;

			MUL_RES <= RES_REG; 

		end generate GEN_RES_REG;
--------------------------------------------------------------------------------

		GEN_COMBINATORIAL_MULT : if USE_PREG = 0 generate
		begin
			MUL_RES <= std_logic_vector(res_unsigned_mul_64(31 downto 0));
		end generate GEN_COMBINATORIAL_MULT;

	end generate GEN_BEHAVIORAL;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

	GEN_STRUCTURAL : if USE_STRUCTURAL generate
		signal A,B,C,D			: std_logic_vector(15 downto 0);
		signal AMOD,BMOD,CMOD,DMOD	: std_logic_vector(17 downto 0);
		signal BD, BC, AD		: std_logic_vector(35 downto 0);
		signal BDMOD			: std_logic_vector(31 downto 0);
		signal BCMOD,ADMOD		: std_logic_vector(15 downto 0);
		signal B_CASCADE		: std_logic_vector(17 downto 0);
		signal BC_PLUS_AD_LO		: std_logic_vector(15 downto 0);
	begin

		A	<= MUL_OP1(31 downto 16);
		B	<= MUL_OP1(15 downto 0);
		C	<= MUL_OP2(31 downto 16);
		D	<= MUL_OP2(15 downto 0);
		AMOD	<= A(15) & A(15) & A;
		CMOD	<= C(15) & C(15) & C;
	       	BMOD	<= "00" & B;
		DMOD	<= "00" & D;

		MULT18X18SIO_BD : MULT18X18SIO
		   generic map (
		      AREG => 0,		-- Enable the input registers on the A port (1=on, 0=off)
		      BREG => 0,		-- Enable the input registers on the B port (1=on, 0=off)
		      B_INPUT => "DIRECT",	-- B cascade input "DIRECT" or "CASCADE" 
		      PREG => USE_PREG) 	-- Enable the input registers on the P port (1=on, 0=off)
		   port map (
		      BCOUT => B_CASCADE,	-- 18-bit cascade output
		      P => BD,   		-- 36-bit multiplier output
		      A => DMOD,    		-- 18-bit multiplier input
		      B => BMOD,    		-- 18-bit multiplier input
		      BCIN => (others => '-'), 	-- 18-bit cascade input
		      CEA => '-', 		-- Clock enable input for the A port
		      CEB => '-', 		-- Clock enable input for the B port
		      CEP => MUL_EN,		-- Clock enable input for the P port 
		      CLK => MUL_CLK,    		-- Clock input
		      RSTA => '-', 		-- Synchronous reset input for the A port
		      RSTB => '-', 		-- Synchronous reset input for the B port
		      RSTP => MUL_RST 		-- Synchronous reset input for the P port
		   );

		
		MULT18X18SIO_BC : MULT18X18SIO
		   generic map (
		      AREG => 0, 
		      BREG => 0, 
		      B_INPUT => "CASCADE", 
		      PREG => USE_PREG) 
		   port map (
		      BCOUT => open, 
		      P => BC,    
		      A => CMOD, 
		      B => (others => '-'),  
		      BCIN => B_CASCADE, 
		      CEA => '-', 
		      CEB => '-', 
		      CEP => MUL_EN, 
		      CLK => MUL_CLK,  
		      RSTA => '-',
		      RSTB => '-',
		      RSTP => MUL_RST 
		   );


		MULT18X18SIO_AD : MULT18X18SIO
		   generic map (
		      AREG => 0, 
		      BREG => 0, 
		      B_INPUT => "DIRECT", 
		      PREG => USE_PREG) 
		   port map (
		      BCOUT => open, 
		      P => AD,    
		      A => AMOD,    
		      B => DMOD,    
		      BCIN => (others => '-'), 
		      CEA => '-', 
		      CEB => '-', 
		      CEP => MUL_EN, 
		      CLK => MUL_CLK, 
		      RSTA => '-', 
		      RSTB => '-', 
		      RSTP => MUL_RST 
		   );

		   ADMOD		<= AD(15 downto 0);
		   BDMOD		<= BD(31 downto 0);
		   BCMOD		<= BC(15 downto 0);
		   BC_PLUS_AD_LO	<= std_logic_vector(unsigned(BCMOD) + unsigned(ADMOD));
		   MUL_RES(15 downto 0)	<= BDMOD(15 downto 0);
		   MUL_RES(31 downto 16)<= std_logic_vector(unsigned(BDMOD(31 downto 16)) + unsigned(BC_PLUS_AD_LO));
	end generate;

	

end BEHAVE;	
