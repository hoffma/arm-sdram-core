library ieee;
use ieee.std_logic_1164.all;
use ieee.STD_LOGIC_ARITH.all;
use ieee.std_logic_unsigned.all;

library work;
use work.ArmTypes.all;
--------------------------------------------------------------------------------
--	Konfigurationsoption fuer den Ort der Z-Bit -Erzeugung
--------------------------------------------------------------------------------
use work.ArmConfiguration.USE_DEDICATED_ARITH_ZERO_ALU;

--------------------------------------------------------------------------------
--	Uncomment the following library declaration if instantiating
--	any Xilinx primitives in this code.
--------------------------------------------------------------------------------
library UNISIM;
use UNISIM.VComponents.all;

entity ArmALUArithPart is
	generic(
		BUS_WIDTH : natural := 32);
	Port (
		OP1	: in  std_logic_vector (BUS_WIDTH-1 downto 0);
        	OP2	: in  std_logic_vector (BUS_WIDTH-1 downto 0);
        	CTRL	: in  std_logic_vector (3 downto 0);
		C_IN	: in  std_logic;
		RES	: out std_logic_vector (BUS_WIDTH-1 downto 0);
		C_OUT	: out std_logic;
		Z_OUT	: out std_logic;
		V_OUT	: out std_logic);
end ArmALUArithPart;

architecture Behavioral of ArmALUArithPart is
	signal OP1_INTERNAL, OP2_INTERNAL : std_logic_vector(BUS_WIDTH-1 downto 0);
	signal C		: std_logic_vector(BUS_WIDTH downto 0) := (others => '0');
	signal FUNC_GENERATOR	: std_logic_vector(BUS_WIDTH-1 downto 0);
	signal RES_INTERNAL	: std_logic_vector(BUS_WIDTH-1 downto 0);
	signal Cn		: std_logic;
	signal OP_IS_ADD	: boolean;
	signal OP_IS_REVERSE	: boolean;
	signal OP_C		: std_logic_vector(BUS_WIDTH-1 downto 0);

--------------------------------------------------------------------------------
--	Attribut zur Steuerung von XST: die Signale c und RES sollen
--	explizit auf Carry-Chains abgebildet werden (wahrscheinlich 
--	ueberfluessig).
--------------------------------------------------------------------------------
--	attribute use_carry_chain: string;
--	attribute use_carry_chain of c: signal is "yes";
--	attribute use_carry_chain of RES: signal is "yes";


begin
	--eigendlich korrekte Beschreibung fuer Addition auf der Carry-Chain, reicht aber nicht
--	c(0) <= '0';
--	XORS <= OP1 xor OP2;
--	RES(0) <= XORS(0);
--	c(0) <= OP1(0) when XORS(0) = '0' else '0';
--	process(OP1,OP2,XORS,c)is
--	begin 	
--		for i in 1 to 7 loop
--			RES(i) <= XORS(i) xor c(i);
--			if XORS(i) = '1' then
--				c(i+1) <= c(i);
--			else
--				c(i+1) <= OP1(i); 
--			end if;
--		end loop;
--		
--	end process;

--	33 Zeilen

--	Hilfssignale zur Feststellung ob eine Verknuepung eine Addition
--	oder eine inverse (Subtraktion) ist
	OP_IS_ADD <= (CTRL = OP_ADD) or (CTRL = OP_ADC) or (CTRL = OP_CMN);-- or (CTRL = OP_EOR);
	OP_IS_REVERSE <= (CTRL = OP_RSB) or (CTRL = OP_RSC);

--	Weil OP1 und OP2 xor und xnor verknuepft werden, ist ihre Reihenfolge egal, allerdings
--	haengt der 0-Eingang der MUXCY von der konkreten Operation an
	OP1_INTERNAL <= OP1;--OP2 when OP_IS_REVERSE else OP1; 
	OP2_INTERNAL <= OP2;--OP1 when OP_IS_REVERSE else OP2; 
	OP_C <= OP2 when OP_IS_REVERSE else OP1;

	SET_C0 : process(CTRL,C_IN)is
	begin
--		case CTRL is		
--			when OP_SUB | OP_RSB | OP_CMP =>
--	gewoehnliche Subtraktionen verwenden als C0 eine 1, da das Carry in der Carry Chain
--	stets invertiert verwendet wird und '1' hier fuer "kein Carry" steht
--				C(0) <= '1';
--			when OP_ADD | OP_CMN =>
--	gewoehnliche Additionen verwenden 0 als Carryeingang, Additionen fuehren das nicht invertierte Uebertragssignal				
--				C(0) <= '0';	
--			when others =>
--	Additionen mit Uebertragseingang koennen C_IN unmittelbar verwenden, fuer Subtraktionen wirkt dieses Signal als 
--	Inverses Carry, was aber auch der Spezifikation entspricht				
--				C(0) <= C_IN;
--		end case;

--		Eine Prioritaetsschaltung mit moeglichst kurzer Laufzeit fuer C_IN sollte von Vorteil sein,
--		fuehrt faktisch aber in zeitlicher und raeumlicher Komplexitaet zum gleichen Ergebnis wie das Case,
--		denn auch dieses wird mit der gleichen priorisierung fuer C_IN synthetisiert
		if (CTRL = OP_ADC) or (CTRL = OP_SBC) or (CTRL = OP_RSC) then
			C(0) <= C_IN;
		else
			if(CTRL = OP_ADD) or (CTRL = OP_CMN) then
				C(0) <= '0';
			else
				C(0) <= '1';
			end if;
		end if;
	end process SET_C0;


	FUNC_GENERATOR <= (OP1_INTERNAL xor OP2_INTERNAL) when OP_IS_ADD else (OP1_INTERNAL xnor OP2_INTERNAL);

	GEN_LOOP : for i in 0 to BUS_WIDTH-1 generate
	begin
		GEN_C0 : if i = 0 generate
			MUXCY_inst : MUXCY
			port map (
				O => C(i+1),   -- Carry output signal
				CI => C(0), -- Carry input signal
				DI => OP_C(i),--OP1_INTERNAL(i), -- Data input signal
				S => FUNC_GENERATOR(i)    -- MUX select, tie to '1' or LUT4 out
			);

			XORCY_inst : XORCY
			port map (
				O => RES_INTERNAL(i),   -- XOR output signal
				CI => C(0), -- Carry input signal
				LI => FUNC_GENERATOR(i)		      
			-- LUT4 input signal
			);
			RES(i) <= RES_INTERNAL(i);
--			Z(i) <= not RES_INTERNAL(i);

		end generate GEN_C0;

		GEN_CI : if (i > 0 and i < BUS_WIDTH-2) or (i = BUS_WIDTH-1) generate --Das vorletzte Carry muss anders beschrieben werden
			MUXCY_inst : MUXCY
			port map (
				O => C(i+1),   -- Carry output signal
				CI => C(i), -- Carry input signal
				DI => OP_C(i),--OP1_INTERNAL(i), -- Data input signal
				S => FUNC_GENERATOR(i)    -- MUX select, tie to '1' or LUT4 out
			);
			XORCY_inst : XORCY
			port map (
				O => RES_INTERNAL(i),   -- XOR output signal
				CI => C(i), -- Carry input signal
				LI => FUNC_GENERATOR(i)		      
			-- LUT4 input signal
			);
			RES(i) <= RES_INTERNAL(i);
--			Z(i) <= (not RES_INTERNAL(i)) or Z(i-1);
		end generate GEN_CI;

		GEN_Cn_MINUS_2 : if i = BUS_WIDTH-2 generate 
			MUXCY_inst : MUXCY_D
			port map (
				LO => Cn,   -- Carry output signal
				O => C(i+1),
				CI => C(i), -- Carry input signal
				DI => OP_C(i),--OP1_INTERNAL(i), -- Data input signal
				S => FUNC_GENERATOR(i)    -- MUX select, tie to '1' or LUT4 out
			);
			XORCY_inst : XORCY
			port map (
				O => RES_INTERNAL(i),   -- XOR output signal
				CI => C(i), -- Carry input signal
				LI => FUNC_GENERATOR(i)		      
			-- LUT4 input signal
			);
			RES(i) <= RES_INTERNAL(i);
--			Z(i) <= (not RES_INTERNAL(i)) or Z(i-1);
		end generate GEN_Cn_MINUS_2;	

			
	end generate GEN_LOOP;

	C_OUT <= C(BUS_WIDTH);
	--Durch die Verwendung eines lokalen dedizierten XOR-Gatters zusammen
	--mit einem lokalen und einem Globalen Ausgang der Carry-Chain-Multiplexer
	--entfallen die Routingdelays
	--Weil der globale Ausgang des vorletzten Multiplexers verwendet wird
	--ist die zusaetzliche Zeitstrafe gering 
	XORCY_inst : XORCY
			port map (
				O => V_OUT,   -- XOR output signal
				CI => c(BUS_WIDTH), -- Carry input signal
				LI => Cn		      
			-- LUT4 input signal
			);
			
	Z_OUT<= '0' when not USE_DEDICATED_ARITH_ZERO_ALU else
		'1' when RES_INTERNAL = X"00000000" else '0';--= Z(BUS_WIDTH-1);

end Behavioral;

