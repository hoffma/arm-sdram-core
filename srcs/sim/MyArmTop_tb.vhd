----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 06/30/2021 10:47:52 PM
-- Design Name: 
-- Module Name: MyArmTop_tb - Behavioral
-- Target Devices: ARMv4 Prozessor
-- Description: 
-- Simuliert den gesamten Prozessor von Herrn Böhme inklusive SDRAM Anbindung.
-- Der SDRAM benötigt ca. 125µs, bis init_complete aktiv wird. Wird
-- init_complete danach nicht aktiv, so sollte als erste Fehlerbehebung
-- versucht werden die MIG IP neu zu generieren.
-- Der zu verwendende Source Code muss in den internen BRAM des Prozessors
-- eingelesen werden als COE datei und bereits synthetisiert sein.
-- Die benötigten Taktsignale werden mit einer PLL-IP innerhalb der Instanz
-- MyArmTop generiert. Lediglich das hardwareseitige Taktsignal (100 MHz) muss
-- generiert werden.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.ArmGlobalProbes.all;

entity MyArmTop_tb is
--  Port ( );
end MyArmTop_tb;

architecture Behavioral of MyArmTop_tb is
    signal clk : std_logic := '0';
    signal rst, ldp, rxd, txd : std_logic;
    signal led : std_logic_vector(7 downto 0);
    
    -- ddr3 signals
    signal ddr3_dq : std_logic_vector(15 downto 0);
    signal ddr3_dqs_p, ddr3_dqs_n, tdqs_n : std_logic_vector(1 downto 0);
    signal ddr3_addr : std_logic_vector(13 downto 0);
    signal ddr3_ba : std_logic_vector(2 downto 0);
    signal ddr3_ras_n, ddr3_cas_n, ddr3_we_n, ddr3_reset_n : std_logic;
    signal ddr3_ck_p, ddr3_ck_n, ddr3_cke, ddr3_cs_n, ddr3_odt : std_logic_vector(0 downto 0);
    signal ddr3_dm : std_logic_vector(1 downto 0);
    
begin

    clk <= not clk after 5 ns; -- 100 MHz
    rst <= '1', '0' after 1 us;
    ldp <= '0'; -- , '0' after 2 us;
    rxd <= '0';
    
    inst_top : entity work.MyArmTop port map (
        EXT_RST => rst,
        EXT_CLK => clk,
        EXT_LDP => ldp,
        EXT_RXD => rxd,
        EXT_TXD => txd,
        EXT_LED => led,
        --ddr3 connection
        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n,
        ddr3_cke => ddr3_cke,
        ddr3_cs_n => ddr3_cs_n,
        ddr3_dm => ddr3_dm,
        ddr3_odt => ddr3_odt
    );
    
    inst_ddr : entity work.ddr3 port map(
        rst_n => ddr3_reset_n,
        ck => ddr3_ck_p,
        ck_n => ddr3_ck_n,
        cke => ddr3_cke,
        cs_n => ddr3_cs_n,
        ras_n => ddr3_ras_n,
        cas_n => ddr3_cas_n,
        we_n => ddr3_we_n,
        dm_tdqs => ddr3_dm,
        ba => ddr3_ba,
        addr => ddr3_addr,
        dq => ddr3_dq,
        dqs => ddr3_dqs_p,
        dqs_n => ddr3_dqs_n,
        odt => ddr3_odt,
        tdqs_n => tdqs_n
    );

end Behavioral;

