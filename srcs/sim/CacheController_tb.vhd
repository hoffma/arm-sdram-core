----------------------------------------------------------------------------------
-- Engineer: Mario Hoffmann
-- 
-- Create Date: 11/04/2021 09:16:03 AM
-- Design Name: 
-- Module Name: CacheController_tb - Behavioral
-- Target Devices: ARMv4 Prozessor
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use std.env.stop;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CacheController_tb is
--  Port ( );
end CacheController_tb;

architecture Behavioral of CacheController_tb is
    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out20          : out    std_logic;
      clk_out_inv20          : out    std_logic;
      clk_out200          : out    std_logic;
      clk_out166          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;

    signal clk, clk20, sys_clk166, ref_clk200 : std_logic := '0';
    signal rst, writeEn, readEn, ack : std_logic := '0';
    signal mask : std_logic_vector(15 downto 0) := (others => '0');
    signal addr : std_Logic_Vector(27 downto 0);
    signal ram_din, ram_dout : std_logic_vector(127 downto 0);
    signal cache_din, cache_dout, ram_tmp_din, ram_tmp_dout : std_logic_vector(31 downto 0);
    signal cache_ready, cache_nRW : std_Logic := '0';
    
    file f_INPUT_DATA : text;
    file f_OUTPUT_DATA : text;
    
    -- ddr3 signals
    signal ddr3_dq : std_logic_vector(15 downto 0);
    signal ddr3_dqs_p, ddr3_dqs_n, tdqs_n : std_logic_vector(1 downto 0);
    signal ddr3_addr : std_logic_vector(13 downto 0);
    signal ddr3_ba : std_logic_vector(2 downto 0);
    signal ddr3_ras_n, ddr3_cas_n, ddr3_we_n, ddr3_reset_n : std_logic;
    signal ddr3_ck_p, ddr3_ck_n, ddr3_cke, ddr3_cs_n, ddr3_odt : std_logic_vector(0 downto 0);
    signal ddr3_dm : std_logic_vector(1 downto 0);
begin
    clk <= not clk after 5 ns; -- 100 MHz
    rst <= '1', '0' after 50 ns;
    
    
    process
        variable v_iLINE : line;
        variable v_oLINE : line;
        variable v_SPACE : character;
        variable v_COUNT : integer := 0;
        variable v_ADDR : std_logic_vector(31 downto 0);
        variable v_DATA : std_logic_Vector(31 downto 0);
        variable v_oDATA : std_logic_vector(31 downto 0);
    begin
        writeEn <= '0';
        readEn <= '0';
        v_COUNT := 0;
        wait for 50 ns;
        
        file_open(f_INPUT_DATA, "/home/user/Desktop/TODO: ADD DATA.txt", read_mode);
        
        report "Writing the data to SDRAM";
        while not endfile(f_INPUT_DATA) loop
            readline(f_INPUT_DATA, v_iLINE); -- read the next line
            -- each line consists of address and data
            hread(v_iLINE, v_ADDR);
            read(v_iLINE, v_SPACE);
            hread(v_iLINE, v_DATA);
            
            writeEn <= '1';
            readEn <= '0';
            addr <= v_ADDR(27 downto 0) & "0000";
            cache_din <= v_DATA;
            
            wait until cache_ready = '1';
            
            writeEn <= '0';
        end loop;
    
        report "Simulation finished";
        stop; -- end simulation
    end process;

    inst_clk : clk_wiz_0 port map (
        clk_in1 => clk,
        reset => rst,
        
        clk_out20 => clk20,
        clk_out200 => ref_clk200,
        clk_out166 => sys_clk166
    );
    
    inst_cache : entity work.CacheController port map (
        sys_clk => clk20,
        sys_rst => rst,
        READY => cache_ready,
        EN => '1',
        ADDR => addr,
        nRW => cache_nRW,
        DIN => cache_din,
        DOUT => cache_dout,
        MEM_READY => ack,
        MEM_IN_COUT => ram_tmp_din,
        MEM_OUT_CIN => ram_tmp_dout
    );

    inst_driver : entity work.SdramDriver port map (
        sys_clk => clk20,
        sys_rst => rst,
        
        sys_clk166 => sys_clk166,
        ref_clk200 => ref_clk200,
        
        writeEn => writeEn,
        readEn => readEn,
        ack => ack,
        
        addr => addr,
        din => ram_din,
        dout => ram_dout,
        mask => mask,
        --ddr3 connection
        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n,
        ddr3_cke => ddr3_cke,
        ddr3_cs_n => ddr3_cs_n,
        ddr3_dm => ddr3_dm,
        ddr3_odt => ddr3_odt
    );

    inst_ddr : entity work.ddr3 port map(
        rst_n => ddr3_reset_n,
        ck => ddr3_ck_p,
        ck_n => ddr3_ck_n,
        cke => ddr3_cke,
        cs_n => ddr3_cs_n,
        ras_n => ddr3_ras_n,
        cas_n => ddr3_cas_n,
        we_n => ddr3_we_n,
        dm_tdqs => ddr3_dm,
        ba => ddr3_ba,
        addr => ddr3_addr(12 downto 0),
        dq => ddr3_dq,
        dqs => ddr3_dqs_p,
        dqs_n => ddr3_dqs_n,
        odt => ddr3_odt,
        tdqs_n => tdqs_n
    );

end Behavioral;
