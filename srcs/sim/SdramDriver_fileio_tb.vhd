----------------------------------------------------------------------------------
-- Engineer:  Mario Hoffmann
-- 
-- Create Date: 11/02/2021 12:26:45 AM
-- Design Name: 
-- Module Name: SdramDriver_fileio_tb - Behavioral
-- Description: 
-- Testbench um die Entität SdramDriver zu testen. Es werden Daten aus einer
-- Textdatei gelesen, in den SDRAM geschrieben und anschließend ausgelesen und in
-- eine Output-Datei geschrieben. Die Input-Datei muss vorher generiert und die
-- Pfade müssen angepasst werden. In der Input-Datei müssen Adresse und Daten
-- in Hex, getrennt durch ein Leerzeichen eingetragen werden. Adresse und Daten
-- müssen 32 Bit breit sein. Die Adressen haben eine Wortausrichtung.
-- Beispieldaten:
-- 00000004 8A80164D
-- 00000008 48AB3428
-- 0000000C 25224532
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use std.env.stop;

entity SdramDriver_fileio_tb is
--  Port ( );
end SdramDriver_fileio_tb;

architecture Behavioral of SdramDriver_fileio_tb is
    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out20          : out    std_logic;
      clk_out_inv20          : out    std_logic;
      clk_out200          : out    std_logic;
      clk_out166          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;

    signal clk, clk20, ref_clk200, sys_clk166, rst : std_logic := '0';
    signal writeEn, readEn, ack : std_logic := '0';
    signal mask : std_Logic_vector(15 downto 0) := (others => '0');
    
    signal addr : std_logic_vector(27 downto 0) := (others => '0');
    signal din, dout : std_logic_vector(127 downto 0) := (others => '0');
    
    file file_INPUT : text;
    file file_OUTPUT : text;
    
    constant DUMMY : std_logic_vector(95 downto 0) := (others => '0');
    constant DUMMY_WORD : std_logic_vector(31 downto 0) := (others => '0');
    
    -- ddr3 signals
    signal ddr3_dq : std_logic_vector(15 downto 0);
    signal ddr3_dqs_p, ddr3_dqs_n, tdqs_n : std_logic_vector(1 downto 0);
    signal ddr3_addr : std_logic_vector(13 downto 0);
    signal ddr3_ba : std_logic_vector(2 downto 0);
    signal ddr3_ras_n, ddr3_cas_n, ddr3_we_n, ddr3_reset_n : std_logic;
    signal ddr3_ck_p, ddr3_ck_n, ddr3_cke, ddr3_cs_n, ddr3_odt : std_logic_vector(0 downto 0);
    signal ddr3_dm : std_logic_vector(1 downto 0);
begin

    clk <= not clk after 5 ns; -- 100 MHz
    rst <= '1', '0' after 50 ns;
    
    -- example from 
    -- https://www.nandland.com/vhdl/examples/example-file-io.html
    -- and https://surf-vhdl.com/read-from-file-in-vhdl-using-textio-library/
    process
        variable v_ILINE : line;
        variable v_OLINE : line;
        variable v_ADDR : std_logic_vector(31 downto 0);
        variable v_SPACE : character;
        variable v_DATA : std_logic_vector(31 downto 0);
        variable v_count : integer := 0;
        variable o_DATA : std_logic_vector(31 downto 0);
    begin
        writeEn <= '0';
        readEn <= '0';
        v_count := 0;
        wait until rst = '0';
        wait for 50 ns;
        
        file_open(file_INPUT, "/home/user/Desktop/input_data.txt", read_mode);
        -- write file into sdram
        report "Start writing data to SDRAM";
        while not endfile(file_INPUT) loop
            readline(file_INPUT, v_ILINE);
            hread(v_ILINE, v_ADDR);
            read(v_ILINE, v_SPACE);
            hread(v_ILINE, v_DATA);
            
            writeEn <= '1';
            readEn <= '0';
            addr <= v_ADDR(27 downto 4) & "0000";
            
            
            case v_ADDR(3 downto 2) is
                when "00" =>
                    din <= DUMMY & v_DATA;
                    mask <= x"fff0";
                when "01" =>
                    din <= DUMMY_WORD & DUMMY_WORD & v_DATA & DUMMY_WORD;
                    mask <= x"ff0f";
                when "10" =>
                    din <= DUMMY_WORD & v_DATA & DUMMY_WORD & DUMMY_WORD;
                    mask <= x"f0ff";
                when "11" =>
                    din <= v_DATA & DUMMY_WORD & DUMMY_WORD & DUMMY_WORD;
                    mask <= x"0fff";
                when others =>
            end case;
            
            wait until ack = '1';
            
            writeEn <= '0';
            v_count := v_count + 1;
            wait for 50 ns;
        end loop;
        file_close(file_INPUT);
        report "Finished writing data";
        
        
        -- read back
        file_open(file_INPUT, "/home/user/Desktop/input_data.txt", read_mode);
        file_open(file_OUTPUT, "/home/user/Desktop/output_data.txt", write_mode);
        report "Start reading data from SDRAM";
        for I in 0 to v_count-1 loop
            readline(file_INPUT, v_ILINE);
            hread(v_ILINE, v_ADDR);
            read(v_ILINE, v_SPACE);
            hread(v_ILINE, v_DATA);
            
            writeEn <= '0';
            readEn <= '1';
            addr <= v_ADDR(27 downto 4) & "0000";
            din <= (others => '0');
            
            wait until ack = '1';
            
            case v_ADDR(3 downto 2) is
                when "00" =>
                    o_DATA := dout(31 downto 0);
                when "01" =>
                    o_DATA := dout(63 downto 32);
                when "10" =>
                    o_DATA := dout(95 downto 64);
                when "11" =>
                    o_DATA := dout(127 downto 96);
                when others =>
            end case;
            
            hwrite(v_OLINE, v_ADDR);
            write(v_OLINE, v_SPACE);
            hwrite(v_OLINE, o_DATA);
            writeline(file_OUTPUT, v_OLINE);
            
            wait until ack = '0';
        end loop;
        file_close(file_INPUT);
        file_close(file_OUTPUT);
        report "Finished reading data";
        report "Finished simulation";
        stop;
    end process;

    inst_clk : clk_wiz_0 port map (
        clk_in1 => clk,
        reset => rst,
        
        clk_out20 => clk20,
        clk_out200 => ref_clk200,
        clk_out166 => sys_clk166
    );

    inst_driver : entity work.SdramDriver port map (
        sys_clk => clk20,
        sys_rst => rst,
        
        sys_clk166 => sys_clk166,
        ref_clk200 => ref_clk200,
        
        writeEn => writeEn,
        readEn => readEn,
        ack => ack,
        
        addr => addr,
        din => din,
        dout => dout,
        mask => mask,
        --ddr3 connection
        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n,
        ddr3_cke => ddr3_cke,
        ddr3_cs_n => ddr3_cs_n,
        ddr3_dm => ddr3_dm,
        ddr3_odt => ddr3_odt
    );

    inst_ddr : entity work.ddr3 port map(
        rst_n => ddr3_reset_n,
        ck => ddr3_ck_p,
        ck_n => ddr3_ck_n,
        cke => ddr3_cke,
        cs_n => ddr3_cs_n,
        ras_n => ddr3_ras_n,
        cas_n => ddr3_cas_n,
        we_n => ddr3_we_n,
        dm_tdqs => ddr3_dm,
        ba => ddr3_ba,
        addr => ddr3_addr(12 downto 0),
        dq => ddr3_dq,
        dqs => ddr3_dqs_p,
        dqs_n => ddr3_dqs_n,
        odt => ddr3_odt,
        tdqs_n => tdqs_n
    );
    
end Behavioral;
