----------------------------------------------------------------------------------
-- Engineer:  Mario Hoffmann
-- 
-- Create Date: 11/07/2021 11:50:21 PM
-- Design Name: 
-- Module Name: Sdram_fileio_tb - Behavioral
-- Description: Diese Testbench simuliert die SDRAM entität mit vorher
-- generierten Daten. Die Eingangsdaten müssen in einer .txt Datei vorliegen
-- und müssen in jeder Zeile Adresse und Daten in Hex, gentrennt durch ein
-- Leerzeichen beinhalten. Z.B.:
-- 00000004 8A80164D
-- 00000008 48AB3428
-- 0000000C 25224532
-- Adresse und Daten sind jeweils 32 Bit breit.
-- Die Pfade zu den Input und Output Dateien müssen angepasst werden.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use std.env.stop;

entity Sdram_fileio_tb is
--  Port ( );
end Sdram_fileio_tb;

architecture Behavioral of Sdram_fileio_tb is
    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out20          : out    std_logic;
      clk_out_inv20          : out    std_logic;
      clk_out200          : out    std_logic;
      clk_out166          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;

    signal clk, clk20, ref_clk200, sys_clk166, rst : std_logic := '0';
    signal writeEn, readEn, ack : std_logic := '0';
    signal mask : std_Logic_vector(15 downto 0) := (others => '0');
    
    signal addr : std_logic_vector(27 downto 0) := (others => '0');
    signal din, dout : std_logic_vector(31 downto 0) := (others => '0');
    
    file file_INPUT : text;
    file file_OUTPUT : text;
    
    -- ddr3 signals
    signal ddr3_dq : std_logic_vector(15 downto 0);
    signal ddr3_dqs_p, ddr3_dqs_n, tdqs_n : std_logic_vector(1 downto 0);
    signal ddr3_addr : std_logic_vector(13 downto 0);
    signal ddr3_ba : std_logic_vector(2 downto 0);
    signal ddr3_ras_n, ddr3_cas_n, ddr3_we_n, ddr3_reset_n : std_logic;
    signal ddr3_ck_p, ddr3_ck_n, ddr3_cke, ddr3_cs_n, ddr3_odt : std_logic_vector(0 downto 0);
    signal ddr3_dm : std_logic_vector(1 downto 0);
begin

    clk <= not clk after 5 ns; -- 100 MHz
    rst <= '1', '0' after 50 ns;


    process
        variable v_ILINE : line;
        variable v_OLINE : line;
        variable v_ADDR : std_logic_vector(31 downto 0);
        variable v_SPACE : character;
        variable v_DATA : std_logic_vector(31 downto 0);
        variable v_count : integer := 0;
        variable o_DATA : std_logic_vector(31 downto 0);
        variable v_TIME : time := 0 ns;
    begin
        writeEn <= '0';
        readEn <= '0';
        v_count := 0;
        wait until rst = '0';
        wait for 50 ns;
        
        file_open(file_INPUT, "/home/user/Desktop/data_input.txt", read_mode);
        -- write file into sdram
        report "Start writing data to SDRAM";
        while not endfile(file_INPUT) loop
            -- Daten aus der Textdatei lesen und vorbereiten.
            readline(file_INPUT, v_ILINE);
            hread(v_ILINE, v_ADDR);
            read(v_ILINE, v_SPACE);
            hread(v_ILINE, v_DATA);
            
            -- SDRAM signale setzen um zu schreiben
            writeEn <= '1';
            readEn <= '0';
            addr <= v_ADDR(27 downto 0);
            din <= v_DATA;

            -- Auf Bestätigung des SDRAM warten und anschließend mit dem
            -- nächsten Datum beginnen.
            wait until ack = '1';
                        
            writeEn <= '0';
            v_count := v_count + 1;
            wait for 50 ns;
        end loop;
        file_close(file_INPUT);
        report "Finished writing data";
        
        
        -- read back
        -- Das gleiche wie schreiben, mit anderem Kommando. Die gelesenen Daten
        -- werden in die Datei file_OUTPUT geschrieben.
        file_open(file_INPUT, "/home/user/Desktop/data_input.txt", read_mode);
        file_open(file_OUTPUT, "/home/user/Desktop/data_output.txt", write_mode);
        report "Start reading data from SDRAM";
        report "READ START TIME = " & time'image(now);
        v_TIME := now;
        for I in 0 to v_count-1 loop
            readline(file_INPUT, v_ILINE);
            hread(v_ILINE, v_ADDR);
            read(v_ILINE, v_SPACE);
            hread(v_ILINE, v_DATA);
            
            writeEn <= '0';
            readEn <= '1';
            addr <= v_ADDR(27 downto 2) & "00";
            din <= (others => '0');
            
            wait until ack = '1';
            hwrite(v_OLINE, v_ADDR);
            write(v_OLINE, v_SPACE);
            
            wait until ack = '0';
            o_DATA := dout(31 downto 0);
            hwrite(v_OLINE, o_DATA);
            writeline(file_OUTPUT, v_OLINE);
        end loop;
        file_close(file_INPUT);
        file_close(file_OUTPUT);
        report "READ END TIME = " & time'image(now);
        v_TIME := now - V_TIME; --used to find delta time
        report "TOTAL TIME TAKEN = " & time'image(v_TIME);
        report "Finished reading data";
        report "Finished simulation";
        stop;
    end process;
    
    -- inst_Sdram : entity work.Sdram(SimpleBuffer) port map
    inst_Sdram : entity work.Sdram(DirectMapped) port map
    (
        sys_clk => clk20,
        sys_rst => rst,
        sys_clk166 => sys_clk166,
        ref_clk200 => ref_clk200,
        writeEn => writeEn,
        readEn => readEn,
        ack => ack,
        
        addr => addr,
        din => din,
        dout => dout,
        -- hardware interface
        ddr3_addr                      => ddr3_addr,
        ddr3_ba                        => ddr3_ba,
        ddr3_cas_n                     => ddr3_cas_n,
        ddr3_ck_n                      => ddr3_ck_n,
        ddr3_ck_p                      => ddr3_ck_p,
        ddr3_cke                       => ddr3_cke,
        ddr3_ras_n                     => ddr3_ras_n,
        ddr3_reset_n                   => ddr3_reset_n,
        ddr3_we_n                      => ddr3_we_n,
        ddr3_dq                        => ddr3_dq,
        ddr3_dqs_n                     => ddr3_dqs_n,
        ddr3_dqs_p                     => ddr3_dqs_p,
        ddr3_cs_n                      => ddr3_cs_n,
        ddr3_dm                        => ddr3_dm,
        ddr3_odt                       => ddr3_odt
    );
    
    inst_ddr : entity work.ddr3 port map(
        rst_n => ddr3_reset_n,
        ck => ddr3_ck_p,
        ck_n => ddr3_ck_n,
        cke => ddr3_cke,
        cs_n => ddr3_cs_n,
        ras_n => ddr3_ras_n,
        cas_n => ddr3_cas_n,
        we_n => ddr3_we_n,
        dm_tdqs => ddr3_dm,
        ba => ddr3_ba,
        addr => ddr3_addr, --(12 downto 0),
        dq => ddr3_dq,
        dqs => ddr3_dqs_p,
        dqs_n => ddr3_dqs_n,
        odt => ddr3_odt,
        tdqs_n => tdqs_n
    );
    
    inst_clk : clk_wiz_0 port map (
        clk_in1 => clk,
        reset => rst,
        
        clk_out20 => clk20,
        clk_out200 => ref_clk200,
        clk_out166 => sys_clk166
    );
    
end Behavioral;
