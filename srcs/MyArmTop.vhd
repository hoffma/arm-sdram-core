----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2021 09:20:13 AM
-- Design Name: 
-- Module Name: MyArmTop - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MyArmTop is
    Generic (
        CACHE_ENABLE : boolean := TRUE
    );
    Port 
    (
        EXT_RST : in STD_LOGIC;
        EXT_CLK : in STD_LOGIC;
        EXT_LDP : in STD_LOGIC;
        EXT_RXD : in STD_LOGIC;
        EXT_TXD : out STD_LOGIC;
        EXT_LED : out STD_LOGIC_VECTOR (7 downto 0);
        
		ddr3_dq       : inout std_logic_vector(15 downto 0);
        ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
        ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
        
        ddr3_addr     : out   std_logic_vector(13 downto 0);
        ddr3_ba       : out   std_logic_vector(2 downto 0);
        ddr3_ras_n    : out   std_logic;
        ddr3_cas_n    : out   std_logic;
        ddr3_we_n     : out   std_logic;
        ddr3_reset_n  : out   std_logic;
        ddr3_ck_p     : out   std_logic_vector(0 downto 0);
        ddr3_ck_n     : out   std_logic_vector(0 downto 0);
        ddr3_cke      : out   std_logic_vector(0 downto 0);
        ddr3_cs_n     : out   std_logic_vector(0 downto 0);
        ddr3_dm       : out   std_logic_vector(1 downto 0);
        ddr3_odt      : out   std_logic_vector(0 downto 0)
    );
end MyArmTop;

architecture Behavioral of MyArmTop is
    
    component clk_wiz_0
    port
     (-- Clock in ports
      -- Clock out ports
      clk_out20          : out    std_logic;
      clk_out_inv20          : out    std_logic;
      clk_out200          : out    std_logic;
      clk_out166          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1           : in     std_logic
     );
    end component;

    signal INT_CLK, INT_INV_CLK, INT_CLK_200, INT_CLK_166, DCM_LOCKED : std_logic;
begin
--    Inst_ArmClkGen: entity work.ArmClkGen 
--        port map(
--            CLKIN_IN	=> EXT_CLK,
--            RST_IN		=> EXT_RST,
--            CLKFX_OUT	=> INT_CLK,
--            CLKFX180_OUT	=> INT_INV_CLK,
--            CLKIN_IBUFG_OUT	=> open,
--            LOCKED_OUT	=> DCM_LOCKED
--        );
    
    clk_inst : clk_wiz_0 port map
    (
        clk_in1 => EXT_CLK,
        reset => EXT_RST,
        
        clk_out20 => INT_CLK,
        clk_out_inv20 => INT_INV_CLK,
        clk_out200 => INT_CLK_200,
        clk_out166 => INT_CLK_166,
        locked => DCM_LOCKED
    );
    
    EXT_LED(7 downto 1) <= (others => '0');
    
    
    Inst_ArmTop: entity work.ArmTop
    generic map (
        CACHE_ENABLE => CACHE_ENABLE
    )
    port map(
        EXT_RST => EXT_RST,
        INT_CLK => INT_CLK,
        INT_INV_CLK => INT_INV_CLK,
        EXT_LDP => EXT_LDP,
        EXT_RXD => EXT_RXD,
        EXT_TXD => EXT_TXD,
        EXT_LED => EXT_LED(0),
        DCM_LOCKED => DCM_LOCKED,
        
        SDRAM_CLK => INT_CLK_200,
        CLK166 => INT_CLK_166,
        -- hardware interface
        ddr3_dq => ddr3_dq,
        ddr3_dqs_p => ddr3_dqs_p,
        ddr3_dqs_n => ddr3_dqs_n,
        ddr3_addr => ddr3_addr,
        ddr3_ba => ddr3_ba,
        ddr3_ras_n => ddr3_ras_n,
        ddr3_cas_n => ddr3_cas_n,
        ddr3_we_n => ddr3_we_n,
        ddr3_reset_n => ddr3_reset_n,
        ddr3_ck_p => ddr3_ck_p,
        ddr3_ck_n => ddr3_ck_n,
        ddr3_cke => ddr3_cke,
        ddr3_cs_n => ddr3_cs_n,
        ddr3_dm => ddr3_dm,
        ddr3_odt => ddr3_odt
    );
end Behavioral;
